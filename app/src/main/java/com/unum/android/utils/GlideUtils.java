package com.unum.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.unum.android.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Dhaval Parmar on 7/9/16.
 */
public class GlideUtils {

	public static void loadGif(Context context, ImageView loadingView) {
		GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(loadingView);
		Glide.with(context).load(R.drawable.unum_black_gif).into(imageViewTarget);
	}

	public static void getImageFromPath(Context context, ImageView imageView, String path) {
		if (TextUtils.isEmpty(path)) return;

		UnumSession mUnumSession = new UnumSession(context);
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		int placeHolder = R.drawable.image_placeholder_light;
		if(theme > 3){
			placeHolder = R.drawable.image_placeholder_dark;
		}
		Glide.with(context)
				.load(path)
				.placeholder(placeHolder)
				.error(placeHolder)
				.diskCacheStrategy(DiskCacheStrategy.SOURCE)
				.into(imageView);
	}

	public static void getImageFromPath(Context context, ImageView imageView, String path,
			OnImageLoadListener listener, int width, int height) {
		if (TextUtils.isEmpty(path)) return;

		UnumSession mUnumSession = new UnumSession(context);
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		int placeHolder = R.drawable.image_placeholder_light;
		if(theme > 3){
			placeHolder = R.drawable.image_placeholder_dark;
		}

		Glide.with(context)
				.load(path)
				.placeholder(placeHolder)
				.error(placeHolder)
				.override(width, height)
				.diskCacheStrategy(DiskCacheStrategy.SOURCE)
				.centerCrop()
				.listener(new RequestListener<String, GlideDrawable>() {
					@Override
					public boolean onException(Exception e, String model, Target<GlideDrawable> target,
							boolean isFirstResource) {
						if(listener!=null) {
							listener.onImageLoadError("Error loading image :"+path);
						}
						return false;
					}

					@Override public boolean onResourceReady(GlideDrawable resource, String model,
							Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
						if(listener!=null) {
							listener.onImageLoadingComplete();
						}
						return false;
					}
				})
				.into(imageView);
	}

	public static void loadImageInImageView(Context mContext, ImageView mImageView, String imageUrl,
			OnImageLoadListener listener) {

		AppLogger.d("Image url", imageUrl);
		if (imageUrl == null) return;

		File dest = makeFile(imageUrl);

		Glide.with(mContext).load(imageUrl).asBitmap().listener(new RequestListener<String, Bitmap>() {
			@Override public boolean onException(Exception e, String model, Target<Bitmap> target,
					boolean isFirstResource) {
				if (null != listener) {
					listener.onImageLoadError(e.getMessage());
				}
				return false;
			}

			@Override public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target,
					boolean isFromMemoryCache, boolean isFirstResource) {
				if (null != listener) {
					listener.onImageLoadingComplete();
				}
				return false;
			}
		}).into(new SimpleTarget<Bitmap>() {
			@Override
			public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
				mImageView.setImageBitmap(bitmap);
				try {
					if (!dest.exists()) {
						dest.getParentFile().mkdir();
						OutputStream out = new FileOutputStream(dest);
						bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
						out.flush();
						out.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static File makeFile(String imageUrl) {
		try {
			String mFileName = "", mfileNameWithoutExtn = "", mfileExtn = "";
			File mRoot;
			String mRootDir;

			mFileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1, imageUrl.length());
			mfileNameWithoutExtn = mFileName.substring(0, mFileName.lastIndexOf('.'));
			mfileExtn = mFileName.substring(mFileName.lastIndexOf('.') + 1);

			mRootDir = Environment.getExternalStorageDirectory() + File.separator + "UNUM";
			mRoot = Environment.getExternalStoragePublicDirectory(mRootDir);

			if (!mRoot.exists()) {
				mRoot.mkdir();
			}

			String fileName = mfileNameWithoutExtn + "." + mfileExtn;
			return new File(mRootDir, fileName);
		} catch (Exception e) {
			AppLogger.e(e.getMessage());
			return null;
		}
	}

	public interface OnImageLoadListener {
		 void onImageLoadingComplete();
		 void onImageLoadError(String error);
	}
}
