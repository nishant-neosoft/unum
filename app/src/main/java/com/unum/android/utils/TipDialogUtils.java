package com.unum.android.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.michael.easydialog.EasyDialog;
import com.unum.android.R;
import java.util.ArrayList;

/**
 * Created by Dhaval Parmar on 13/9/16.
 */
public class TipDialogUtils {
	private Context mContext;
	private LayoutInflater inflater;
	private View view;

	private TextView textTipTitle;
	private ImageView imageCloseTip;
	private ImageView imagePreviousTip;
	private TextView textTipDescription;
	private ImageView imageNextTip;

	private EasyDialog mEasyDialog;

	private int currentPosition = 0;

	public TipDialogUtils(Context mContext, ArrayList<Tips> tips, int currentPosition) {
		this.mContext = mContext;
		this.currentPosition = currentPosition;

		View attachedView = null;

		refreshViews(tips);

		//if (tips.size() == 1) {
		//	imageNextTip.setVisibility(View.GONE);
		//	imagePreviousTip.setVisibility(View.GONE);
		//}

		try {
			textTipTitle.setText(tips.get(currentPosition).getTitle());
			textTipDescription.setText(tips.get(currentPosition).getMessage());
			attachedView = tips.get(currentPosition).getViewId();
		} catch (Exception e) {
			e.printStackTrace();
		}

		showTip(attachedView, tips.get(currentPosition).isCenter(),
				tips.get(currentPosition).isPointer());
	}

	private void refreshViews(ArrayList<Tips> tips) {
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.tip_layout, null);

		if (mEasyDialog != null) {
			mEasyDialog.dismiss();
		}

		mEasyDialog = new EasyDialog(mContext);

		textTipTitle = (TextView) view.findViewById(R.id.text_tip_title);
		imageCloseTip = (ImageView) view.findViewById(R.id.image_close_tip);
		imagePreviousTip = (ImageView) view.findViewById(R.id.image_previous_tip);
		textTipDescription = (TextView) view.findViewById(R.id.text_tip_description);
		imageNextTip = (ImageView) view.findViewById(R.id.image_next_tip);

		if (currentPosition == 0) {
			imagePreviousTip.setVisibility(View.GONE);
			imageNextTip.setVisibility(View.VISIBLE);
		} else if (currentPosition == tips.size()) {
			imagePreviousTip.setVisibility(View.VISIBLE);
			imageNextTip.setVisibility(View.GONE);
		}

		imageCloseTip.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				mEasyDialog.dismiss();
			}
		});

		imageNextTip.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				currentPosition++;
				if (tips.size() > 1 && currentPosition <= tips.size()) {
					refreshViews(tips);
					textTipTitle.setText(tips.get(currentPosition).getTitle());
					textTipDescription.setText(tips.get(currentPosition).getMessage());
					showTip(tips.get(currentPosition).getViewId(), tips.get(currentPosition).isCenter(),
							tips.get(currentPosition).isPointer());
				}

				if (currentPosition == tips.size() - 1) {
					imageNextTip.setVisibility(View.GONE);
				}
			}
		});

		imagePreviousTip.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				currentPosition--;
				if (tips.size() > 1 && currentPosition <= tips.size()) {
					refreshViews(tips);
					textTipTitle.setText(tips.get(currentPosition).getTitle());
					textTipDescription.setText(tips.get(currentPosition).getMessage());
					showTip(tips.get(currentPosition).getViewId(), tips.get(currentPosition).isCenter(),
							tips.get(currentPosition).isPointer());
				}

				if (currentPosition == tips.size() - 1) {
					imagePreviousTip.setVisibility(View.GONE);
				}
			}
		});
	}

	public TipDialogUtils() {

	}

	public void showTip(View attachedView, boolean isCenter, boolean isPointer) {

		mEasyDialog.setLayout(view)
				.setBackgroundColor(mContext.getResources().getColor(R.color.black))
				//.setTouchOutsideDismiss(false)
				.setCancelable(false)
				.setMatchParent(true)
				.setMarginLeftAndRight(50, 50);

		if (isCenter) {
			mEasyDialog.setLocation(
					new int[] { mEasyDialog.getScreenWidth() / 2, mEasyDialog.getScreenHeight() / 2 })
					.showPointer(false);
		} else {
			if (isPointer) {
				mEasyDialog.setLocationByAttachedView(attachedView)
						.showPointer(true)
						.setGravity(EasyDialog.GRAVITY_TOP);
			} else {
				mEasyDialog.setLocationByAttachedView(attachedView)
						.showPointer(false)
						.setGravity(EasyDialog.GRAVITY_TOP);
			}
		}
		Window window = mEasyDialog.getDialog().getWindow();
		window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
				WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
		window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		mEasyDialog.show();
	}

	public void showTip(View attachedView, boolean isPointer) {
		mEasyDialog.setLayout(view)
				.setBackgroundColor(mContext.getResources().getColor(R.color.black))
				.setTouchOutsideDismiss(true)
				.setMatchParent(true)
				.setMarginLeftAndRight(50, 50);

		if (isPointer) {
			mEasyDialog.setLocationByAttachedView(attachedView)
					.showPointer(true)
					.setGravity(EasyDialog.GRAVITY_BOTTOM);
		} else {
			mEasyDialog.setLocationByAttachedView(attachedView)
					.showPointer(false)
					.setGravity(EasyDialog.GRAVITY_BOTTOM);
		}
		mEasyDialog.show();
	}

	public class Tips {
		String title, message;
		View viewId;
		boolean isCenter, isPointer;

		public Tips(String title, String message, View viewId, boolean isCenter, boolean isPointer) {
			this.title = title;
			this.message = message;
			this.viewId = viewId;
			this.isCenter = isCenter;
			this.isPointer = isPointer;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public View getViewId() {
			return viewId;
		}

		public void setViewId(View viewId) {
			this.viewId = viewId;
		}

		public boolean isCenter() {
			return isCenter;
		}

		public void setCenter(boolean center) {
			isCenter = center;
		}

		public boolean isPointer() {
			return isPointer;
		}

		public void setPointer(boolean pointer) {
			isPointer = pointer;
		}
	}
}
