package com.unum.android.utils;

import java.util.List;
import java.util.Random;

/**
 * Created by Snehal on 9/19/2016.
 */
public class ShuffleUtils {

	public static List<Integer> shuffleList(List<Integer> a) {
		int n = a.size();
		Random random = new Random();
		random.nextInt();
		for (int i = 0; i < n; i++) {
			int change = i + random.nextInt(n - i);
			swap(a, i, change);
		}
		return a;
	}

	private static void swap(List<Integer> a, int i, int change) {
		int helper = a.get(i);
		a.set(i, a.get(change));
		a.set(change, helper);
	}
}
