package com.unum.android.utils;

import android.app.Activity;
import android.content.Intent;
import com.unum.android.R;

/**
 * Created by Dhaval Parmar on 21/9/16.
 */

public class ThemeUtils {
	private static int cTheme;

	public final static int GOLD = 0;
	public final static int CYAN = 1;
	public final static int PITCH = 2;
	public final static int GREY = 3;
	public final static int GOLD_DARK = 4;
	public final static int CYAN_DARK = 5;
	public final static int PITCH_DARK = 6;
	public final static int GREY_DARK = 7;

	public static UnumSession unumSession;

	public static void changeToTheme(Activity activity, int theme) {
		cTheme = theme;
		unumSession = new UnumSession(activity);
		unumSession.setDefaulTheme(cTheme);
		activity.finish();
		activity.startActivity(new Intent(activity, activity.getClass()));
		activity.overridePendingTransition(R.anim.com_adobe_image_fade_in_long,
				R.anim.com_adobe_image_fade_out_long);
	}

	public static void onActivityCreateSetTheme(Activity activity, int theme) {
		switch (theme) {
			default:
				activity.getTheme().applyStyle(R.style.AppTheme, true);
				activity.setTheme(R.style.Gold);
			case GOLD:
				activity.getTheme().applyStyle(R.style.AppTheme, true);
				activity.setTheme(R.style.Gold);
				break;
			case CYAN:
				activity.getTheme().applyStyle(R.style.AppTheme, true);
				activity.setTheme(R.style.Cyan);
				break;
			case PITCH:
				activity.getTheme().applyStyle(R.style.AppTheme, true);
				activity.setTheme(R.style.Pitch);
				break;
			case GREY:
				activity.getTheme().applyStyle(R.style.AppTheme, true);
				activity.setTheme(R.style.Grey);
				break;
			case GOLD_DARK:
				activity.getTheme().applyStyle(R.style.ThemeDark, true);
				activity.setTheme(R.style.Gold_Dark);
				break;
			case CYAN_DARK:
				activity.getTheme().applyStyle(R.style.ThemeDark, true);
				activity.setTheme(R.style.Cyan_Dark);
				break;
			case PITCH_DARK:
				activity.getTheme().applyStyle(R.style.ThemeDark, true);
				activity.setTheme(R.style.Pitch_Dark);
				break;
			case GREY_DARK:
				activity.getTheme().applyStyle(R.style.ThemeDark, true);
				activity.setTheme(R.style.Grey_Dark);
				break;
		}
	}
}
