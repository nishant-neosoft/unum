package com.unum.android.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dhaval Parmar on 23/8/16.
 */
public class UnumSession {
	/**
	 * SharedPrefrence's Name
	 */
	public final String APP_PREF = "unum_session";

	/**
	 * Tags
	 */
	public final String GCM_DEVICE_TOKEN = "deviceToken";
	public final String UNUM_USER_ID = "_id";
	public final String UNUM_AUTH_TOKEN = "authToken";
	public final String UNUM_USER_NAME = "userName";
	public final String UNUM_FULL_NAME = "fullName";
	public final String UNUM_LIVE_GRID_ENABLED = "liveGridEnabled";
	public final String UNUM_NUMBER_OF_FOLLOWERS = "numFollowers";
	public final String UNUM_NUMBER_OF_FOLLWING = "numFollowing";
	public final String UNUM_NUMBER_OF_POSTS = "numPosts";
	public final String UNUM_DEFAULT_DRAFT = "defaultDraft";
	public final String UNUM_QUEUE_DRAFT = "queueDraft";
	public final String UNUM_DEFAULT_THEME = "theme";

	public final boolean isLiveGridEnabled = false;
	public final int numberOfPosts = 0;
	public final int numberOfFollwers = 0;
	public final int numberOfFollowing = 0;

	public final String UNUM_IS_FIRST_LAUNCH = "isFirstLaunch";
	public final boolean isFirstLaunch = true;

	protected Context mContext;
	protected SharedPreferences mSettings;
	protected SharedPreferences.Editor mEditor;

	/**
	 * Consturctor
	 *
	 * @param mContext - context of Activity
	 */
	public UnumSession(Context mContext) {
		if(null != mContext) {
			this.mContext = mContext;
			mSettings = mContext.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
			mEditor = mSettings.edit();
		}
	}

	/**
	 * Store the unum user details after login process
	 *
	 * @param unumUserId user id of loggend in user use in basic auth during webservice call as
	 * UserName
	 * @param unumUserAuthToken session token of user use in basic auth during webservice call as
	 * Password
	 * @param unumUserName user's instagram name
	 * @param unumUserFullName user's full name
	 * @param unumUserPosts number of posts
	 * @param unumNumberOfFollowing number of follwing
	 * @param unumNumberOfFollowers number of followers
	 */
	public void storeUnumSession(String unumUserId, String unumUserAuthToken,
			boolean unumIsLiveGridEnabled, String unumUserName, String unumUserFullName,
			int unumUserPosts, int unumNumberOfFollowing, int unumNumberOfFollowers) {
		mEditor.putString(UNUM_USER_ID, unumUserId);
		mEditor.putString(UNUM_AUTH_TOKEN, unumUserAuthToken);
		mEditor.putBoolean(UNUM_LIVE_GRID_ENABLED, unumIsLiveGridEnabled);
		mEditor.putString(UNUM_USER_NAME, unumUserName);
		mEditor.putString(UNUM_FULL_NAME, unumUserFullName);
		mEditor.putInt(UNUM_NUMBER_OF_POSTS, unumUserPosts);
		mEditor.putInt(UNUM_NUMBER_OF_FOLLWING, unumNumberOfFollowing);
		mEditor.putInt(UNUM_NUMBER_OF_FOLLOWERS, unumNumberOfFollowers);
		mEditor.commit();
	}

	/**
	 * Store the GCM Device Token
	 *
	 * @param token token provided by GCM/FCM usign Instance ID
	 */
	public void storeDeviceToken(String token) {
		mEditor.putString(GCM_DEVICE_TOKEN, token);
		mEditor.commit();
	}

	/**
	 * Store theme prefeence
	 *
	 * @param theme theme code
	 */
	public void setDefaulTheme(int theme) {
		mEditor.putInt(UNUM_DEFAULT_THEME, theme);
		mEditor.commit();
	}

	/**
	 * Store the Drafts ids
	 *
	 * @param defaultDraft draft id of default draft
	 * @param queueDraft draft id of queue draft
	 */
	public void storeDrafts(String defaultDraft, String queueDraft) {
		mEditor.putString(UNUM_DEFAULT_DRAFT, defaultDraft);
		mEditor.putString(UNUM_QUEUE_DRAFT, queueDraft);
		mEditor.commit();
	}

	/**
	 * Reset the unum session
	 */
	public void resetUnumSession() {
		mEditor.putString(UNUM_USER_ID, null);
		mEditor.putString(UNUM_AUTH_TOKEN, null);
		mEditor.putBoolean(UNUM_LIVE_GRID_ENABLED, false);
		mEditor.putString(UNUM_USER_NAME, null);
		mEditor.putString(UNUM_FULL_NAME, null);
		mEditor.putInt(UNUM_NUMBER_OF_POSTS, 0);
		mEditor.putInt(UNUM_NUMBER_OF_FOLLWING, 0);
		mEditor.putInt(UNUM_NUMBER_OF_FOLLOWERS, 0);
		mEditor.putString(GCM_DEVICE_TOKEN, null);
		mEditor.putString(UNUM_DEFAULT_DRAFT, null);
		mEditor.putString(UNUM_QUEUE_DRAFT, null);
		mEditor.putBoolean(UNUM_IS_FIRST_LAUNCH, true);
		mEditor.putInt(UNUM_DEFAULT_THEME, 0);
		mEditor.commit();
	}

	/**
	 * Get device token
	 *
	 * @return device token
	 */
	public String getGCM_DEVICE_TOKEN() {
		return mSettings.getString(GCM_DEVICE_TOKEN, null);
	}

	/**
	 * Set device token
	 */
	public void setGCM_DEVICE_TOKEN(String deviceToken) {
		mEditor.putString(GCM_DEVICE_TOKEN, deviceToken);
		mEditor.commit();
	}

	/**
	 * Get Unum User ID
	 *
	 * @return unum user id
	 */
	public String getUNUM_USER_ID() {
		return mSettings.getString(UNUM_USER_ID, null);
	}

	/**
	 * Set Unum user id
	 */
	public void setUNUM_USER_ID(String userID) {
		mEditor.putString(UNUM_USER_ID, userID);
		mEditor.commit();
	}

	/**
	 * Get Unum Auth Token (Basic Auth Password)
	 *
	 * @return auth token
	 */
	public String getUNUM_AUTH_TOKEN() {
		return mSettings.getString(UNUM_AUTH_TOKEN, null);
	}

	/**
	 * Set Unum user auth token
	 */
	public void setUNUM_AUTH_TOKEN(String authToken) {
		mEditor.putString(UNUM_AUTH_TOKEN, authToken);
		mEditor.commit();
	}

	/**
	 * Get Unum User Name
	 *
	 * @return unum user name
	 */
	public String getUNUM_USER_NAME() {
		return mSettings.getString(UNUM_USER_NAME, null);
	}

	/**
	 * Set Unum user name
	 */
	public void setUNUM_USER_NAME(String userName) {
		mEditor.putString(UNUM_USER_NAME, userName);
		mEditor.commit();
	}

	/**
	 * Get Unum user's Full Name
	 *
	 * @return unum user's full name
	 */
	public String getUNUM_FULL_NAME() {
		return mSettings.getString(UNUM_FULL_NAME, null);
	}

	/**
	 * Set Unum user full name
	 */
	public void setUNUM_FULL_NAME(String userFullName) {
		mEditor.putString(UNUM_FULL_NAME, userFullName);
		mEditor.commit();
	}

	/**
	 * Get status of live grid
	 *
	 * @return status of live grid of user
	 */
	public boolean isLiveGridEnabled() {
		return mSettings.getBoolean(UNUM_LIVE_GRID_ENABLED, false);
	}

	/**
	 * Set Unum user live grid status
	 */
	public void setisLiveGridEnabled(boolean isLiveGridEnabled) {
		mEditor.putBoolean(UNUM_LIVE_GRID_ENABLED, isLiveGridEnabled);
		mEditor.commit();
	}

	/**
	 * Get number of posts of user
	 *
	 * @return number of posts
	 */
	public int getNumberOfPosts() {
		return mSettings.getInt(UNUM_NUMBER_OF_POSTS, 0);
	}

	/**
	 * Set Unum user's number of posts
	 */
	public void setNumberOfPosts(int numberOfPosts) {
		mEditor.putInt(UNUM_NUMBER_OF_POSTS, numberOfPosts);
		mEditor.commit();
	}

	/**
	 * Get number of follwers of user
	 *
	 * @return number of followers
	 */
	public int getNumberOfFollwers() {
		return mSettings.getInt(UNUM_NUMBER_OF_FOLLOWERS, 0);
	}

	/**
	 * Set Unum user's number of followers
	 */
	public void setNumberOfFollwers(int numberOfFollower) {
		mEditor.putInt(UNUM_NUMBER_OF_FOLLOWERS, numberOfFollower);
		mEditor.commit();
	}

	/**
	 * Get number of following of user
	 *
	 * @return number of following
	 */
	public int getNumberOfFollowing() {
		return mSettings.getInt(UNUM_NUMBER_OF_FOLLWING, 0);
	}

	/**
	 * Set Unum user's number of following
	 */
	public void setNumberOfFollowing(int numberOfFollowing) {
		mEditor.putInt(UNUM_NUMBER_OF_FOLLWING, numberOfFollowing);
		mEditor.commit();
	}

	public String getUNUM_DEFAULT_DRAFT() {
		return mSettings.getString(UNUM_DEFAULT_DRAFT, null);
	}

	public void setUNUM_DEFAULT_DRAFT(String defaultDraft) {
		mEditor.putString(UNUM_DEFAULT_DRAFT, defaultDraft);
		mEditor.commit();
	}

	public String getUNUM_QUEUE_DRAFT() {
		return mSettings.getString(UNUM_QUEUE_DRAFT, null);
	}

	public void setUNUM_QUEUE_DRAFT(String queueDraft) {
		mEditor.putString(UNUM_FULL_NAME, queueDraft);
		mEditor.commit();
	}

	public boolean isFirstLaunch() {
		return mSettings.getBoolean(UNUM_IS_FIRST_LAUNCH, isFirstLaunch);
	}

	public void setisFirstLaunch(boolean isFirstLaunch) {
		mEditor.putBoolean(UNUM_IS_FIRST_LAUNCH, isFirstLaunch);
		mEditor.commit();
	}

	public int getUNUM_DEFAULT_THEME() {
		return mSettings.getInt(UNUM_DEFAULT_THEME, 0);
	}
}
