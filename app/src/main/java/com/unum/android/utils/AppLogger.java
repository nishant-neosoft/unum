package com.unum.android.utils;

import android.util.Log;

/**
 * Created by Nishant on 15/3/16.
 */
public class AppLogger {

	private static boolean isDebug = false;

	public static void print(String msg){
		Log.d("TEST",msg);
	}

	public static void d(String tag, String message) {
		if(isDebug){
			Log.d("debug " + tag, message + "");
		}
	}

	public static void d(String message) {
		if(isDebug) {
			Log.d("debug ", message + "");
		}
	}

	public static void e(String tag, String message) {
		if(isDebug) {
			Log.e("debug " + tag, message + "");
		}
	}

	public static void e(String message) {
		if(isDebug) {
			Log.e("debug ", message + "");
		}
	}
}
