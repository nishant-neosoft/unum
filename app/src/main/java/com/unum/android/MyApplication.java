package com.unum.android;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.adobe.creativesdk.foundation.AdobeCSDKFoundation;
import com.adobe.creativesdk.foundation.auth.IAdobeAuthClientCredentials;
import com.facebook.stetho.Stetho;

/**
 * Created by Nishant Shah on 26-Aug-16.
 */
public class MyApplication extends MultiDexApplication implements IAdobeAuthClientCredentials {

	// Testing Credentials
	private static final String CREATIVE_SDK_CLIENT_ID = "0e4169412c7543e99ed52177373c02b3";
	private static final String CREATIVE_SDK_CLIENT_SECRET = "3742a928-d30c-4570-9184-c989be93bee8";

	// Live Credentials
	//private static final String CREATIVE_SDK_CLIENT_ID = "2c5c2ae1a593435eb9f1b5b854bdd0";
	//private static final String CREATIVE_SDK_CLIENT_SECRET = "828bd36a-12d9-4839-a26e-0c00e6f626e0";

	private static Context sContext;

	public static Context getContext() {
		return sContext;
	}

	public void onCreate() {
		super.onCreate();
		sContext = getApplicationContext();
		Stetho.initializeWithDefaults(this);
		AdobeCSDKFoundation.initializeCSDKFoundation(getApplicationContext());
	}

	@Override public String getClientID() {
		return CREATIVE_SDK_CLIENT_ID;
	}

	@Override public String getClientSecret() {
		return CREATIVE_SDK_CLIENT_SECRET;
	}
}
