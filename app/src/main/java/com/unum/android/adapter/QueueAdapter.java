package com.unum.android.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.unum.android.R;
import com.unum.android.data.model.Image;
import com.unum.android.ui.HomeBaseFragment;
import com.unum.android.ui.helper.ItemTouchHelperAdapter;
import com.unum.android.ui.views.UnumImageView;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Snehal on 7/31/16.
 */
public class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.ViewHolder>
		implements ItemTouchHelperAdapter {

	private Context context;
	private List<Image> images;
	private List<Image> selectedImages;
	private ViewHolder.OnItemClickListener onItemClickListener;
	private LayoutInflater inflater;
	private int size;
	private int imageSize;
	private int padding;
	public int theme;
	private UnumSession mUnumSession;

	public QueueAdapter(Context context, List<Image> images, List<Image> selectedImages,
			ViewHolder.OnItemClickListener onItemClickListener) {
		this.context = context;
		this.images = images;
		this.selectedImages = selectedImages;
		this.onItemClickListener = onItemClickListener;
		inflater = LayoutInflater.from(this.context);
		padding = context.getResources().getDimensionPixelSize(R.dimen.item_padding);
		mUnumSession = new UnumSession(context);
		theme = mUnumSession.getUNUM_DEFAULT_THEME();
	}

	@Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = inflater.inflate(R.layout.list_item_grid_fragment, parent, false);
		return new ViewHolder(itemView, onItemClickListener);
	}

	public void setTheme(int theme) {
		this.theme = theme;
	}

	@Override public void onBindViewHolder(ViewHolder viewHolder, int position) {

		Image image = images.get(position);

		viewHolder.unum_image_view.getLayoutParams().width = imageSize;
		viewHolder.unum_image_view.getLayoutParams().height = imageSize;

		viewHolder.itemView.getLayoutParams().width = size;
		viewHolder.itemView.getLayoutParams().height = size;

		viewHolder.unum_image_view.setImageSize(size, size);
		viewHolder.unum_image_view.setPath(image.getPath());

		viewHolder.view.setAlpha(1.0f);

		if (isSelected(image)) {
			if (Build.VERSION.SDK_INT >= 23) {
				switch (theme) {
					case 0:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_gold, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_gold, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGold));
						break;
					case 1:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_cyan, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_cyan, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeCyan));
						break;
					case 2:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_pitch, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_pitch, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemePitch));
						break;
					case 3:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_grey, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_grey, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGrey));
						break;
					case 4:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_gold, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_gold, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGold));
						break;
					case 5:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_cyan, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_cyan, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeCyan));
						break;
					case 6:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_pitch, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_pitch, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemePitch));
						break;
					case 7:
						((FrameLayout) viewHolder.itemView).setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_grey, context.getTheme()));
						viewHolder.unum_image_view.setForeground(context.getResources()
								.getDrawable(R.drawable.selected_grid_grey, context.getTheme()));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGrey));
						break;
				}
			} else {
				switch (theme) {
					case 0:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_gold));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_gold));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGold));
						break;
					case 1:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_cyan));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_cyan));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeCyan));
						break;
					case 2:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_pitch));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_pitch));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemePitch));
						break;
					case 3:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_grey));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_grey));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGrey));
						break;
					case 4:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_gold));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_gold));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGold));
						break;
					case 5:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_cyan));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_cyan));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeCyan));
						break;
					case 6:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_pitch));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_pitch));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemePitch));
						break;
					case 7:
						viewHolder.unum_image_view.setForeground(ContextCompat.getDrawable(context,R.drawable.selected_grid_grey));
						//((FrameLayout) viewHolder.itemView).setForeground(context.getResources().getDrawable(R.drawable.selected_grid_grey));
						viewHolder.imgInstaLogo.setColorFilter(ContextCompat.getColor(context,R.color.colorSchemeGrey));
						break;
				}
			}
		}else{
			((FrameLayout) viewHolder.itemView).setForeground(null);
			viewHolder.unum_image_view.setForeground(null);
		}

		//Do not show any background / border to insta images
		if (HomeBaseFragment.INSTA_IMAGE.equals(image.getName())) {
			viewHolder.view.setAlpha(0.0f);
			viewHolder.unum_image_view.setForeground(null);
			//viewHolder.unum_image_view.setBackground(null);
			((FrameLayout) viewHolder.itemView).setForeground(null);
		}

		//Show insta logo for Insta images
		if (HomeBaseFragment.INSTA_IMAGE.equals(image.getName())) {
			viewHolder.imgInstaLogo.setVisibility(View.VISIBLE);
		} else {
			viewHolder.imgInstaLogo.setVisibility(View.GONE);
		}
	}

	@Override public int getItemCount() {
		return images.size();
	}

	private boolean isSelected(Image image) {
		for (Image selectedImage : selectedImages) {
			if (selectedImage.getId() == image.getId()) {
				return true;
			}
		}

		return false;
	}

	public void setImageSize(int size) {
		this.size = size;
		imageSize = size - padding * 2;
	}

	public void clear() {
		images.clear();
		notifyDataSetChanged();
	}

	public void addAll(List<Image> images) {
		int startIndex = this.images.size();
		this.images.addAll(startIndex, images);
		notifyItemRangeInserted(startIndex, images.size());
	}

	public Image getItemAtPosition(int pos) {
		return images.get(pos);
	}

	public void addSelected(Image image) {
		selectedImages.add(image);
		notifyItemChanged(images.indexOf(image));
	}

	public void removeSelected(Image image) {
		selectedImages.remove(image);
		notifyItemChanged(images.indexOf(image));
	}

	public void removeSelectedPosition(int position, int clickPosition) {
		selectedImages.remove(position);
		notifyItemChanged(clickPosition);
	}

	public void removeAllExceptInstaImages() {
		ArrayList<Image> temp = new ArrayList<Image>(selectedImages);
		for (Image image : temp) {
			if (!HomeBaseFragment.INSTA_IMAGE.equals(image.getName())) {
				selectedImages.remove(image);
			}
		}
		notifyDataSetChanged();
	}

	public void removeInstaImageIfAny() {
		ArrayList<Image> temp = new ArrayList<Image>(selectedImages);
		for (Image image : temp) {
			if (HomeBaseFragment.INSTA_IMAGE.equals(image.getName())) {
				selectedImages.remove(image);
			}
		}
		notifyDataSetChanged();
	}

	public void removeAllSelectedSingleClick() {
		selectedImages.clear();
		notifyDataSetChanged();
	}

	public void updateImagePathForPosition(int position, String path) {
		images.get(position).setPath(path);
		notifyDataSetChanged();
	}

	public void updateMessageForImage(int pos, String msg) {
		images.get(pos).setMessage(msg);
		notifyDataSetChanged();
	}

	@Override public boolean onItemMove(int fromPosition, int toPosition) {
		//check if it is not insta image
		if (HomeBaseFragment.INSTA_IMAGE.equals(images.get(fromPosition).getName())) {
			return false;//dont move.
		}
		if (fromPosition < toPosition) {
			Image toMove = images.get(fromPosition);
			images.add(toPosition + 1, toMove);
			images.remove(fromPosition);
		} else {
			Image toMove = images.get(fromPosition);
			images.remove(fromPosition);
			images.add(toPosition, toMove);
		}
		notifyItemMoved(fromPosition, toPosition);
		//update all index with new index.
		for (int i = 0; i < images.size(); i++) {
			images.get(i).setId(i);
		}
		onItemClickListener.onItemMoved();
		return true;
	}

	@Override public void onItemDismiss(int position) {
		//swipe to dismiss not required.
	}

	public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {
		private final OnItemClickListener onItemClickListener;
		private final GestureDetector detector;
		private final GestureTap gestureTap;
		private UnumImageView unum_image_view;
		private ImageView imgInstaLogo;
		private View view;

		public ViewHolder(View itemView, OnItemClickListener onItemClickListener) {
			super(itemView);
			unum_image_view = (UnumImageView) itemView.findViewById(R.id.unum_image_view);
			imgInstaLogo = (ImageView) itemView.findViewById(R.id.imgInstaLogo);
			view = itemView.findViewById(R.id.view_alpha);
			this.onItemClickListener = onItemClickListener;
			itemView.setOnTouchListener(this);
			gestureTap = new GestureTap(onItemClickListener);
			detector = new GestureDetector(gestureTap);
		}

		@Override public boolean onTouch(View view, MotionEvent motionEvent) {
			gestureTap.setPosition(getAdapterPosition());
			gestureTap.setView(view);
			detector.onTouchEvent(motionEvent);

			return true;
		}

		public interface OnItemClickListener {
			void onSingleTap(int position);

			void onDoubleTap(int position);

			void onItemMoved();
		}
	}

	static class GestureTap extends GestureDetector.SimpleOnGestureListener {
		private final ViewHolder.OnItemClickListener onItemClickListener;
		private int position;
		private View view;

		GestureTap(ViewHolder.OnItemClickListener onItemClickListener) {
			this.onItemClickListener = onItemClickListener;
		}

		public void setPosition(int position) {
			this.position = position;
		}

		public void setView(View view) {
			this.view = view;
		}

		@Override public boolean onDoubleTap(MotionEvent e) {
			onItemClickListener.onDoubleTap(position);
			return true;
		}

		@Override public boolean onSingleTapConfirmed(MotionEvent e) {
			view.setSelected(true);
			onItemClickListener.onSingleTap(position);
			return true;
		}
	}
}
