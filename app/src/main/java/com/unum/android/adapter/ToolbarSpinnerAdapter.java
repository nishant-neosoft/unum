package com.unum.android.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;

/**
 * Created by Snehal on 8/30/2016.
 */
public class ToolbarSpinnerAdapter extends ArrayAdapter<String> {

	public Resources res;
	private LayoutInflater inflater;
	private Context context1;
	private ArrayList<String> data;
	private UnumSession unumSession;
	private int color = Color.BLACK;

	public ToolbarSpinnerAdapter(Context context, ArrayList<String> objects) {
		super(context, R.layout.list_item_toolbar_spinner, objects);
		context1 = context;
		data = objects;
		unumSession = new UnumSession(getContext());
		inflater = (LayoutInflater) context1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setData(ArrayList data) {
		//this.data = null;
		this.data = data;
		notifyDataSetChanged();
	}

	@Override public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	// This funtion called for each row ( Called data.size() times )
	public View getCustomView(int position, View convertView, ViewGroup parent) {
		View row = inflater.inflate(R.layout.list_item_toolbar_spinner, parent, false);
		//LoggedInstaUsers.User user = (LoggedInstaUsers.User)data.get( position );
		TextView txtUserName = (TextView) row.findViewById(R.id.txtUserName);
		txtUserName.setText(data.get(position));
		int theme = unumSession.getUNUM_DEFAULT_THEME();
		if(theme > 3){
			txtUserName.setTextColor(Color.WHITE);
			row.setBackgroundColor(Color.BLACK);
		}else{
			txtUserName.setTextColor(Color.BLACK);
			row.setBackgroundColor(Color.WHITE);
		}
		return row;
	}
	public void setTextColorTo(int txtColor){
		this.color = txtColor;
	}
}
