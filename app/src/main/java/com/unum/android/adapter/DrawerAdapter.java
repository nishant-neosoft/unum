package com.unum.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.data.model.DrawerItem;
import java.util.List;

/**
 * Created by webwerks on 22/8/16.
 */
public class DrawerAdapter extends BaseAdapter {
	Context context;
	List<DrawerItem> drawerMenu;
	int color = Color.BLACK;

	public DrawerAdapter(Context context, List<DrawerItem> drawerMenu) {
		this.drawerMenu = drawerMenu;
		this.context = context;
	}

	@Override

	public int getCount() {
		return drawerMenu.size();
	}

	@Override public Object getItem(int position) {
		return drawerMenu.get(position);
	}

	@Override public long getItemId(int position) {
		return drawerMenu.indexOf(getItem(position));
	}

	@Override public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater =
				(LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_drawer, null);
			holder = new ViewHolder();
			holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		DrawerItem drawerItem = (DrawerItem) getItem(position);
		holder.txtTitle.setText(drawerItem.getItem());
		holder.txtTitle.setTextColor(color);
		return convertView;
	}

	public void setTextColorTo(int txtColor) {
		this.color = txtColor;
	}

	public void setDrawerMenu(List<DrawerItem> drawerMenu) {
		this.drawerMenu = drawerMenu;
		notifyDataSetChanged();
	}

	private class ViewHolder {

		TextView txtTitle;
	}
}
