package com.unum.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.utils.DateUtil;
import java.util.List;

/**
 * Created by haridas on 24/8/16.
 */
public class ScheduleListAdapter extends BaseAdapter {
	private Context context;

	private List<GetUserReminderResponse> reminderList;

	public ScheduleListAdapter(Context reminderListActivity, List<GetUserReminderResponse> list) {
		context = reminderListActivity;
		this.reminderList = list;
	}

	@Override public int getCount() {
		return reminderList.size();
	}

	@Override public Object getItem(int i) {
		return null;
	}

	@Override public long getItemId(int i) {
		return 0;
	}


	@Override public View getView(int i, View view, ViewGroup viewGroup) {
		Holder holder;
		LayoutInflater layoutInflater;
		if (view == null) {
			layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.item_schedule_list, null);
			holder = new Holder();
			holder.txtTime = (TextView) view.findViewById(R.id.txtTime);
			holder.txtDays = (TextView) view.findViewById(R.id.txtDays);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		GetUserReminderResponse item = reminderList.get(i);
		//Date date = new Date(item.getTime());
		holder.txtTime.setText(DateUtil.getDisplayTime(item.getTime()));
		holder.txtDays.setText(item.getFrequency().getSelectedDays());
		return view;
	}

	public class Holder {
		TextView txtTime, txtDays;
	}
}
