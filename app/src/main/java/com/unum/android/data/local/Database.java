package com.unum.android.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.unum.android.instagram.LoggedInstaUsers;
import com.unum.android.utils.AppLogger;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

/* 
 * usage:  
 * DatabaseSetup.init(egActivityOrContext); 
 * DatabaseSetup.createEntry() or DatabaseSetup.getContactNames() or DatabaseSetup.getDb() 
 * DatabaseSetup.deactivate() then job done 
 */

public class Database extends SQLiteOpenHelper {
	public static final String ALARM_TABLE = "alarm";
	public static final String COLUMN_ALARM_ID = "_id";
	public static final String COLUMN_ALARM_ACTIVE = "alarm_active";
	public static final String COLUMN_ALARM_TIME = "alarm_time";
	public static final String COLUMN_ALARM_DAYS = "alarm_days";
	public static final String COLUMN_ALARM_TONE = "alarm_tone";
	public static final String COLUMN_ALARM_VIBRATE = "alarm_vibrate";
	public static final String COLUMN_ALARM_NAME = "alarm_name";
	public static final String USER_TABLE = "users";
	public static final String COLUMN_USER_ID = "_id";
	public static final String COLUMN_USER_INSTAGRAM_ID = "instagram_id";
	public static final String COLUMN_USER_INSTAGRAM_TOKEN = "instagram_token";
	public static final String COLUMN_USER_INSTAGRAM_PROFILE_PICTURE = "instagram_profile_picture";
	public static final String COLUMN_USER_INSTAGRAM_USER_NAME = "instagram_user_name";
	public static final String COLUMN_USER_INSTAGRAM_FULL_NAME = "instagram_full_name";
	public static final String COLUMN_USER_INSTAGRAM_FOLLWERS = "instagram_followers";
	public static final String COLUMN_USER_INSTAGRAM_FOLLOWING = "instagram_following";
	public static final String COLUMN_USER_INSTAGRAM_MEDIA = "instagram_media";
	public static final String COLUMN_USER_IS_ACTIVE = "is_active";
	public static final String COLUMN_UNUM_USER_ID = "unum_user_id";
	public static final String COLUMN_UNUM_USER_DATA = "unum_user_data";
	static final String DATABASE_NAME = "DB.db";
	static final int DATABASE_VERSION = 1;
	static Database instance = null;
	static SQLiteDatabase database = null;

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static void init(Context context) {
		if (null == instance) {
			instance = new Database(context);
		}
	}

	public static SQLiteDatabase getDatabase() {
		if (null == database) {
			database = instance.getWritableDatabase();
		}
		return database;
	}

	public static void deactivate() {
		if (null != database && database.isOpen()) {
			database.close();
		}
		database = null;
		instance = null;
	}

	public static long create(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
		cv.put(COLUMN_ALARM_TIME, alarm.getAlarmTimeString());

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = null;
			oos = new ObjectOutputStream(bos);
			oos.writeObject(alarm.getDays());
			byte[] buff = bos.toByteArray();

			cv.put(COLUMN_ALARM_DAYS, buff);
		} catch (Exception e) {
		}

		cv.put(COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
		cv.put(COLUMN_ALARM_VIBRATE, alarm.getVibrate());
		cv.put(COLUMN_ALARM_NAME, alarm.getAlarmName());

		return getDatabase().insert(ALARM_TABLE, null, cv);
	}

	public static int update(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
		cv.put(COLUMN_ALARM_TIME, alarm.getAlarmTimeString());

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = null;
			oos = new ObjectOutputStream(bos);
			oos.writeObject(alarm.getDays());
			byte[] buff = bos.toByteArray();

			cv.put(COLUMN_ALARM_DAYS, buff);
		} catch (Exception e) {
		}

		cv.put(COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
		cv.put(COLUMN_ALARM_VIBRATE, alarm.getVibrate());
		cv.put(COLUMN_ALARM_NAME, alarm.getAlarmName());

		return getDatabase().update(ALARM_TABLE, cv, "_id=" + alarm.getId(), null);
	}

	public static int deleteEntry(Alarm alarm) {
		return deleteEntry(alarm.getId());
	}

	public static int deleteEntry(int id) {
		return getDatabase().delete(ALARM_TABLE, COLUMN_ALARM_ID + "=" + id, null);
	}

	public static int deleteAll() {
		return getDatabase().delete(ALARM_TABLE, "1", null);
	}

	public static Alarm getAlarm(int id) {
		// TODO Auto-generated method stub
		String[] columns = new String[] {
				COLUMN_ALARM_ID, COLUMN_ALARM_ACTIVE, COLUMN_ALARM_TIME, COLUMN_ALARM_DAYS,
				COLUMN_ALARM_TONE, COLUMN_ALARM_VIBRATE, COLUMN_ALARM_NAME
		};
		Cursor c =
				getDatabase().query(ALARM_TABLE, columns, COLUMN_ALARM_ID + "=" + id, null, null, null,
						null);
		Alarm alarm = null;

		if (c.moveToFirst()) {

			alarm = new Alarm();
			alarm.setId(c.getInt(1));
			alarm.setAlarmActive(c.getInt(2) == 1);
			alarm.setAlarmTime(c.getString(3));
			byte[] repeatDaysBytes = c.getBlob(4);

			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(repeatDaysBytes);
			try {
				ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
				Alarm.Day[] repeatDays;
				Object object = objectInputStream.readObject();
				if (object instanceof Alarm.Day[]) {
					repeatDays = (Alarm.Day[]) object;
					alarm.setDays(repeatDays);
				}
			} catch (StreamCorruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			alarm.setAlarmTonePath(c.getString(6));
			alarm.setVibrate(c.getInt(7) == 1);
			alarm.setAlarmName(c.getString(8));
		}
		c.close();
		return alarm;
	}

	public static Cursor getCursor() {
		// TODO Auto-generated method stub
		String[] columns = new String[] {
				COLUMN_ALARM_ID, COLUMN_ALARM_ACTIVE, COLUMN_ALARM_TIME, COLUMN_ALARM_DAYS,
				COLUMN_ALARM_TONE, COLUMN_ALARM_VIBRATE, COLUMN_ALARM_NAME
		};
		return getDatabase().query(ALARM_TABLE, columns, null, null, null, null, null);
	}

	public static List<Alarm> getAll() {
		List<Alarm> alarms = new ArrayList<Alarm>();
		Cursor cursor = Database.getCursor();
		if (cursor.moveToFirst()) {

			do {
				// COLUMN_ALARM_ID,
				// COLUMN_ALARM_ACTIVE,
				// COLUMN_ALARM_TIME,
				// COLUMN_ALARM_DAYS,
				// COLUMN_ALARM_DIFFICULTY,
				// COLUMN_ALARM_TONE,
				// COLUMN_ALARM_VIBRATE,
				// COLUMN_ALARM_NAME

				Alarm alarm = new Alarm();
				alarm.setId(cursor.getInt(0));
				alarm.setAlarmActive(cursor.getInt(1) == 1);
				alarm.setAlarmTime(cursor.getString(2));
				byte[] repeatDaysBytes = cursor.getBlob(3);

				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(repeatDaysBytes);
				try {
					ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
					Alarm.Day[] repeatDays;
					Object object = objectInputStream.readObject();
					if (object instanceof Alarm.Day[]) {
						repeatDays = (Alarm.Day[]) object;
						alarm.setDays(repeatDays);
					}
				} catch (StreamCorruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				alarm.setAlarmTonePath(cursor.getString(5));
				alarm.setVibrate(cursor.getInt(6) == 1);
				//	alarm.setAlarmName(cursor.getString(7));

				alarms.add(alarm);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return alarms;
	}

	public static long insertUserDetails(String instagramUserId, String instagramUserToken,
			String instagramProfilePicture, String instagramUserName, String instagramFullName,
			int instagramUserFollowers, int instagramUserFollowing, int instagramUserMedia,
			int isUserActive, String userId, String userData) {
		long rowId = 0;

		AppLogger.print("INserting NEW USER = "+userId+"/"+instagramUserName);

		ContentValues cv = new ContentValues();

		try {
			cv.put(COLUMN_USER_INSTAGRAM_ID, instagramUserId);
			cv.put(COLUMN_USER_INSTAGRAM_TOKEN, instagramUserToken);
			cv.put(COLUMN_USER_INSTAGRAM_PROFILE_PICTURE, instagramProfilePicture);
			cv.put(COLUMN_USER_INSTAGRAM_USER_NAME, instagramUserName);
			cv.put(COLUMN_USER_INSTAGRAM_FULL_NAME, instagramFullName);
			cv.put(COLUMN_USER_INSTAGRAM_FOLLWERS, instagramUserFollowers);
			cv.put(COLUMN_USER_INSTAGRAM_FOLLOWING, instagramUserFollowing);
			cv.put(COLUMN_USER_INSTAGRAM_MEDIA, instagramUserMedia);
			cv.put(COLUMN_USER_IS_ACTIVE, isUserActive);
			cv.put(COLUMN_UNUM_USER_ID, userId);
			cv.put(COLUMN_UNUM_USER_DATA, userData);

			rowId = getDatabase().insert(USER_TABLE, null, cv);

			Log.e("TABLE_USER_INSERT", cv.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowId;
	}

	public static Cursor getValues(String Table_Name, String[] coloum, String where) {

		Cursor cv = null;
		try {

			cv = getDatabase().query(Table_Name, coloum, where, null, null, null, null);
		} catch (Exception e) {
			Log.e("UNUM GET VALUES", e.toString());
		}
		return cv;
	}

	public static void deleteAllUsers() {
		try {
			getDatabase().delete(USER_TABLE, null, null);
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("DELETE USERS", "" + e);
		}
	}

	public static void deleteSingleUser(String unumUserId) {
		try {
			int i =
					getDatabase().delete(USER_TABLE, COLUMN_UNUM_USER_ID + "=?", new String[] { unumUserId });
			Log.d("TAG", "Row deleted " + i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static LoggedInstaUsers.User getActiveUser() {
		Log.d("TAG", "Get Active user");
		LoggedInstaUsers.User activeUser = null;
		String where = COLUMN_USER_IS_ACTIVE + " = 1";
		Cursor userCursor = Database.getValues(USER_TABLE, null, where);

		String accessToken = "", id = "", userName = "", fullName = "";
		boolean isActive = false;
		int status = 1;

		Log.d("TAG", "userCursor.getCount()=" + userCursor.getCount());

		if (!(userCursor == null) && userCursor.getCount() > 0) {
			if (userCursor.moveToNext()) {
				do {
					accessToken =
							userCursor.getString(userCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_TOKEN));
					id = userCursor.getString(userCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_ID));
					userName =
							userCursor.getString(userCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_USER_NAME));
					fullName =
							userCursor.getString(userCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_FULL_NAME));
					status = userCursor.getInt(userCursor.getColumnIndex(COLUMN_USER_IS_ACTIVE));

					if (status == 1) {
						isActive = true;
					} else {
						isActive = false;
					}

					Log.d("TAG", "userName = " + userName);

					activeUser =
							new LoggedInstaUsers().new User(accessToken, id, userName, fullName, isActive);
				} while (userCursor.moveToNext());
			}
		}
		return activeUser;
	}

	public static ArrayList<LoggedInstaUsers.User> getAllUsers() {
		Cursor mCursor = Database.getValues(USER_TABLE, null, null);
		ArrayList<LoggedInstaUsers.User> mUsers = new ArrayList<>(mCursor.getCount());

		String accessToken = "", id = "", userName = "", fullName = "";
		boolean isActive = false;
		int status = 1;

		if (!(mCursor == null) && mCursor.getCount() > 0) {
			if (mCursor.moveToNext()) {
				do {
					accessToken = mCursor.getString(mCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_TOKEN));
					id = mCursor.getString(mCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_ID));
					userName = mCursor.getString(mCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_USER_NAME));
					fullName = mCursor.getString(mCursor.getColumnIndex(COLUMN_USER_INSTAGRAM_FULL_NAME));
					status = mCursor.getInt(mCursor.getColumnIndex(COLUMN_USER_IS_ACTIVE));

					if (status == 1) {
						isActive = true;
					} else {
						isActive = false;
					}

					mUsers.add(
							new LoggedInstaUsers().new User(accessToken, id, userName, fullName, isActive));
				} while (mCursor.moveToNext());
			}
		}

		return mUsers;
	}

	public static void changeuserStatus() {
		Cursor mCursor = Database.getValues(USER_TABLE, null, null);
		ContentValues cv = new ContentValues();
		if (!(mCursor == null) && mCursor.getCount() > 0) {
			if (mCursor.moveToNext()) {
				do {
					cv.put(COLUMN_USER_IS_ACTIVE, 0);
					getDatabase().update(USER_TABLE, cv, null, null);
					Log.e("DABASE UPDATE", cv.toString());
				} while (mCursor.moveToNext());
			}
		}
	}

	@Override public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ ALARM_TABLE
				+ " ( "
				+ COLUMN_ALARM_ID
				+ " INTEGER primary key autoincrement, "
				+ COLUMN_ALARM_ACTIVE
				+ " INTEGER NOT NULL, "
				+ COLUMN_ALARM_TIME
				+ " TEXT NOT NULL, "
				+ COLUMN_ALARM_DAYS
				+ " BLOB NOT NULL, "
				+ COLUMN_ALARM_TONE
				+ " TEXT NOT NULL, "
				+ COLUMN_ALARM_VIBRATE
				+ " INTEGER NOT NULL, "
				+ COLUMN_ALARM_NAME
				+ " TEXT NOT NULL)");

		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ USER_TABLE
				+ " ( "
				+ COLUMN_USER_ID
				+ " INTEGER primary key autoincrement, "
				+ COLUMN_USER_INSTAGRAM_ID
				+ " TEXT NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_TOKEN
				+ " TEXT NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_PROFILE_PICTURE
				+ " TEXT NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_USER_NAME
				+ " TEXT NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_FULL_NAME
				+ " TEXT NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_FOLLWERS
				+ " INTEGER NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_FOLLOWING
				+ " INTEGER NOT NULL, "
				+ COLUMN_USER_INSTAGRAM_MEDIA
				+ " INTEGER NOT NULL, "
				+ COLUMN_USER_IS_ACTIVE
				+ " INTEGER NOT NULL, "
				+ COLUMN_UNUM_USER_ID
				+ " TEXT NOT NULL, "
				+ COLUMN_UNUM_USER_DATA
				+ " TEXT NOT NULL, "
				+ "UNIQUE ("
				+ COLUMN_UNUM_USER_ID
				+ ")"
				+ " ON CONFLICT REPLACE)");
	}

	@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + ALARM_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
		onCreate(db);
	}
}