package com.unum.android.data.server;

import com.unum.android.data.model.request.CreatePostRequest;
import com.unum.android.data.model.request.CreateReminderRequest;
import com.unum.android.data.model.request.DeletePostsRequest;
import com.unum.android.data.model.request.DeleteReminderRequest;
import com.unum.android.data.model.request.DeleteScheduleRequest;
import com.unum.android.data.model.request.GetImageResoueceRequest;
import com.unum.android.data.model.request.LoginSignupRequest;
import com.unum.android.data.model.request.UpdateDeviceTokenRequest;
import com.unum.android.data.model.request.UpdateMultiplePost;
import com.unum.android.data.model.request.UpdateReminderRequest;
import com.unum.android.data.model.request.UpdateScheduleRequest;
import com.unum.android.data.model.response.CreatePostResponse;
import com.unum.android.data.model.response.GetAllSchedulesResponse;
import com.unum.android.data.model.response.GetImageResourcesResponse;
import com.unum.android.data.model.response.GetUserPostsResponse;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.data.model.response.LoginSignupResponse;
import com.unum.android.data.model.response.UpdateProfileResponse;
import com.unum.android.data.model.response.UpdateSinglePostResponse;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Dhaval Parmar on 22-Aug-16.
 */
public interface UnumService {

	@Headers(UnumConstants.API_CONTENT_TYPE) @POST("/v1/login/instagram")
	Call<LoginSignupResponse> loginUser(@Body LoginSignupRequest mLoginSignupRequest);

	@Headers(UnumConstants.API_CONTENT_TYPE) @POST("/v1/users") Call<LoginSignupResponse> signUpUser(
			@Body LoginSignupRequest mLoginSignupRequest);

	@PUT("/v1/users/{id}") Call<UpdateProfileResponse> updateUserInfoPut(@Path("id") String id,
			@Query("userName") String username, @Query("fullName") String fullName,
			@Query("numPosts") int numPosts, @Query("numFollowers") int numFollowers,
			@Query("numFollowing") int numFollowing, @Query("liveGridEnabled") boolean liveGridEnabled,
			@Query("UTCOffset") long UTCOffset);

	@GET("/v1/logout") Call<ResponseBody> logoutUser();

	@GET("/v1/self") Call<LoginSignupResponse> getSelfInformation();

	@PUT("/v1/self/sessions/{id}") Call<ResponseBody> updateDeviceTokenPut(@Path("id") String id,
			@Body UpdateDeviceTokenRequest pushToken);

	@POST("/v1/resources?addPolicy=true&acl=public-read")
	Call<GetImageResourcesResponse> getImageResources(
			@Body GetImageResoueceRequest mGetImageResoueceRequest);

	@POST("/v1/posts") Call<CreatePostResponse> createPost(
			@Body CreatePostRequest mCreatePostRequest);

	@GET("/v1/posts") Call<List<GetUserPostsResponse>> getUserPosts(@Query("creator") String creator);

	@PUT("/v1/posts/{id}") Call<UpdateSinglePostResponse> updateSinglePost(@Path("id") String postId,
			@Body UpdateMultiplePost.Posts mUpdateSinglePostRequest);

	@POST("/v1/posts/updates/") Call<ResponseBody> updateMultiplePost(
			@Body UpdateMultiplePost mUpdateMultiplePost);

	@HTTP(method = "DELETE", path = "/v2/posts/", hasBody = true) Call<ResponseBody> deletePost(
			@Body DeletePostsRequest mDeletePostsRequest);

	@GET("/v1/schedules") Call<List<GetUserReminderResponse>> getAllReminders();

	@POST("/v1/schedules") Call<ResponseBody> createReminder(
			@Body CreateReminderRequest updateReminderRequest);

	@HTTP(method = "DELETE", path = "/v1/schedules", hasBody = true)
	Call<ResponseBody> deleteReminder(@Body DeleteReminderRequest deleteReminderRequest);

	@POST("/v1/schedules/update") Call<ResponseBody> updateReminder(
			@Body UpdateReminderRequest updateReminderRequest);

	@GET("/v1/schedules/custom") Call<List<GetAllSchedulesResponse>> getAllSchedule();

	@HTTP(method = "DELETE", path = "/v1/schedules/custom", hasBody = true)
	Call<ResponseBody> deleteSchedule(@Body DeleteScheduleRequest deleteScheduleRequest);

	@POST("/v1/schedules/custom/update") Call<ResponseBody> updateSchedule(
			@Body UpdateScheduleRequest updateScheduleRequest);

	@POST("/v1/drafts/{id}/makeDefault") Call<LoginSignupResponse> updateDraft(
			@Header("Authorization") String basic, @Path("id") String id);
}

