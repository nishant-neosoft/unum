package com.unum.android.data.model.request;

/**
 * Created by Snehal on 9/20/2016.
 */
public class UpdateReminderRequest {

	String scheduleId, time;
	Frequency frequency;

	public UpdateReminderRequest(String scheduleId, String time, Frequency frequency){
		this.scheduleId = scheduleId;
		this.time = time;
		this.frequency = frequency;
	}
}
