package com.unum.android.data.model.request;

/**
 * Created by Dhaval Parmar on 22/8/16.
 * <p/>
 * Body request for signup and login process
 */
public class LoginSignupRequest {
	String type, instagramId, instagramToken, username, fullName;
	int numPosts, numFollowers, numFollowing;

	/**
	 * Pass the values to constructor for signup / login process
	 *
	 * @param type type of device
	 * @param instagramId id which got from instagram login API
	 * @param instagramToken token provided from instagram login API
	 * @param username username provided from instagram login API
	 * @param fullName full name of user provided from instagram login API
	 * @param numPosts total number of posts provided from instagram login API
	 * @param numFollowers total number of followers provided from instagram login API
	 * @param numFollowing total number of following provided from instagram login API
	 */
	public LoginSignupRequest(String type, String instagramId, String instagramToken, String username,
			String fullName, int numPosts, int numFollowers, int numFollowing) {
		this.type = type;
		this.instagramId = instagramId;
		this.instagramToken = instagramToken;
		this.username = username;
		this.fullName = fullName;
		this.numPosts = numPosts;
		this.numFollowers = numFollowers;
		this.numFollowing = numFollowing;
	}
}
