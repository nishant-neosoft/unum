package com.unum.android.data.model.response;

/**
 * Created by Snehal on 9/23/2016.
 */
public class GetAllSchedulesResponse {
	int __v, index, priority, progress;
	Data data;
	Attempts attempts;
	String id, type, state, created_at, promote_at, updated_at, delay, ttl;

	public class Data {
		public String userId, postId;
	}

	public class Attempts {
		public int made, remaining, max;
	}

	public int get__v() {
		return __v;
	}

	public Attempts getAttempts() {
		return attempts;
	}

	public Data getData() {
		return data;
	}

	public int getIndex() {
		return index;
	}

	public int getPriority() {
		return priority;
	}

	public int getProgress() {
		return progress;
	}

	public String getCreated_at() {
		return created_at;
	}

	public String getDelay() {
		return delay;
	}

	public String getId() {
		return id;
	}

	public String getPromote_at() {
		return promote_at;
	}

	public String getState() {
		return state;
	}

	public String getTtl() {
		return ttl;
	}

	public String getType() {
		return type;
	}

	public String getUpdated_at() {
		return updated_at;
	}
}
