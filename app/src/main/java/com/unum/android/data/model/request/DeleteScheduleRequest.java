package com.unum.android.data.model.request;

import java.util.ArrayList;

/**
 * Created by Snehal on 9/20/2016.
 */
public class DeleteScheduleRequest {
	ArrayList<String> postIds;

	public DeleteScheduleRequest(ArrayList<String> deleteArray) {
		this.postIds = deleteArray;
	}
}
