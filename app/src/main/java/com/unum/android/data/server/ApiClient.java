package com.unum.android.data.server;

import android.util.Base64;
import android.util.Log;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nishant Shah on 22-Aug-16.
 */
public class ApiClient {
	public static final String BASE_URL = "https://unum-api.herokuapp.com";
	//public static final String BASE_URL = "https://unum-test.herokuapp.com";
	private static Retrofit retrofit = null;

	public static Retrofit getClient() {
		if (retrofit == null) {

			OkHttpClient client =
					new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();

			retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
					.client(client)
					.addConverterFactory(GsonConverterFactory.create())
					.build();
		}

		//        UnumService service = retrofit.create(UnumService.class);
		//        return service;

		return retrofit;
	}

	public static Retrofit getClientWithAuth(final String userName, final String passWord) {

		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.addNetworkInterceptor(new StethoInterceptor());
		Retrofit.Builder builder =
				new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create());

		String credentials = userName + ":" + passWord;
		final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

		Log.e("BASIC AUTH", basic);

		httpClient.addInterceptor(new Interceptor() {
			@Override public Response intercept(Interceptor.Chain chain) throws IOException {
				Request original = chain.request();

				Request.Builder requestBuilder = original.newBuilder()
						.header("Authorization", basic)
						.header("Content-Type", "application/json")
						.method(original.method(), original.body());

				Request request = requestBuilder.build();
				return chain.proceed(request);
			}
		});

		OkHttpClient client = httpClient.build();
		retrofit = builder.client(client).build();
		return retrofit;
	}
}
