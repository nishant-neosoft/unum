package com.unum.android.data.model.response;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by Dhaval Parmar on 24/8/16.
 */
public class GetImageResourcesResponse {
	int __v;
	String creator, name, description, _id, createdDate;
	ArrayList<Files> files;

	public GetImageResourcesResponse(int __v, String creator, String name, String description,
			String _id, String createdDate, ArrayList<Files> files) {
		this.__v = __v;
		this.creator = creator;
		this.name = name;
		this.description = description;
		this._id = _id;
		this.createdDate = createdDate;
		this.files = files;
	}

	public int get__v() {
		return __v;
	}

	public void set__v(int __v) {
		this.__v = __v;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public ArrayList<Files> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<Files> files) {
		this.files = files;
	}

	/**
	 * For files ArrayList in get resources Process
	 */
	public class Files {

		Meta meta;
		String url, type, key, acl, _id, status, createdDate;
		int size;

		public Files(Meta meta, String url, String type, String key, String acl, String _id,
				String status, String createdDate, int size) {
			this.meta = meta;
			this.url = url;
			this.type = type;
			this.key = key;
			this.acl = acl;
			this._id = _id;
			this.status = status;
			this.createdDate = createdDate;
			this.size = size;
		}

		public Meta getMeta() {
			return meta;
		}

		public void setMeta(Meta meta) {
			this.meta = meta;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getAcl() {
			return acl;
		}

		public void setAcl(String acl) {
			this.acl = acl;
		}

		public String get_id() {
			return _id;
		}

		public void set_id(String _id) {
			this._id = _id;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		/**
		 * For meta of files in get resources Process
		 */
		public class Meta {
			String uploadUrl;
			Policy policy;

			public Meta(String uploadUrl, Policy policy) {
				this.uploadUrl = uploadUrl;
				this.policy = policy;
			}

			public String getUploadUrl() {
				return uploadUrl;
			}

			public void setUploadUrl(String uploadUrl) {
				this.uploadUrl = uploadUrl;
			}

			public Policy getPolicy() {
				return policy;
			}

			public void setPolicy(Policy policy) {
				this.policy = policy;
			}

			/**
			 * For policy of meta tag for files in get resources Process
			 */
			public class Policy {
				@SerializedName("Content-Type") String Contentype;
				String signature, policy, acl, AWSAccessKeyId, key;

				public Policy(String contentype, String signature, String policy, String acl,
						String AWSAccessKeyId, String key) {
					Contentype = contentype;
					this.signature = signature;
					this.policy = policy;
					this.acl = acl;
					this.AWSAccessKeyId = AWSAccessKeyId;
					this.key = key;
				}

				public String getContentype() {
					return Contentype;
				}

				public void setContentype(String contentype) {
					Contentype = contentype;
				}

				public String getSignature() {
					return signature;
				}

				public void setSignature(String signature) {
					this.signature = signature;
				}

				public String getPolicy() {
					return policy;
				}

				public void setPolicy(String policy) {
					this.policy = policy;
				}

				public String getAcl() {
					return acl;
				}

				public void setAcl(String acl) {
					this.acl = acl;
				}

				public String getAWSAccessKeyId() {
					return AWSAccessKeyId;
				}

				public void setAWSAccessKeyId(String AWSAccessKeyId) {
					this.AWSAccessKeyId = AWSAccessKeyId;
				}

				public String getKey() {
					return key;
				}

				public void setKey(String key) {
					this.key = key;
				}
			}
		}
	}
}
