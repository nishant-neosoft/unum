package com.unum.android.data.model.request;

/**
 * Created by Snehal on 9/20/2016.
 */
public class CreateReminderRequest {

	String time;
	Frequency frequency;

	public CreateReminderRequest(String time, Frequency frequency){
		this.time = time;
		this.frequency = frequency;
	}
}
