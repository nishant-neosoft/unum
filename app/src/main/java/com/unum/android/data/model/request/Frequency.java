package com.unum.android.data.model.request;

public class Frequency {
	boolean day0, day1, day2, day3, day4, day5, day6;

	public Frequency(boolean day0, boolean day1, boolean day2, boolean day3, boolean day4,
			boolean day5, boolean day6) {
		this.day0 = day0;
		this.day1 = day1;
		this.day2 = day2;
		this.day3 = day3;
		this.day4 = day4;
		this.day5 = day5;
		this.day6 = day6;
	}
}