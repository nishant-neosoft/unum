package com.unum.android.data.model.request;

import java.util.ArrayList;

/**
 * Created by Snehal on 9/20/2016.
 */
public class DeleteReminderRequest {
	ArrayList<String> scheduleIds;

	public DeleteReminderRequest(ArrayList<String> deleteArray) {
		this.scheduleIds = deleteArray;
	}
}
