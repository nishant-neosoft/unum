package com.unum.android.data.model.request;

import java.util.ArrayList;

/**
 * Created by Dhaval Parmar on 8/9/16.
 */
public class UpdateMultiplePost {

	ArrayList<Posts> arr;

	public UpdateMultiplePost() {

	}

	public UpdateMultiplePost(ArrayList<Posts> arr) {
		this.arr = arr;
	}

	public class Posts {
		String _id, draft, imageUrl, message;
		int index;

		public Posts(String _id, String draft, String imageUrl, String message, int index) {
			this._id = _id;
			this.draft = draft;
			this.imageUrl = imageUrl;
			this.message = message;
			this.index = index;
		}
	}
}
