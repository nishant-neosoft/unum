package com.unum.android.data.model;

/**
 * Created by webwerks on 22/8/16.
 */
public class DrawerItem {
	String item;

	public DrawerItem(String item) {
		this.item = item;
	}

	public String getItem() {
		return this.item;
	}

	public void setItem(String item) {
		this.item = item;
	}
}
