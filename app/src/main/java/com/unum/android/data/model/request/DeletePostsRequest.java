package com.unum.android.data.model.request;

import java.util.ArrayList;

/**
 * Created by Dhaval Parmar on 9/9/16.
 */
public class DeletePostsRequest {
	ArrayList<String> deleteArray;

	public DeletePostsRequest(ArrayList<String> deleteArray) {
		this.deleteArray = deleteArray;
	}
}
