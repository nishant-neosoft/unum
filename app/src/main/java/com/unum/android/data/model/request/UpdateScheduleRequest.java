package com.unum.android.data.model.request;

import java.util.ArrayList;

/**
 * Created by Snehal on 9/20/2016.
 */
public class UpdateScheduleRequest {

	String date;
	ArrayList<String> postIds;

	public UpdateScheduleRequest(String date,ArrayList<String> postIds){
		this.date = date;
		this.postIds = postIds;
	}
}
