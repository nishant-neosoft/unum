package com.unum.android.data.model.request;

/**
 * Created by Dhaval Parmar on 24/8/16.
 * <p/>
 * Body request for update Device Token
 */
public class UpdateDeviceTokenRequest {
	String pushToken;

	/**
	 * Pass the values to constructor for update device token process
	 *
	 * @param pushToken The Device token of the device (GCM/FCM Instance ID)
	 */
	public UpdateDeviceTokenRequest(String pushToken) {
		this.pushToken = pushToken;
	}
}
