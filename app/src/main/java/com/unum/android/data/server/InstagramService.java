package com.unum.android.data.server;

import com.unum.android.data.model.InstagramImages;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Nishant Shah on 22-Aug-16.
 */
public interface InstagramService {

	@GET("/v1/users/{id}/media/recent/") Call<InstagramImages> getAllImages(@Path("id") String id,
			@Query("access_token") String accessToken);
}
