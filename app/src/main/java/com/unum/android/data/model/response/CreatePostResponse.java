package com.unum.android.data.model.response;

import java.util.ArrayList;

/**
 * Created by Dhaval Parmar on 26/8/16.
 */
public class CreatePostResponse {
	int __v, index;
	Creator creator;
	String message, imageUrl, draft, _id, createdDate;

	public CreatePostResponse(int __v, int index, Creator creator, String message, String imageUrl,
			String draft, String _id, String createdDate) {
		this.__v = __v;
		this.index = index;
		this.creator = creator;
		this.message = message;
		this.imageUrl = imageUrl;
		this.draft = draft;
		this._id = _id;
		this.createdDate = createdDate;
	}

	public int get__v() {
		return __v;
	}

	public void set__v(int __v) {
		this.__v = __v;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Creator getCreator() {
		return creator;
	}

	public void setCreator(Creator creator) {
		this.creator = creator;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public class Creator {
		String _id, braintreeCustomerId, username, fullName, instagramId, instagramToken, createdDate;
		int __v, UTCOffset, numTiles, numFollowing, numFollowers, numPosts;
		boolean liveGridEnabled;
		ArrayList<Drafts> drafts;
		ArrayList<NotificationsSettings> notificationSettings;
		ArrayList<Sessions> sessions;

		public Creator(String _id, String braintreeCustomerId, String username, String fullName,
				String instagramId, String instagramToken, String createdDate, int __v, int UTCOffset,
				int numTiles, int numFollowing, int numFollowers, int numPosts, boolean liveGridEnabled,
				ArrayList<Drafts> drafts, ArrayList<NotificationsSettings> notificationSettings,
				ArrayList<Sessions> sessions) {
			this._id = _id;
			this.braintreeCustomerId = braintreeCustomerId;
			this.username = username;
			this.fullName = fullName;
			this.instagramId = instagramId;
			this.instagramToken = instagramToken;
			this.createdDate = createdDate;
			this.__v = __v;
			this.UTCOffset = UTCOffset;
			this.numTiles = numTiles;
			this.numFollowing = numFollowing;
			this.numFollowers = numFollowers;
			this.numPosts = numPosts;
			this.liveGridEnabled = liveGridEnabled;
			this.drafts = drafts;
			this.notificationSettings = notificationSettings;
			this.sessions = sessions;
		}

		public String get_id() {
			return _id;
		}

		public void set_id(String _id) {
			this._id = _id;
		}

		public String getBraintreeCustomerId() {
			return braintreeCustomerId;
		}

		public void setBraintreeCustomerId(String braintreeCustomerId) {
			this.braintreeCustomerId = braintreeCustomerId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getFullName() {
			return fullName;
		}

		public void setFullName(String fullName) {
			this.fullName = fullName;
		}

		public String getInstagramId() {
			return instagramId;
		}

		public void setInstagramId(String instagramId) {
			this.instagramId = instagramId;
		}

		public String getInstagramToken() {
			return instagramToken;
		}

		public void setInstagramToken(String instagramToken) {
			this.instagramToken = instagramToken;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public int get__v() {
			return __v;
		}

		public void set__v(int __v) {
			this.__v = __v;
		}

		public int getUTCOffset() {
			return UTCOffset;
		}

		public void setUTCOffset(int UTCOffset) {
			this.UTCOffset = UTCOffset;
		}

		public int getNumTiles() {
			return numTiles;
		}

		public void setNumTiles(int numTiles) {
			this.numTiles = numTiles;
		}

		public int getNumFollowing() {
			return numFollowing;
		}

		public void setNumFollowing(int numFollowing) {
			this.numFollowing = numFollowing;
		}

		public int getNumFollowers() {
			return numFollowers;
		}

		public void setNumFollowers(int numFollowers) {
			this.numFollowers = numFollowers;
		}

		public int getNumPosts() {
			return numPosts;
		}

		public void setNumPosts(int numPosts) {
			this.numPosts = numPosts;
		}

		public boolean isLiveGridEnabled() {
			return liveGridEnabled;
		}

		public void setLiveGridEnabled(boolean liveGridEnabled) {
			this.liveGridEnabled = liveGridEnabled;
		}

		public ArrayList<Drafts> getDrafts() {
			return drafts;
		}

		public void setDrafts(ArrayList<Drafts> drafts) {
			this.drafts = drafts;
		}

		public ArrayList<NotificationsSettings> getNotificationSettings() {
			return notificationSettings;
		}

		public void setNotificationSettings(ArrayList<NotificationsSettings> notificationSettings) {
			this.notificationSettings = notificationSettings;
		}

		public ArrayList<Sessions> getSessions() {
			return sessions;
		}

		public void setSessions(ArrayList<Sessions> sessions) {
			this.sessions = sessions;
		}

		/**
		 * Creator's Drafts
		 */
		public class Drafts {
			String _id, createdDate;
			boolean isDefault;

			public Drafts(String _id, String createdDate, boolean isDefault) {
				this._id = _id;
				this.createdDate = createdDate;
				this.isDefault = isDefault;
			}

			public String get_id() {
				return _id;
			}

			public void set_id(String _id) {
				this._id = _id;
			}

			public String getCreatedDate() {
				return createdDate;
			}

			public void setCreatedDate(String createdDate) {
				this.createdDate = createdDate;
			}

			public boolean isDefault() {
				return isDefault;
			}

			public void setDefault(boolean aDefault) {
				isDefault = aDefault;
			}
		}

		/**
		 * Creator's Notifications Settings
		 */
		public class NotificationsSettings {
			String _id;
			int frequency;
			boolean evening, midday, morning;

			public NotificationsSettings(String _id, int frequency, boolean evening, boolean midday,
					boolean morning) {
				this._id = _id;
				this.frequency = frequency;
				this.evening = evening;
				this.midday = midday;
				this.morning = morning;
			}

			public String get_id() {
				return _id;
			}

			public void set_id(String _id) {
				this._id = _id;
			}

			public int getFrequency() {
				return frequency;
			}

			public void setFrequency(int frequency) {
				this.frequency = frequency;
			}

			public boolean isEvening() {
				return evening;
			}

			public void setEvening(boolean evening) {
				this.evening = evening;
			}

			public boolean isMidday() {
				return midday;
			}

			public void setMidday(boolean midday) {
				this.midday = midday;
			}

			public boolean isMorning() {
				return morning;
			}

			public void setMorning(boolean morning) {
				this.morning = morning;
			}
		}

		/**
		 * Creator's Sessions
		 */
		public class Sessions {
			String type, _id, lastActiveDate, createdDate;

			public Sessions(String type, String _id, String lastActiveDate, String createdDate) {
				this.type = type;
				this._id = _id;
				this.lastActiveDate = lastActiveDate;
				this.createdDate = createdDate;
			}

			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			public String get_id() {
				return _id;
			}

			public void set_id(String _id) {
				this._id = _id;
			}

			public String getLastActiveDate() {
				return lastActiveDate;
			}

			public void setLastActiveDate(String lastActiveDate) {
				this.lastActiveDate = lastActiveDate;
			}

			public String getCreatedDate() {
				return createdDate;
			}

			public void setCreatedDate(String createdDate) {
				this.createdDate = createdDate;
			}
		}
	}
}
