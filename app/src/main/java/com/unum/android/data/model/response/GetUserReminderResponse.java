package com.unum.android.data.model.response;

import java.util.Date;

/**
 * Created by Snehal on 9/20/2016.
 */
public class GetUserReminderResponse {

	Date time;
	String _id, owner, createdDate;
	int __v;
	Frequency frequency;

	public class Frequency {
		boolean day0, day1, day2, day3, day4, day5, day6;

		public Frequency(boolean day0, boolean day1, boolean day2, boolean day3, boolean day4,
				boolean day5, boolean day6) {
			this.day0 = day0;
			this.day1 = day1;
			this.day2 = day2;
			this.day3 = day3;
			this.day4 = day4;
			this.day5 = day5;
			this.day6 = day6;
		}

		public boolean isDay0() {
			return day0;
		}

		public boolean isDay1() {
			return day1;
		}

		public boolean isDay2() {
			return day2;
		}

		public boolean isDay3() {
			return day3;
		}

		public boolean isDay4() {
			return day4;
		}

		public boolean isDay5() {
			return day5;
		}

		public boolean isDay6() {
			return day6;
		}

		public String getSelectedDays(){
			StringBuffer days = new StringBuffer("");
			if(day0){
				days.append("Mondays");
			}
			if(day1){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Tuesdays");
			}
			if(day2){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Wednesdays");
			}
			if(day3){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Thursdays");
			}
			if(day4){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Fridays");
			}
			if(day5){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Saturdays");
			}
			if(day6){
				if(days.length() > 0){
					days.append(",");
				}
				days.append("Sundays");
			}

			if(day0 && day1 && day2 && day3 && day4 && day5 && day6){
				days.setLength(0);
				days.append("EVERYDAY");
			}

			if(day0 && day1 && day2 && day3 && day4 && !day5 && !day6){
				days.setLength(0);
				days.append("WEEKDAYS");
			}

			return days.toString();
		}

	}

	public Frequency getFrequency() {
		return frequency;
	}

	public int get__v() {
		return __v;
	}

	public String get_id() {
		return _id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getOwner() {
		return owner;
	}

	public Date getTime() {
		return time;
	}

	public void set__v(int __v) {
		this.__v = __v;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
