package com.unum.android.data.model.request;

/**
 * Created by Dhaval Parmar on 23/8/16.
 * <p/>
 * Body request for update profile process
 */
public class UpdateProfileRequest {
	String username, fullName;
	int numPosts, numFollowers, numFollowing;
	boolean liveGridEnabled;
	long UTCOffset;

	/**
	 * Pass the values to constructor for update profile process
	 *
	 * @param username the username of the instagram account
	 * @param fullName full name of the user from instagram account
	 * @param numPosts number of instagram posts
	 * @param numFollowers number of followers
	 * @param numFollowing number of following
	 * @param liveGridEnabled a flag to see if live grid is enabled
	 * @param UTCOffset The offset of UTC in seconds
	 */
	public UpdateProfileRequest(String username, String fullName, int numPosts, int numFollowers,
			int numFollowing, boolean liveGridEnabled, long UTCOffset) {
		this.username = username;
		this.fullName = fullName;
		this.numPosts = numPosts;
		this.numFollowers = numFollowers;
		this.numFollowing = numFollowing;
		this.liveGridEnabled = liveGridEnabled;
		this.UTCOffset = UTCOffset;
	}
}
