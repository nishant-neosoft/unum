package com.unum.android.data.server;

/**
 * Created by Dhaval Parmar on 22/8/16.
 */
public class UnumConstants {

	// Header Content Type
	public static final String API_CONTENT_TYPE = "Content-Type:application/json";

	// Device Type
	public static final String DEVICE_TYPE = "android";

	// Login Request
	public static final String LOGIN_API = "v1/login/instagram";

	// SignUp Request
	public static final String SIGN_UP_API = "v1/users";
}
