package com.unum.android.data.model;

import java.util.ArrayList;

/**
 * Created by Nishant Shah on 22-Aug-16.
 */
public class InstagramImages {

	ArrayList<Data> data;

	public InstagramImages(ArrayList<Data> data) {
		this.data = data;
	}

	public ArrayList<Data> getData() {
		int size = data.size();
		for (Data d : data) {
			if (d.getImages() != null) {
				d.setEmpty(false);
				d.setSelected(false);
				d.setInstaImage(true);
				int index = size + 1;
				d.setIndex(index);
				size++;
			}
		}
		return data;
	}

	public void setData(ArrayList<Data> data) {
		this.data = data;
	}

	public ArrayList<Data> getEmptyDataList(int count) {
		for (int i = 0; i < count; i++) {
			data.add(new Data(null, i, false, false, true));
		}
		return data;
	}

	public class Data {

		Images images;
		boolean isInstaImage = false;
		boolean isSelected = false;
		boolean isEmpty = true;
		int index = -1;

		public Data(Images images, int index, boolean isInstaImage, boolean isSelected,
				boolean isEmpty) {
			this.images = images;
		}

		public boolean isInstaImage() {
			return isInstaImage;
		}

		public void setInstaImage(boolean instaImage) {
			isInstaImage = instaImage;
		}

		public Images getImages() {
			return images;
		}

		public void setImages(Images images) {
			this.images = images;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public boolean isEmpty() {
			return isEmpty;
		}

		public void setEmpty(boolean empty) {
			isEmpty = empty;
		}

		public boolean isSelected() {
			return isSelected;
		}

		public void setSelected(boolean selected) {
			isSelected = selected;
		}

		public void setImagePath(String path) {
			if (images == null) {
				images = new Images(null, null, null);
			}
			images.initThumbnail(path, null, null);
		}

		public class Images {

			Resolutions low_resolution, thumbnail, standard_resolution;

			public Images(Resolutions low_resolution, Resolutions thumbnail,
					Resolutions standard_resolution) {
				this.low_resolution = low_resolution;
				this.thumbnail = thumbnail;
				this.standard_resolution = standard_resolution;
			}

			public void initThumbnail(String path, String w, String h) {
				thumbnail = new Resolutions(path, w, h);
			}

			public Resolutions getLow_resolution() {
				return low_resolution;
			}

			public void setLow_resolution(Resolutions low_resolution) {
				this.low_resolution = low_resolution;
			}

			public Resolutions getThumbnail() {
				return thumbnail;
			}

			public void setThumbnail(Resolutions thumbnail) {
				this.thumbnail = thumbnail;
			}

			public Resolutions getStandard_resolution() {
				return standard_resolution;
			}

			public void setStandard_resolution(Resolutions standard_resolution) {
				this.standard_resolution = standard_resolution;
			}

			public class Resolutions {
				String url, width, height;

				public Resolutions(String url, String width, String height) {
					this.url = url;
					this.width = width;
					this.height = height;
				}

				public String getUrl() {
					return url;
				}

				public void setUrl(String url) {
					this.url = url;
				}

				public String getWidth() {
					return width;
				}

				public void setWidth(String width) {
					this.width = width;
				}

				public String getHeight() {
					return height;
				}

				public void setHeight(String height) {
					this.height = height;
				}
			}
		}
	}
}
