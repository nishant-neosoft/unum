package com.unum.android.data.model.response;

/**
 * Created by Dhaval Parmar on 29/8/16.
 */
public class GetUserPostsResponse {
	String _id, creator, message, imageUrl, draft, createdDate;
	int index, __v;

	public GetUserPostsResponse(String _id, String creator, String message, String imageUrl,
			String draft, String createdDate, int index, int __v) {
		this._id = _id;
		this.creator = creator;
		this.message = message;
		this.imageUrl = imageUrl;
		this.draft = draft;
		this.createdDate = createdDate;
		this.index = index;
		this.__v = __v;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int get__v() {
		return __v;
	}

	public void set__v(int __v) {
		this.__v = __v;
	}
}
