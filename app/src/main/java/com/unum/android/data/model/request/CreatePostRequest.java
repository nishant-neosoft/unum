package com.unum.android.data.model.request;

/**
 * Created by Dhaval Parmar on 26/8/16.
 * <p>
 * Body request for post image
 */
public class CreatePostRequest {
	String message, imageUrl, draft;
	int index;

	/**
	 * Constructor for post image process
	 *
	 * @param message caption for the post
	 * @param imageUrl URL of the image
	 * @param draft ObjectID of the Draft containing the Post
	 * @param index position of the post in the draft
	 */
	public CreatePostRequest(String message, String imageUrl, String draft, int index) {
		this.message = message;
		this.imageUrl = imageUrl;
		this.draft = draft;
		this.index = index;
	}
}
