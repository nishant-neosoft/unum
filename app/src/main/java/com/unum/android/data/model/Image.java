package com.unum.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.unum.android.ui.QueueFragment;

/**
 * Created by Snehal on 7/31/16.
 */
public class Image implements Parcelable {

	public static final Creator<Image> CREATOR = new Creator<Image>() {
		@Override public Image createFromParcel(Parcel source) {
			return new Image(source);
		}

		@Override public Image[] newArray(int size) {
			return new Image[size];
		}
	};
	private long id;
	private String postId;
	private String message;
	private String name;
	private String path;
	private String draft;
	private boolean isSelected;
	private String insta_high_resolution_image_path;

	public Image(long id, String postId, String msg, String name, String path, String draft,
			boolean isSelected) {
		this.id = id;
		this.postId = postId;
		this.message = msg;
		this.name = name;
		this.path = path;
		this.draft = draft;
		this.isSelected = isSelected;
	}

	protected Image(Parcel in) {
		this.id = in.readLong();
		this.postId = in.readString();
		this.message = in.readString();
		this.name = in.readString();
		this.path = in.readString();
		this.insta_high_resolution_image_path = in.readString();
		this.draft = in.readString();
		this.isSelected = in.readByte() != 0;
	}

	public String getInsta_high_resolution_image_path() {
		return insta_high_resolution_image_path;
	}

	public void setInsta_high_resolution_image_path(String insta_high_resolution_image_path) {
		this.insta_high_resolution_image_path = insta_high_resolution_image_path;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getMessage() {
		if(null == message){
			message = "@unumdesign #unum";
		}
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		if(TextUtils.isEmpty(path)){
			path = QueueFragment.EMPTY_IMAGE_PATH;
		}
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	@Override public int describeContents() {
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(this.id);
		dest.writeString(this.postId);
		dest.writeString(this.message);
		dest.writeString(this.name);
		dest.writeString(this.path);
		dest.writeString(this.insta_high_resolution_image_path);
		dest.writeString(this.draft);
		dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
	}

	@Override public String toString() {
		return "Image{" +
				"id=" + id +
				'}';
	}
}
