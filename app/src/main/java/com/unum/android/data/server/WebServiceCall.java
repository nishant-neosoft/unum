package com.unum.android.data.server;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import com.google.gson.Gson;
import com.unum.android.data.local.Database;
import com.unum.android.data.model.request.CreatePostRequest;
import com.unum.android.data.model.request.CreateReminderRequest;
import com.unum.android.data.model.request.DeletePostsRequest;
import com.unum.android.data.model.request.DeleteReminderRequest;
import com.unum.android.data.model.request.DeleteScheduleRequest;
import com.unum.android.data.model.request.Frequency;
import com.unum.android.data.model.request.GetImageResoueceRequest;
import com.unum.android.data.model.request.LoginSignupRequest;
import com.unum.android.data.model.request.UpdateDeviceTokenRequest;
import com.unum.android.data.model.request.UpdateMultiplePost;
import com.unum.android.data.model.request.UpdateReminderRequest;
import com.unum.android.data.model.request.UpdateScheduleRequest;
import com.unum.android.data.model.response.CreatePostResponse;
import com.unum.android.data.model.response.GetAllSchedulesResponse;
import com.unum.android.data.model.response.GetImageResourcesResponse;
import com.unum.android.data.model.response.GetUserPostsResponse;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.data.model.response.LoginSignupResponse;
import com.unum.android.data.model.response.UpdateProfileResponse;
import com.unum.android.data.model.response.UpdateSinglePostResponse;
import com.unum.android.instagram.InstagramApp;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Dhaval Parmar on 23/8/16.
 */
public class WebServiceCall {

	InstagramApp mApp;
	HashMap<String, String> userInfoHashmap;
	Context mContext;
	LoginCallback loginCallback;

	UnumSession mUnumSession;
	String unumUserId = "", unumAuth = "";

	public WebServiceCall(Context mContext, InstagramApp mApp,
			HashMap<String, String> userInfoHashmap, LoginCallback callback) {
		this.mApp = mApp;
		this.userInfoHashmap = userInfoHashmap;
		this.mContext = mContext;
		this.loginCallback = callback;
		mUnumSession = new UnumSession(mContext);
		unumUserId = mUnumSession.getUNUM_USER_ID();
		unumAuth = mUnumSession.getUNUM_AUTH_TOKEN();
		Database.init(mContext);
	}

	public WebServiceCall(Context mContext) {
		mUnumSession = new UnumSession(mContext);
		unumUserId = mUnumSession.getUNUM_USER_ID();
		unumAuth = mUnumSession.getUNUM_AUTH_TOKEN();
		//Database.init(mContext);
	}

	/**
	 * method for login in unum app.
	 */
	public void loginWithUnum() {
		LoginSignupRequest mRequest =
				new LoginSignupRequest(UnumConstants.DEVICE_TYPE, mApp.getId(), mApp.getTOken(),
						mApp.getUserName(), mApp.getName(),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_MEDIA)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWED_BY)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWS)));

		Log.e("UNUM LOGIN REQUEST", new Gson().toJson(mRequest));

		UnumService unumService = ApiClient.getClient().create(UnumService.class);
		Call<LoginSignupResponse> call = unumService.loginUser(mRequest);
		call.enqueue(new CallbackRequest<LoginSignupResponse>() {

			@Override public void onResponse(Call<LoginSignupResponse> call,
					Response<LoginSignupResponse> response) {
				super.onResponse(call, response);
				Log.e("UNUM LOGIN RES CODE", new Gson().toJson(response.code()));

				if (response.code() == 404) {
					signUpWithUnum();
				}
			}

			@Override public void onResponse(Response<LoginSignupResponse> response) {
				Log.e("UNUM LOGIN RESPONSE", new Gson().toJson(response.body()));

				Database.changeuserStatus();

				Database.insertUserDetails(mApp.getId(), mApp.getTOken(), "", mApp.getUserName(),
						mApp.getName(), Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWED_BY)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWS)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_MEDIA)), 1,
						response.body().get_id(), new Gson().toJson(response.body()));

				String userData = "";

				String where = Database.COLUMN_UNUM_USER_ID + "='" + response.body().get_id() + "'";
				Cursor mCursor = Database.getValues(Database.USER_TABLE, null, where);

				if (!(mCursor == null) && mCursor.getCount() > 0) {
					if (mCursor.moveToNext()) {
						do {
							userData = mCursor.getString(mCursor.getColumnIndex(Database.COLUMN_UNUM_USER_DATA));
						} while (mCursor.moveToNext());
					}
				}

				LoginSignupResponse mLoginSignupResponse =
						new Gson().fromJson(userData, LoginSignupResponse.class);

				mUnumSession.storeUnumSession(mLoginSignupResponse.get_id(),
						mLoginSignupResponse.getAuthToken(), mLoginSignupResponse.isLiveGridEnabled(),
						mLoginSignupResponse.getUsername(), mLoginSignupResponse.getFullName(),
						mLoginSignupResponse.getNumPosts(), mLoginSignupResponse.getNumFollowing(),
						mLoginSignupResponse.getNumFollowers());

				String defaultDraft = "", queueDraft = "";

				for (int i = 0; i < mLoginSignupResponse.getDrafts().size(); i++) {
					if (mLoginSignupResponse.getDrafts().get(i).isDefault()) {
						defaultDraft = mLoginSignupResponse.getDrafts().get(i).get_id();
					} else {
						queueDraft = mLoginSignupResponse.getDrafts().get(i).get_id();
					}
				}

				mUnumSession.storeDrafts(defaultDraft, queueDraft);

				loginCallback.loginSuccess();
				updateDeviceToken();
			}

			@Override public void onError() {
				Log.e("UNUM LOGIN ERROR", "HERE");
				loginCallback.loginFail();
			}
		});
	}

	/**
	 * method for sign up the user with unum.
	 * when the user tries to login with loginWithUnum() but it gives 404 error or gives the message
	 * like
	 * "no user available" at that time use this method firstly to sign up and after that use the
	 * loginWithUnum() to logged in the user.
	 */
	public void signUpWithUnum() {
		LoginSignupRequest mRequest =
				new LoginSignupRequest(UnumConstants.DEVICE_TYPE, mApp.getId(), mApp.getTOken(),
						mApp.getUserName(), mApp.getName(),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_MEDIA)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWED_BY)),
						Integer.parseInt(userInfoHashmap.get(InstagramApp.TAG_FOLLOWS)));

		Log.e("UNUM SIGNUP REQUEST", new Gson().toJson(mRequest));

		UnumService unumService = ApiClient.getClient().create(UnumService.class);
		Call<LoginSignupResponse> call = unumService.signUpUser(mRequest);
		call.enqueue(new CallbackRequest<LoginSignupResponse>() {
			@Override public void onResponse(Response<LoginSignupResponse> response) {
				Log.e("UNUM SIGNUP RESPONSE", new Gson().toJson(response.body()));
			}

			@Override public void onError() {
				Log.e("UNUM SIGNUP ERROR", "HERE");
			}

			@Override public void onResponse(Call<LoginSignupResponse> call,
					Response<LoginSignupResponse> response) {
				super.onResponse(call, response);
				if (response.code() == 200) {
					loginWithUnum();
				}
			}
		});
	}

	/**
	 * method for update the user info
	 */
	public void updateUserInfo() {

		String unumUserName = mUnumSession.getUNUM_USER_NAME();
		String unumFullName = mUnumSession.getUNUM_FULL_NAME();
		int unumPosts = mUnumSession.getNumberOfPosts();
		int unumFollower = mUnumSession.getNumberOfFollwers();
		int unumFollowing = mUnumSession.getNumberOfFollowing();
		boolean unumIsLiveGrid = mUnumSession.isLiveGridEnabled();

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long dateOffSet = calendar.getTimeInMillis() / 1000L;

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);

		/**
		 * Call for PUT
		 */
		Call<UpdateProfileResponse> call =
				unumService.updateUserInfoPut(unumUserId, unumUserName, unumFullName, unumPosts,
						unumFollower, unumFollowing, unumIsLiveGrid, dateOffSet);

		call.enqueue(new CallbackRequest<UpdateProfileResponse>() {
			@Override public void onResponse(Response<UpdateProfileResponse> response) {
				Log.e("UNUM UPDATE RESPONSE", new Gson().toJson(response.body()));
			}

			@Override public void onError() {
			}

			@Override public void onResponse(Call<UpdateProfileResponse> call,
					Response<UpdateProfileResponse> response) {
				super.onResponse(call, response);
				Log.e("UNUM UPDATE RESPONSE", new Gson().toJson(response.code()));
				updateDeviceToken();
			}
		});
	}

	/**
	 * method for logout the user
	 * On success clear all the sessions for application that UnumSession and InstagramSession
	 */
	public void logoutUser() {
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Call<ResponseBody> call = unumService.logoutUser();

		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.e("UNUM LOGOUT RESPONSE", new Gson().toJson(response));
			}

			@Override public void onError() {
			}

			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				Log.e("UNUM LOGOUT RES CODE", new Gson().toJson(response));
			}
		});
	}

	/**
	 * method for get self information
	 */
	public void getSelfInfo() {
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Call<LoginSignupResponse> call = unumService.getSelfInformation();

		call.enqueue(new CallbackRequest<LoginSignupResponse>() {
			@Override public void onResponse(Response<LoginSignupResponse> response) {
				Log.e("UNUM SELF INFO RESPONSE", new Gson().toJson(response.body()));
			}

			@Override public void onError() {
			}
		});
	}

	/**
	 * method for update device token for device
	 */
	public void updateDeviceToken() {
		String deviceToken = mUnumSession.getGCM_DEVICE_TOKEN();
		AppLogger.print("updateDeviceToken() deviceToken=" + deviceToken);

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);

		UpdateDeviceTokenRequest mRequest = new UpdateDeviceTokenRequest(deviceToken);

		/**
		 * Call for PUT
		 */
		Call<ResponseBody> call = unumService.updateDeviceTokenPut(unumAuth, mRequest);

		Log.e("UNUM UPDATE TOKEN REQ", new Gson().toJson(mRequest));

		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.e("UNUM UPDATE TOKEN RES", new Gson().toJson(response.body()));
				Log.e("UNUM UPDATE TOKEN CODE", "=" + response.code());
				if (response.code() == 200) {
					Log.e("UNUM UPDATE TOKEN RES", "SUCCESS");
				} else {
					Log.e("UNUM UPDATE TOKEN RES", "ERROR");
				}
			}

			@Override public void onError() {
				Log.e("UNUM UPDATE TOKEN RES", "ERROR");
			}

			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				Log.e("UNUM UPDATE TOKEN RES", new Gson().toJson(response.code()));
				Log.e("UNUM UPDATE TOKEN CODE", "=" + response.code());
				Log.e("UNUM UPDATE TOKEN RES", "SUCCESS");
			}
		});
	}

	/**
	 * method to get all the resources according to image to upload
	 *
	 * @param size size of image in bytes
	 * @param type image type
	 * @param fileToUpload File to upload
	 */
	public void getImageResources(final int pos, String size, String type, final File fileToUpload,
			String draft, final Callback<String> callback) {
		final int imageSize = Integer.parseInt(size);

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		GetImageResoueceRequest mRequest = new GetImageResoueceRequest(imageSize, type);
		Call<GetImageResourcesResponse> call = unumService.getImageResources(mRequest);

		Log.e("UNUM GET IMAGE RES REQ", new Gson().toJson(mRequest));

		call.enqueue(new CallbackRequest<GetImageResourcesResponse>() {
			@Override public void onResponse(Response<GetImageResourcesResponse> response) {
				Log.e("UNUM GET IMAGE RESN RES", new Gson().toJson(response.body()));

				final String uploadUrl = response.body().getFiles().get(0).getMeta().getUploadUrl();
				final String policy = response.body().getFiles().get(0).getMeta().getPolicy().getPolicy();
				final String signature =
						response.body().getFiles().get(0).getMeta().getPolicy().getSignature();
				final String contentType =
						response.body().getFiles().get(0).getMeta().getPolicy().getContentype();
				final String key = response.body().getFiles().get(0).getMeta().getPolicy().getKey();
				final String acl = response.body().getFiles().get(0).getMeta().getPolicy().getAcl();
				final String awsAccessKey =
						response.body().getFiles().get(0).getMeta().getPolicy().getAWSAccessKeyId();
				final int size = response.body().getFiles().get(0).getSize();
				String imageUrl = response.body().getFiles().get(0).getUrl();

				uploadImage(pos, uploadUrl, policy, signature, contentType, key, acl, awsAccessKey, size,
						fileToUpload, imageUrl, draft, callback);
			}

			@Override public void onError() {
			}
		});
	}

	/**
	 * method for upload image on server. After getting image resources successfully upload the image
	 * using uploadUrl provided in response of getImageResources
	 *
	 * @param uploadUrl url for upload the image
	 * @param policy policy for aws
	 * @param signature signature for aws
	 * @param contentType content type of image
	 * @param key key of aws
	 * @param acl access level of file
	 * @param awsAccessKey accesskey for aws
	 * @param size size of file
	 * @param file file for upload
	 */
	public void uploadImage(final int pos, final String uploadUrl, final String policy,
			final String signature, final String contentType, final String key, final String acl,
			final String awsAccessKey, final int size, final File file, final String imageUrl,
			String draft, final Callback<String> callback) {

		new AsyncTask<Void, Integer, String>() {
			@Override protected String doInBackground(Void... voids) {
				String charset = "UTF-8";
				File uploadFile = file;
				String requestURL = uploadUrl;

				try {

					String credentials = unumUserId + ":" + unumAuth;
					final String basic =
							"Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

					Log.e("BASIC AUTH", basic);

					MultipartUtility multipart =
							new MultipartUtility(requestURL, charset, new ProgressAsyncCallback() {
								@Override public void onProgress(int progress) {
									publishProgress(progress);
								}
							});

					multipart.addHeaderField("Authorization", basic);
					multipart.addHeaderField("Content-Type", contentType);
					multipart.addHeaderField("Content-Length", String.valueOf(size));

					multipart.addFormField("policy", policy);
					multipart.addFormField("signature", signature);
					multipart.addFormField("Content-Type", contentType);
					multipart.addFormField("key", key);
					multipart.addFormField("acl", acl);
					multipart.addFormField("AWSAccessKeyId", awsAccessKey);

					multipart.addFilePart("file", uploadFile);

					List<String> response = multipart.finish();

					System.out.println("SERVER REPLIED:");

					for (String line : response) {
						System.out.println(line);
						Log.e("Upload Response", line);
					}
				} catch (IOException ex) {
					System.err.println(ex);

					if (ex.getMessage().contains("204")) {
						return ex.getMessage();
					}
				}
				return null;
			}

			@Override protected void onPostExecute(String message) {
				super.onPostExecute(message);
				callback.onSuccess("Upload Successfully");
				createPostOnServer(pos, imageUrl, draft);
			}

			@Override protected void onProgressUpdate(Integer... values) {
				callback.onProgressUpdate(values[0]);
			}
		}.execute();
	}

	/**
	 * Create post after uploading the image on server
	 *
	 * @param imageUrl image url is provided when you get the resources for upload the image
	 * <p>
	 * You have to pass the index of grid and as well draft id in which
	 * you are uploading the image and the message if there is any custom message there
	 */
	private void createPostOnServer(int pos, String imageUrl, String draft) {

		String message = "@unumdesign #unum";
		//String draft = mUnumSession.getUNUM_DEFAULT_DRAFT();
		//        String draft = mUnumSession.getUNUM_QUEUE_DRAFT();
		int postIndex = pos;

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		CreatePostRequest mRequest = new CreatePostRequest(message, imageUrl, draft, postIndex);
		Call<CreatePostResponse> call = unumService.createPost(mRequest);

		call.enqueue(new CallbackRequest<CreatePostResponse>() {
			@Override public void onResponse(Response<CreatePostResponse> response) {
				Log.e("UNUM CREATE POST RES B", new Gson().toJson(response.body()));
			}

			@Override public void onError() {
			}

			@Override
			public void onResponse(Call<CreatePostResponse> call, Response<CreatePostResponse> response) {
				super.onResponse(call, response);
				Log.e("UNUM CREATE POST RES C", new Gson().toJson(response.code()));
			}
		});
	}

	/**
	 * Get user's all posts
	 */
	public void getUsersPosts(final Callback<List<GetUserPostsResponse>> callback) {
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		final Call<List<GetUserPostsResponse>> call = unumService.getUserPosts(unumUserId);

		call.enqueue(new CallbackRequest<List<GetUserPostsResponse>>() {
			@Override public void onResponse(Response<List<GetUserPostsResponse>> response) {
				Log.e("UNUM USER POSTS RES", new Gson().toJson(response.body()));

				ArrayList<GetUserPostsResponse> posts = (ArrayList<GetUserPostsResponse>) response.body();

				callback.onSuccess(posts);
			}

			@Override public void onError() {
			}
		});
	}

	public void getUserReminders(final Callback<List<GetUserReminderResponse>> callback) {
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		final Call<List<GetUserReminderResponse>> call = unumService.getAllReminders();
		call.enqueue(new CallbackRequest<List<GetUserReminderResponse>>() {
			@Override public void onResponse(Response<List<GetUserReminderResponse>> response) {
				AppLogger.d("REMINDER RESPONSE CODE :::: " + response.code());
				AppLogger.d("REMINDER RESPONSE RESPONSE :::: " + new Gson().toJson(response.body()));
				ArrayList<GetUserReminderResponse> reminderResponses =
						(ArrayList<GetUserReminderResponse>) response.body();

				callback.onSuccess(reminderResponses);
			}

			@Override public void onError() {
				callback.onFailure();
			}
		});
	}

	public void getAllSchedules(final Callback<List<GetAllSchedulesResponse>> callback) {
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		final Call<List<GetAllSchedulesResponse>> call = unumService.getAllSchedule();
		call.enqueue(new CallbackRequest<List<GetAllSchedulesResponse>>() {
			@Override public void onResponse(Response<List<GetAllSchedulesResponse>> response) {
				AppLogger.d("getAllSchedules CODE :::: " + response.code());
				AppLogger.d("getAllSchedules RESPONSE :::: " + new Gson().toJson(response.body()));
				List<GetAllSchedulesResponse> scheduleList =
						(List<GetAllSchedulesResponse>) response.body();

				callback.onSuccess(scheduleList);
			}

			@Override public void onError() {
				callback.onFailure();
			}
		});
	}

	/**
	 * Method to update the single post from the posts
	 *
	 * @param postId ID of post
	 * @param postMessage caption for the post
	 * @param imageUrl the image of the url
	 * @param index position of the post in the draft
	 * @param drafts ObjectID of the Draft containing the Post
	 */
	public void updateSinglePost(String postId, String postMessage, String imageUrl, int index,
			String drafts, Callback<String> callback) {

		Log.d("TAG", "updateSinglePost="
				+ postId
				+ ","
				+ postMessage
				+ ","
				+ imageUrl
				+ ","
				+ index
				+ ","
				+ drafts);

		UpdateMultiplePost.Posts mSinglePostUpdateRequest =
				new UpdateMultiplePost().new Posts(postId, drafts, imageUrl, postMessage, index);

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);

		Log.e("UPDATE SINGLE POST REQ", new Gson().toJson(mSinglePostUpdateRequest));

		Call<UpdateSinglePostResponse> call =
				unumService.updateSinglePost(postId, mSinglePostUpdateRequest);

		call.enqueue(new CallbackRequest<UpdateSinglePostResponse>() {
			@Override public void onResponse(Response<UpdateSinglePostResponse> response) {
				Log.d("TAG", "response=" + new Gson().toJson(response.code()));

				UpdateSinglePostResponse updateSinglePostResponse = response.body();
				callback.onSuccess(updateSinglePostResponse.getMessage());
			}

			@Override public void onError() {
				Log.d("TAG", "onError");
				callback.onFailure();
			}
		});
	}

	public void updateSchedule(String date, ArrayList<String> postIds, Callback<String> callback) {
		Log.d("TAG", "updateSchedule=" + date + "," + postIds);
		UpdateScheduleRequest request = new UpdateScheduleRequest(date, postIds);
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("UPDATE SCHEDULE REQ", new Gson().toJson(request));

		Call<ResponseBody> call = unumService.updateSchedule(request);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.d("TAG", "response=" + new Gson().toJson(response.code()));

				ResponseBody responseBody = response.body();
				callback.onSuccess(responseBody.toString());
			}

			@Override public void onError() {
				Log.d("TAG", "onError");
				callback.onFailure();
			}
		});
	}

	public void createSchedule(String date, ArrayList<String> postIds, Callback<String> callback) {
		Log.d("TAG", "createOrUpdateSchedule=" + date + "," + postIds);

		UpdateScheduleRequest updateScheduleRequest = new UpdateScheduleRequest(date, postIds);
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("CREATE SCHEDULE REQ", new Gson().toJson(updateScheduleRequest));

		Call<ResponseBody> call = unumService.updateSchedule(updateScheduleRequest);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.d("TAG", "response=" + new Gson().toJson(response.code()));

				ResponseBody responseBody = response.body();
				callback.onSuccess(responseBody.toString());
			}

			@Override public void onError() {
				Log.d("TAG", "onError");
				callback.onFailure();
			}
		});
	}

	public void createReminder(String time, boolean day0, boolean day1, boolean day2, boolean day3,
			boolean day4, boolean day5, boolean day6, Callback<String> callback) {
		Log.d("TAG", "createReminder="
				+ time
				+ ","
				+ day0
				+ ","
				+ day1
				+ ","
				+ day2
				+ ","
				+ day3
				+ ","
				+ day4
				+ ","
				+ day5
				+ ","
				+ day6);

		CreateReminderRequest createReminderRequest =
				new CreateReminderRequest(time, new Frequency(day0, day1, day2, day3, day4, day5, day6));

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("CREATE REMINDER REQ", new Gson().toJson(createReminderRequest));

		Call<ResponseBody> call = unumService.createReminder(createReminderRequest);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.d("TAG", "response=" + new Gson().toJson(response.code()));

				ResponseBody responseBody = response.body();
				callback.onSuccess(responseBody.toString());
			}

			@Override public void onError() {
				Log.d("TAG", "onError");
				callback.onFailure();
			}
		});
	}

	public void updateReminder(String scheduleId, String time, boolean day0, boolean day1,
			boolean day2, boolean day3, boolean day4, boolean day5, boolean day6,
			Callback<String> callback) {
		Log.d("TAG", "updateReminder="
				+ scheduleId
				+ ","
				+ time
				+ ","
				+ day0
				+ ","
				+ day1
				+ ","
				+ day2
				+ ","
				+ day3
				+ ","
				+ day4
				+ ","
				+ day5
				+ ","
				+ day6);
		UpdateReminderRequest request = new UpdateReminderRequest(scheduleId, time,
				new Frequency(day0, day1, day2, day3, day4, day5, day6));
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("UPDATE REMINDER REQ", new Gson().toJson(request));

		Call<ResponseBody> call = unumService.updateReminder(request);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Response<ResponseBody> response) {
				Log.d("TAG", "response=" + new Gson().toJson(response.code()));

				ResponseBody responseBody = response.body();
				callback.onSuccess(responseBody.toString());
			}

			@Override public void onError() {
				Log.d("TAG", "onError");
				callback.onFailure();
			}
		});
	}

	/**
	 * Method to update the single post from the posts
	 *
	 * @param mPosts arraylist of posts for update
	 */
	public void updateMultiplePosts(UpdateMultiplePost mPosts, Callback<String> callback) {

		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		AppLogger.print("WebserviceCall updateMultiplePosts()"+ new Gson().toJson(mPosts));
		Call<ResponseBody> call = unumService.updateMultiplePost(mPosts);

		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				AppLogger.print("onResponse () "+response.code() + " CODE");
				if (response.code() == 200) {
					AppLogger.print("onResponse () RESPONSE CODE IS 200");
					callback.onSuccess(response.body().toString());
				} else {
					AppLogger.print("onResponse () RESPONSE CODE IS NOT 200");
				}
			}

			@Override public void onResponse(Response<ResponseBody> response) {
				AppLogger.print("onResponse () "+new Gson().toJson(response));
			}

			@Override public void onError() {
				AppLogger.print("onError ()");
				callback.onFailure();
			}
		});
	}

	public void deleteReminder(ArrayList<String> scheduleIds, Callback<String> callback) {

		DeleteReminderRequest deleteReminderRequest = new DeleteReminderRequest(scheduleIds);
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("DELETE REMINDER REQ", new Gson().toJson(deleteReminderRequest));
		Call<ResponseBody> call = unumService.deleteReminder(deleteReminderRequest);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				Log.e("DELETE REMINDER CODE", response.code() + "");
				if (response.code() == 200) {
					callback.onSuccess(response.body().toString());
				} else {
					callback.onFailure();
				}
			}

			@Override public void onResponse(Response<ResponseBody> response) {
				Log.e("DELETE POST RES", new Gson().toJson(response));
			}

			@Override public void onError() {
				callback.onFailure();
			}
		});
	}

	public void deleteSchedules(ArrayList<String> postIds, Callback<String> callback) {
		DeleteScheduleRequest deleteScheduleRequest = new DeleteScheduleRequest(postIds);
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);
		Log.e("DELETE SCHEDULE REQ", new Gson().toJson(deleteScheduleRequest));

		Call<ResponseBody> call = unumService.deleteSchedule(deleteScheduleRequest);
		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				Log.e("DELETE SCHEDULE CODE", response.code() + "");
				if (response.code() == 200) {
					callback.onSuccess(response.body().toString());
				} else {
					callback.onFailure();
				}
			}

			@Override public void onResponse(Response<ResponseBody> response) {
				Log.e("DELETE POST RES", new Gson().toJson(response));
			}

			@Override public void onError() {
				callback.onFailure();
			}
		});
	}

	/**
	 * Method to delete the several posts
	 *
	 * @param mPosts arraylist of post's id
	 */
	public void deletePosts(ArrayList<String> mPosts, Callback<String> callback) {

		DeletePostsRequest deletePostsRequest = new DeletePostsRequest(mPosts);
		UnumService unumService =
				ApiClient.getClientWithAuth(unumUserId, unumAuth).create(UnumService.class);

		Log.e("DELETE POST REQ", new Gson().toJson(deletePostsRequest));
		Call<ResponseBody> call = unumService.deletePost(deletePostsRequest);

		call.enqueue(new CallbackRequest<ResponseBody>() {
			@Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				super.onResponse(call, response);
				Log.e("DELETE POST RES CODE", response.code() + "");
				if (response.code() == 200) {
					callback.onSuccess(response.body().toString());
				} else {
					callback.onFailure();
				}
			}

			@Override public void onResponse(Response<ResponseBody> response) {
				Log.e("DELETE POST RES", new Gson().toJson(response));
			}

			@Override public void onError() {
				callback.onFailure();
			}
		});
	}

	/**
	 * Swap the drafts
	 */
	public void swapDrafts(SwapDrafrs swapDrafrs) {
		String draftToChange = mUnumSession.getUNUM_QUEUE_DRAFT();

		UnumService unumService = ApiClient.getClient().create(UnumService.class);

		String credentials = unumUserId + ":" + unumAuth;
		final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

		Call<LoginSignupResponse> call = unumService.updateDraft(basic, draftToChange);

		call.enqueue(new CallbackRequest<LoginSignupResponse>() {
			@Override public void onResponse(Response<LoginSignupResponse> response) {
				Log.e("UNUM SWAP DRAFT RES", new Gson().toJson(response.body()));
				Log.e("UNUM SWAP DRAFT RESC", new Gson().toJson(response.code()));

				String defaultDraft = "", queueDraft = "";
				for (int i = 0; i < response.body().getDrafts().size(); i++) {
					if (response.body().getDrafts().get(i).isDefault()) {
						defaultDraft = response.body().getDrafts().get(i).get_id();
					} else {
						queueDraft = response.body().getDrafts().get(i).get_id();
					}
				}

				mUnumSession.storeDrafts(defaultDraft, queueDraft);

				swapDrafrs.onSwapSuccess();
			}

			@Override public void onError() {
			}
		});
	}

	public interface Callback<T> {
		void onSuccess(T t);

		void onFailure();

		void onProgressUpdate(int progress);
	}

	public interface LoginCallback {
		void loginSuccess();

		void loginFail();
	}

	public interface ProgressAsyncCallback {
		void onProgress(int progress);
	}

	public interface SwapDrafrs {
		void onSwapSuccess();

		void onSwapFailure();
	}
}
