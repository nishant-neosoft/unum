package com.unum.android.data.model.request;

/**
 * Created by Dhaval Parmar on 24/8/16.
 * <p/>
 * Body request for get all the resources according to image to b upload
 */
public class GetImageResoueceRequest {
	String type;
	int size;

	/**
	 * Pass the values to constructor for update device token process
	 *
	 * @param size size of image in bytes
	 * @param type mime type of image - "image/jpeg"
	 */
	public GetImageResoueceRequest(int size, String type) {
		this.size = size;
		this.type = type;
	}
}
