package com.unum.android.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.data.model.Image;
import com.unum.android.utils.GlideUtils;
import com.unum.android.utils.TouchImageView;
import com.unum.android.utils.UnumSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class TileImageDialogFragment extends DialogWithNetworkCall {

	private View rootView;
	private TouchImageView imageForTile;
	private ArrayList<Image> imageArrayList;
	private ImageView cancel;
	private ImageView ok;
	private LinearLayout main;
	private TextView textView;

	private String imageUrl = "";
	private String mFileName = "", mfileNameWithoutExtn = "", mfileExtn = "";
	private File mRoot;
	private String mRootDir;

	private OnImageTiled callback;

	public static TileImageDialogFragment newInstance(ArrayList<Image> images) {
		TileImageDialogFragment frag = new TileImageDialogFragment();
		Bundle args = new Bundle();
		args.putParcelableArrayList(BaseActivity.KEY_TILE_IMAGES, images);
		frag.setArguments(args);
		return frag;
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_tile_image_dialog, container, false);

		try {

			imageArrayList = getArguments().getParcelableArrayList(BaseActivity.KEY_TILE_IMAGES);
			getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

			imageForTile = (TouchImageView) rootView.findViewById(R.id.image_for_tile);
			main = (LinearLayout) rootView.findViewById(R.id.main);
			textView = (TextView) rootView.findViewById(R.id.textView);
			cancel = (ImageView) rootView.findViewById(R.id.imgBtnCancel);
			ok = (ImageView) rootView.findViewById(R.id.imgBtnOk);

			imageUrl = imageArrayList.get(0).getPath();

			mFileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1, imageUrl.length());
			mfileNameWithoutExtn = mFileName.substring(0, mFileName.lastIndexOf('.'));
			mfileExtn = mFileName.substring(mFileName.lastIndexOf('.') + 1);

			mRootDir = Environment.getExternalStorageDirectory() + File.separator + "UNUM";
			mRoot = Environment.getExternalStoragePublicDirectory(mRootDir);

			if (!mRoot.exists()) {
				mRoot.mkdir();
			}

			showLoadingDialog();
			GlideUtils.loadImageInImageView(getActivity(), imageForTile, imageUrl, new GlideUtils.OnImageLoadListener() {
				@Override public void onImageLoadingComplete() {
					dismissLoadingDialog();
				}

				@Override public void onImageLoadError(String error) {
					Toast.makeText(getActivity(), R.string.some_err_occured, Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
				}
			});
			imageForTile.setDrawingCacheEnabled(true);

			cancel.setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View view) {
					dismiss();
				}
			});
			ok.setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View view) {
					ArrayList<SlicedImageData> slicedImages = tileImage();
					if (callback != null) {
						callback.onImageTiled(slicedImages);
					}
					dismiss();
				}
			});

			UnumSession	mUnumSession = new UnumSession(getContext());
			int theme = mUnumSession.getUNUM_DEFAULT_THEME();
			if(theme > 3){
				main.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_dark));
				textView.setTextColor(Color.WHITE);
				cancel.setImageResource(R.drawable.ic_clear_white_48dp);
				ok.setImageResource(R.drawable.ic_done_white_48dp);
			}else{
				main.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_light));
				textView.setTextColor(Color.BLACK);
				cancel.setImageResource(R.drawable.ic_clear_black_48dp);
				ok.setImageResource(R.drawable.ic_done_black_48dp);
			}
		} catch (Exception e) {
			Toast.makeText(getActivity(), R.string.some_err_occured, Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		return rootView;
	}

	private ArrayList<SlicedImageData> tileImage() {
		Bitmap visibleBitmap = imageForTile.getDrawingCache();
		ArrayList<SlicedImageData> slicedImages = new ArrayList<>();

		int rows, cols;
		int chunkHeight, chunkWidth;
		int chunkNumbers = 9;

		Bitmap scaledBitmap = Bitmap.createScaledBitmap(visibleBitmap, visibleBitmap.getWidth(),
				visibleBitmap.getHeight(), true);

		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = visibleBitmap.getHeight() / rows;
		chunkWidth = visibleBitmap.getWidth() / cols;

		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		int index = 0;
		for (int x = 0; x < rows; x++) {
			int xCoord = 0;
			for (int y = 0; y < cols; y++) {

				String fileName = mfileNameWithoutExtn + "_" + x + "_" + y + "." + mfileExtn;
				File dest = new File(mRootDir, fileName);
				Bitmap chunkedBitmap;
				try {
					if (!dest.exists()) {
						dest.getParentFile().mkdir();
					}
					OutputStream out = new FileOutputStream(dest);
					chunkedBitmap =
							Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight);
					chunkedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
					out.flush();
					out.close();
					slicedImages.add(
							new SlicedImageData(dest.getAbsoluteFile().toString(), chunkedBitmap, index));
					xCoord += chunkWidth;
					index++;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			yCoord += chunkHeight;
		}

		return slicedImages;
	}

	public interface OnImageTiled {
		public void onImageTiled(ArrayList<SlicedImageData> tiledImage);
	}

	public void setImageTileCallBack(OnImageTiled callback) {
		this.callback = callback;
	}

	public class SlicedImageData {
		String filePath;
		Bitmap fileBitmap;
		int index;

		public SlicedImageData(String filePath, Bitmap fileBitmap, int index) {
			this.filePath = filePath;
			this.fileBitmap = fileBitmap;
			this.index = index;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public Bitmap getFileBitmap() {
			return fileBitmap;
		}

		public void setFileBitmap(Bitmap fileBitmap) {
			this.fileBitmap = fileBitmap;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
	}
}
