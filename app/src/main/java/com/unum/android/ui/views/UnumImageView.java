package com.unum.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.unum.android.R;
import com.unum.android.ui.QueueFragment;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.GlideUtils;

/**
 * Created by Snehal on 9/16/2016.
 */
public class UnumImageView extends FrameLayout implements GlideUtils.OnImageLoadListener {

	private Context mContext;
	private ImageView imageView;
	private UnumProgressView progressBar;
	private int mWidth, mHieght;
	private boolean isLoadingFinished = false;

	public UnumImageView(Context context) {
		super(context);
		init(context);
	}

	public UnumImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public UnumImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(mContext);
	}

	private void init(Context context) {
		mContext = context;
		LayoutInflater inflater =
				(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rootView = inflater.inflate(R.layout.coumpound_unum_image_view, this, true);
		imageView = (ImageView) rootView.findViewById(R.id.image);
		progressBar = (UnumProgressView) rootView.findViewById(R.id.progress_bar);
	}

	public ImageView getImageView() {
		return imageView;
	}

	public UnumProgressView getProgressBar() {
		return progressBar;
	}

	public void setImageSize(int width, int height) {
		this.mWidth = width;
		this.mHieght = height;
	}

	public void setPath(String path) {
		AppLogger.d(">>>" + path);
		if (null == path) {
			path = QueueFragment.EMPTY_IMAGE_PATH;
		}
		progressBar.setVisibility(
				(isLoadingFinished || path.startsWith(QueueFragment.EMPTY_IMAGE_PATH)) ? View.GONE
						: View.VISIBLE);
		GlideUtils.getImageFromPath(mContext, imageView, path, this, mWidth, mHieght);
	}

	@Override public void onImageLoadingComplete() {
		progressBar.setVisibility(View.GONE);
		isLoadingFinished = true;
	}

	@Override public void onImageLoadError(String error) {
		progressBar.setVisibility(View.GONE);
		isLoadingFinished = false;
	}
}
