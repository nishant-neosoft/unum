package com.unum.android.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.data.model.Image;
import com.unum.android.data.model.request.UpdateMultiplePost;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;

/**
 * Created by webwerks on 30/8/16.
 */
public class CaptionDialog extends DialogWithNetworkCall {
	private View rootView;
	private EditText editText;
	private ImageView cancel;
	private ImageView ok;
	private LinearLayout layoutParent;
	private ArrayList<Image> imageArrayList;
	private OnPostUpdated callback;
	private static String multiPostCaption = "";
	private static String drafts;
	private UnumSession mUnumSession;

	public CaptionDialog() {
	}

	public interface OnPostUpdated {
		public void onPostUpdate(String msg);
	}

	public void setCallback(OnPostUpdated callback) {
		this.callback = callback;
	}

	public static CaptionDialog newInstance(ArrayList<Image> images, String draft) {
		CaptionDialog frag = new CaptionDialog();
		Bundle args = new Bundle();
		args.putParcelableArrayList(BaseActivity.KEY_UPDATE_POST_IMAGES, images);
		frag.setArguments(args);
		drafts = draft;
		return frag;
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.dialog_caption, container, false);
		if (getArguments() == null) {
			dismiss();
		}

		imageArrayList = getArguments().getParcelableArrayList(BaseActivity.KEY_UPDATE_POST_IMAGES);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		editText = (EditText) rootView.findViewById(R.id.editText);
		cancel = (ImageView) rootView.findViewById(R.id.imgBtnCancel);
		ok = (ImageView) rootView.findViewById(R.id.imgBtnOk);
		layoutParent = (LinearLayout) rootView.findViewById(R.id.layoutParent);

		editText.setText("@unumdesign #unum");

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				dismiss();
			}
		});
		ok.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				postMessage();
			}
		});

		if (imageArrayList.size() == 1) {
			String msg = imageArrayList.get(0).getMessage();
			if (!TextUtils.isEmpty(msg)) {
				editText.setText(msg);
			}
		}

		mUnumSession = new UnumSession(getContext());
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if(theme > 3){

			layoutParent.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_dark));
			//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_dark, getContext().getTheme()));
			//}else{
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_dark));
			//}
			((TextView)rootView.findViewById(R.id.textView)).setTextColor(Color.WHITE);
			cancel.setImageResource(R.drawable.ic_clear_white_48dp);
			ok.setImageResource(R.drawable.ic_done_white_48dp);
		}else{
			layoutParent.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_light));
			//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_light, getContext().getTheme()));
			//}else{
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_light));
			//}
			((TextView)rootView.findViewById(R.id.textView)).setTextColor(Color.BLACK);
			cancel.setImageResource(R.drawable.ic_clear_black_48dp);
			ok.setImageResource(R.drawable.ic_done_black_48dp);
		}
		return rootView;
	}

	private void log(String msg) {
		AppLogger.d("TAG", msg);
	}

	private void postMessage() {
		//Hide soft keyboard
		InputMethodManager imm =
				(InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

		log("postMessage.");
		String msg = editText.getText().toString();
		log("msg =" + msg);
		log("drafts=" + drafts);
		if (TextUtils.isEmpty(msg)) {
			return;
		}
		log("arraysize=" + imageArrayList.size());
		if (imageArrayList.size() == 1) {
			showLoadingDialog();
			Image image = imageArrayList.get(0);
			WebServiceCall webServiceCall = new WebServiceCall(getContext());
			webServiceCall.updateSinglePost(image.getPostId(), msg, image.getPath(), (int) image.getId(),
					drafts, new WebServiceCall.Callback<String>() {
						@Override public void onSuccess(String s) {
							log("POST UPDATE SUCCESS = " + s);
							dismissLoadingDialog();
							if (callback != null) {
								callback.onPostUpdate(s);
							}
							dismiss();
							Toast.makeText(getContext(), getString(R.string.message_post_success),
									Toast.LENGTH_LONG).show();
						}

						@Override public void onFailure() {
							dismissLoadingDialog();
							dismiss();
							Toast.makeText(getContext(), getString(R.string.message_post_some_err_occured),
									Toast.LENGTH_LONG).show();
						}

						@Override public void onProgressUpdate(int progress) {

						}
					});
		} else {
			showLoadingDialog();
			multiPostCaption = msg;
			ArrayList<UpdateMultiplePost.Posts> postsForUpdate =
					new ArrayList<UpdateMultiplePost.Posts>();

			int size = imageArrayList.size();
			for (int i = 0; i < size; i++) {
				Image image = imageArrayList.get(i);
				postsForUpdate.add(new UpdateMultiplePost().new Posts(image.getPostId(),
						drafts, image.getPath(), msg, (int) image.getId()));
			}
			Log.d("TAG", "size=" + postsForUpdate.size());
			UpdateMultiplePost updates = new UpdateMultiplePost(postsForUpdate);
			WebServiceCall webServiceCall = new WebServiceCall(getContext());
			webServiceCall.updateMultiplePosts(updates, new WebServiceCall.Callback<String>() {
				@Override public void onSuccess(String s) {
					dismissLoadingDialog();
					dismiss();
					callback.onPostUpdate(multiPostCaption);
					Toast.makeText(getContext(), getString(R.string.message_post_success), Toast.LENGTH_LONG)
							.show();
				}

				@Override public void onFailure() {
					dismissLoadingDialog();
					dismiss();
					Toast.makeText(getContext(), getString(R.string.message_post_some_err_occured),
							Toast.LENGTH_LONG).show();
				}

				@Override public void onProgressUpdate(int progress) {

				}
			});
		}
	}
}
