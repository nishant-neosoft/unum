package com.unum.android.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.unum.android.R;
import com.unum.android.utils.CheckNetworkConnection;

public class SplashActivity extends BaseActivity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);

		if (CheckNetworkConnection.isConnectionAvailable(this)) {
			Thread timer = new Thread() {
				public void run() {
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						startActivity(new Intent(SplashActivity.this, LoginActivity.class));
						SplashActivity.this.finish();
					}
				}
			};
			timer.start();
		} else {
			showMessageDialogWithOkButton(getString(R.string.message_no_internet_title),
					getString(R.string.message_no_internet_message), new DialogInterface.OnClickListener() {
						@Override public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
		}
	}
}
