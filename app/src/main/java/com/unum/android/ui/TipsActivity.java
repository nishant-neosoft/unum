package com.unum.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.unum.android.R;

/**
 * Created by webwerks on 19/8/16.
 */
public class TipsActivity extends BackButtonActivity implements View.OnClickListener {
	private RelativeLayout tipsheader;
	private TextView txtSelecttips;
	private ImageView imgArrow;
	private LinearLayout linerScheduling;
	private LinearLayout linerGridshift;
	private LinearLayout linerAddelete;
	private LinearLayout linerPost;
	private LinearLayout linerReaarage;
	private LinearLayout linerEdit;
	private LinearLayout linerTile;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		setContentView(R.layout.activity_tips);
		getToolbar().setTitle(getString(R.string.title_tips));

		findViews();
	}

	private void findViews() {
		txtSelecttips = (TextView) findViewById(R.id.txt_selecttips);
		tipsheader = (RelativeLayout) findViewById(R.id.tipsheader);
		txtSelecttips = (TextView) findViewById(R.id.txt_selecttips);
		imgArrow = (ImageView) findViewById(R.id.imgArrow);
		linerScheduling = (LinearLayout) findViewById(R.id.liner_scheduling);
		linerGridshift = (LinearLayout) findViewById(R.id.liner_gridshift);
		linerAddelete = (LinearLayout) findViewById(R.id.liner_addelete);
		linerPost = (LinearLayout) findViewById(R.id.liner_post);
		linerReaarage = (LinearLayout) findViewById(R.id.liner_reaarage);
		linerEdit = (LinearLayout) findViewById(R.id.liner_edit);
		linerTile = (LinearLayout) findViewById(R.id.liner_tile);

		txtSelecttips.setText(Html.fromHtml(getResources().getString(R.string.message_selecttip)));

		linerScheduling.setOnClickListener(this);
		linerGridshift.setOnClickListener(this);
		linerAddelete.setOnClickListener(this);
		linerPost.setOnClickListener(this);
		linerReaarage.setOnClickListener(this);
		linerEdit.setOnClickListener(this);
		linerTile.setOnClickListener(this);
		linerTile.setVisibility(View.GONE);
	}

	@Override public void onClick(View view) {
		switch (view.getId()) {
			case R.id.liner_scheduling:
				break;
			case R.id.liner_gridshift:
				break;
			case R.id.liner_addelete:
				Intent addDeleteTips = new Intent();
				addDeleteTips.putExtra(BaseActivity.KEY_TIP, "addDeletePostTip");
				setResult(Activity.RESULT_OK, addDeleteTips);
				finish();
				break;
			case R.id.liner_post:
				Intent postTips = new Intent();
				postTips.putExtra(BaseActivity.KEY_TIP, "postTip");
				setResult(Activity.RESULT_OK, postTips);
				finish();
				break;
			case R.id.liner_reaarage:
				Intent rearrangeTips = new Intent();
				rearrangeTips.putExtra(BaseActivity.KEY_TIP, "rearrangeTip");
				setResult(Activity.RESULT_OK, rearrangeTips);
				finish();
				break;
			case R.id.liner_edit:
				Intent editTips = new Intent();
				editTips.putExtra(BaseActivity.KEY_TIP, "editTips");
				setResult(Activity.RESULT_OK, editTips);
				finish();
				break;
			case R.id.liner_tile:
				Intent tileTips = new Intent();
				tileTips.putExtra(BaseActivity.KEY_TIP, "tileTip");
				setResult(Activity.RESULT_OK, tileTips);
				finish();
				break;
		}
	}
}
