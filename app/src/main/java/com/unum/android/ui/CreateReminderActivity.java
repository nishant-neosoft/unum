package com.unum.android.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import com.google.gson.Gson;
import com.unum.android.R;
import com.unum.android.data.local.Alarm;
import com.unum.android.data.local.Database;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.ui.views.UnumProgressView;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.DateUtil;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Haridas on 31/8/16.
 */
public class CreateReminderActivity extends BackButtonActivity implements View.OnClickListener {

	private EditText txtHours;
	private EditText txtMinutes;
	private RelativeLayout main;
	private Button btnDelete;
	private Button btnSave;
	private UnumProgressView unumProgressView;
	private ToggleButton amTogglebutton, monTogglebutton, tueTogglebutton, wedTogglebutton,
			thuTogglebutton, friTogglebutton, satTogglebutton, sunTogglebutton;

	private GetUserReminderResponse reminder;
	private Calendar now;
	private boolean isForUpdate = false;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		setContentView(R.layout.activity_create_reminder);

		initView();
	}

	private void showLoading() {
		unumProgressView.setVisibility(View.VISIBLE);
	}

	private void hideLoading() {
		unumProgressView.setVisibility(View.GONE);
	}

	private void initView() {
		main = (RelativeLayout) findViewById(R.id.main);
		unumProgressView = (UnumProgressView) findViewById(R.id.unumProgressView);

		txtHours = (EditText) findViewById(R.id.txtHours);
		txtMinutes = (EditText) findViewById(R.id.txtMinutes);

		amTogglebutton = (ToggleButton) findViewById(R.id.toggleAm);
		monTogglebutton = (ToggleButton) findViewById(R.id.toggleMonday);
		tueTogglebutton = (ToggleButton) findViewById(R.id.toggleTuesday);
		wedTogglebutton = (ToggleButton) findViewById(R.id.toggleWedensday);
		thuTogglebutton = (ToggleButton) findViewById(R.id.toggleThursday);
		friTogglebutton = (ToggleButton) findViewById(R.id.toggleFriday);
		satTogglebutton = (ToggleButton) findViewById(R.id.toggleSaturday);
		sunTogglebutton = (ToggleButton) findViewById(R.id.toggleSunday);

		btnDelete = (Button) findViewById(R.id.btnDelete);
		btnSave = (Button) findViewById(R.id.btnSave);

		btnDelete.setOnClickListener(this);
		btnSave.setOnClickListener(this);

		now = Calendar.getInstance();
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {
				String extra = getIntent().getStringExtra(BaseActivity.KEY_REMINDER_ID);
				if (BaseActivity.KEY_CREATE_REMINDER.equals(extra)) {
					//Create new Reminder
					btnDelete.setEnabled(false);
				} else {
					//update reminder
					isForUpdate = true;
					reminder = new Gson().fromJson(extra, GetUserReminderResponse.class);
					GetUserReminderResponse.Frequency frequency = reminder.getFrequency();
					monTogglebutton.setChecked(frequency.isDay0());
					tueTogglebutton.setChecked(frequency.isDay1());
					wedTogglebutton.setChecked(frequency.isDay2());
					thuTogglebutton.setChecked(frequency.isDay3());
					friTogglebutton.setChecked(frequency.isDay4());
					satTogglebutton.setChecked(frequency.isDay5());
					sunTogglebutton.setChecked(frequency.isDay6());

					now.setTime(reminder.getTime());

					txtHours.setText(DateUtil.getDisplayHours(now.getTime()));
					txtMinutes.setText(DateUtil.getDisplayMins(now.getTime()));

					String ampm = DateUtil.getDisplayAMPM(now.getTime());
					amTogglebutton.setChecked(ampm.equalsIgnoreCase("pm") ? true : false);
				}
			}
		}
		mUnumSession = new UnumSession(this);
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if (theme > 3) {
			main.setBackgroundColor(Color.BLACK);
		} else {
			main.setBackgroundColor(Color.WHITE);
		}
		switch (theme) {
			case 0:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				break;
			case 1:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				break;
			case 2:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				break;
			case 3:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				break;
			case 4:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_gold));
				break;
			case 5:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_cyan));
				break;
			case 6:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_peach));
				break;
			case 7:
				monTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				tueTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				wedTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				thuTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				friTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				satTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				sunTogglebutton.setBackgroundDrawable(
						ContextCompat.getDrawable(this, R.drawable.selector_togglebutton_grey));
				break;
		}
	}

	public void saveAlarm(View v) {

		Alarm alarm = new Alarm();
		alarm.setAlarmActive(true);
		alarm.setId(99);
		setDays(alarm);

		int hour = Integer.parseInt(txtHours.getText().toString());
		if (amTogglebutton.isChecked()) {
			hour = hour + 12;
		}
		alarm.setAlarmTime(hour + ":" + txtMinutes.getText().toString());
		alarm.schedule(this);
		Database.create(alarm);

		finish();
	}

	private void setDays(Alarm alarm) {

		if (!monTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.MONDAY);
		}
		if (!tueTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.TUESDAY);
		}
		if (!wedTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.WEDNESDAY);
		}
		if (!thuTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.THURSDAY);
		}
		if (!friTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.FRIDAY);
		}
		if (!satTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.SATURDAY);
		}
		if (!sunTogglebutton.isChecked()) {
			alarm.removeDay(Alarm.Day.SUNDAY);
		}
	}

	@Override protected void onResume() {
		super.onResume();
		//Database.init(this);
	}

	@Override protected void onPause() {
		super.onPause();
		//Database.deactivate();
	}

	@Override public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btnDelete:
				deleteReminder();
				break;
			case R.id.btnSave:
				if (isForUpdate) {
					updateReminder();
				} else {
					createReminder();
				}
				break;
		}
	}

	private void updateReminder() {

		if (TextUtils.isEmpty(txtHours.getText().toString())) {
			showToast("Please input hours.");
			return;
		}

		if (TextUtils.isEmpty(txtMinutes.getText().toString())) {
			showToast("Please input mins.");
			return;
		}

		showLoading();
		now.set(Calendar.HOUR_OF_DAY, Integer.parseInt(txtHours.getText().toString()));
		now.set(Calendar.MINUTE, Integer.parseInt(txtMinutes.getText().toString()));
		Log("> hour " + txtHours.getText().toString());
		Log("> min " + txtMinutes.getText().toString());
		String date = DateUtil.fromDate(now.getTime());
		Log(">" + date);

		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.updateReminder(reminder.get_id(), date, monTogglebutton.isChecked(),
				tueTogglebutton.isChecked(), wedTogglebutton.isChecked(), thuTogglebutton.isChecked(),
				friTogglebutton.isChecked(), satTogglebutton.isChecked(), sunTogglebutton.isChecked(),
				new WebServiceCall.Callback<String>() {
					@Override public void onSuccess(String s) {
						hideLoading();
						showToast("Reminder has been saved.");
						AppLogger.d("UPDATE REMINDER SUCCESS :: " + new Gson().toJson(s));
						setResult(ReminderFragment.RESULT_CODE);
						finish();
					}

					@Override public void onFailure() {
						hideLoading();
						showToast("Some error occured. Please try again later.");
						AppLogger.d("UPDATE REMINDER FAIL");
					}

					@Override public void onProgressUpdate(int progress) {

					}
				});
	}

	private void createReminder() {
		if (TextUtils.isEmpty(txtHours.getText().toString())) {
			showToast("Please input hours.");
			return;
		}

		if (TextUtils.isEmpty(txtMinutes.getText().toString())) {
			showToast("Please input mins.");
			return;
		}

		showLoading();
		now.set(Calendar.HOUR_OF_DAY, Integer.parseInt(txtHours.getText().toString()));
		now.set(Calendar.MINUTE, Integer.parseInt(txtMinutes.getText().toString()));

		Log("> hour " + txtHours.getText().toString());
		Log("> min " + txtMinutes.getText().toString());

		String date = DateUtil.fromDate(now.getTime());
		Log(">" + date);
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.createReminder(date, monTogglebutton.isChecked(), tueTogglebutton.isChecked(),
				wedTogglebutton.isChecked(), thuTogglebutton.isChecked(), friTogglebutton.isChecked(),
				satTogglebutton.isChecked(), sunTogglebutton.isChecked(),
				new WebServiceCall.Callback<String>() {
					@Override public void onSuccess(String s) {
						hideLoading();
						showToast("Reminder has been created.");
						AppLogger.d("CREATE REMINDER SUCCESS :: " + new Gson().toJson(s));
						setResult(ReminderFragment.RESULT_CODE);
						finish();
					}

					@Override public void onFailure() {
						hideLoading();
						showToast("Some error occured. Please try again later.");
						AppLogger.d("CREATE REMINDER FAIL");
					}

					@Override public void onProgressUpdate(int progress) {

					}
				});
	}

	private void deleteReminder() {
		showLoading();
		ArrayList<String> scheduleIds = new ArrayList<String>();
		scheduleIds.add("test");
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.deleteReminder(scheduleIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				hideLoading();
				AppLogger.d("DELETE REMINDER SUCCESS :: " + new Gson().toJson(s));
				showToast("Reminder has been deleted.");
				setResult(ReminderFragment.RESULT_CODE);
				finish();
			}

			@Override public void onFailure() {
				hideLoading();
				showToast("Some error occured.");
				AppLogger.d("DELETE REMINDER FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}
}
