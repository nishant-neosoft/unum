package com.unum.android.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.data.model.Image;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;

/**
 * Created by webwerks on 30/8/16.
 */
public class DeleteImagesDialog extends DialogWithNetworkCall {
	private LinearLayout layoutParent;
	private TextView cancel, delete;
	private TextView txtSelectedItem;
	private ArrayList<Image> imageArrayList;
	private onDeleteListener listener;

	public DeleteImagesDialog() {
	}

	public void setListener(onDeleteListener onDeleteListener){
		this.listener = onDeleteListener;
	}

	public static DeleteImagesDialog newInstance(ArrayList<Image> images) {
		DeleteImagesDialog frag = new DeleteImagesDialog();
		Bundle args = new Bundle();
		args.putParcelableArrayList(BaseActivity.KEY_UPDATE_POST_IMAGES, images);
		frag.setArguments(args);
		return frag;
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		if (getArguments() == null) {
			dismiss();
		}
		imageArrayList = getArguments().getParcelableArrayList(BaseActivity.KEY_UPDATE_POST_IMAGES);
		AppLogger.d("ORIGINAL imageArrayList = "+imageArrayList.size());
		for(Image image : new ArrayList<>(imageArrayList)){
			if(image.getPath().startsWith(QueueFragment.EMPTY_IMAGE_PATH)){
				AppLogger.d("REMOVING imageArrayList = "+image.getPath());
				imageArrayList.remove(image);
			}
		}
		AppLogger.d("DELETE imageArrayList = "+imageArrayList.size());

		return inflater.inflate(R.layout.dialog_delete_images, container, false);
	}

	@Override public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(rootView, savedInstanceState);
		layoutParent = (LinearLayout) rootView.findViewById(R.id.layoutParent);
		cancel = (TextView) rootView.findViewById(R.id.btnCancel);
		delete = (TextView) rootView.findViewById(R.id.btnOk);

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				dismiss();
			}
		});
		delete.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				deletePosts();
			}
		});

		// Fetch arguments from bundle and set title
		int count = imageArrayList.size();
		txtSelectedItem = (TextView) rootView.findViewById(R.id.txtSelectedItem);
		if(count == 1){
			txtSelectedItem.setText("Delete " + count + " photo?");
		}else {
			txtSelectedItem.setText("Delete " + count + " photos?");
		}

		UnumSession mUnumSession = new UnumSession(getContext());
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if(theme > 3){
			layoutParent.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_dark));
			//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_dark, getContext().getTheme()));
			//}else{
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_dark));
			//}
			cancel.setTextColor(Color.WHITE);
			delete.setTextColor(Color.WHITE);
			txtSelectedItem.setTextColor(Color.WHITE);
			((TextView)rootView.findViewById(R.id.textView)).setTextColor(Color.WHITE);
		}else{
			layoutParent.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.rectangle_shape_light));
			//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_light, getContext().getTheme()));
			//}else{
			//	layoutParent.setBackground(getContext().getResources()
			//			.getDrawable(R.drawable.rectangle_shape_light));
			//}
			cancel.setTextColor(Color.BLACK);
			delete.setTextColor(Color.BLACK);
			txtSelectedItem.setTextColor(Color.BLACK);
			((TextView)rootView.findViewById(R.id.textView)).setTextColor(Color.BLACK);
		}
	}

	private void deletePosts() {
		showLoadingDialog();
		ArrayList<String> postIds = new ArrayList<>();
		for(Image image : imageArrayList){
			postIds.add(image.getPostId());
		}

		WebServiceCall webServiceCall = new WebServiceCall(getContext());
		webServiceCall.deletePosts(postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				dismissLoadingDialog();
				if(listener!=null){
					listener.onDelete();
				}
				dismiss();
			}

			@Override public void onFailure() {
				dismissLoadingDialog();
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	public interface onDeleteListener{
		public void onDelete();
	}
}
