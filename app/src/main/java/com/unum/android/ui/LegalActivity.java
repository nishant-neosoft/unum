package com.unum.android.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.unum.android.R;

/**
 * Created by webwerks on 19/8/16.
 */
public class LegalActivity extends BackButtonActivity {

	private LinearLayout linerPrivacy, linerTerms;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		setContentView(R.layout.activity_legal);
		getToolbar().setTitle(getString(R.string.title_legal));
		linerPrivacy = (LinearLayout) findViewById(R.id.linerPrivacy);
		linerTerms = (LinearLayout) findViewById(R.id.linerTerms);

		linerPrivacy.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://unum.la/privacy-policy/"));
				startActivity(browserIntent);
			}
		});

		linerTerms.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://unum.la/terms-of-service/"));
				startActivity(browserIntent);
			}
		});
	}
}
