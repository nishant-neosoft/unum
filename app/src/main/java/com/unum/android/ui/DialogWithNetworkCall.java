package com.unum.android.ui;

import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;
import com.unum.android.R;

/**
 * Created by Snehal on 9/9/2016.
 */
public class DialogWithNetworkCall extends DialogFragment {

	protected ProgressDialog pDialog;

	protected void showLoadingDialog() {
		pDialog = new ProgressDialog(getContext(), R.style.LoadingDialogTheme);
		pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		//pDialog.setCancelable(false);
		//pDialog.setMessage(getString(R.string.message_loading));
		pDialog.show();
	}

	protected void dismissLoadingDialog() {
		try {
			pDialog.dismiss();
		} catch (Exception e) {
			//to prevent WindowLeaked exception.
			e.printStackTrace();
		}
	}

}
