package com.unum.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.adapter.DrawerAdapter;
import com.unum.android.adapter.ToolbarSpinnerAdapter;
import com.unum.android.data.local.Database;
import com.unum.android.data.model.DrawerItem;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.instagram.InstaConstants;
import com.unum.android.instagram.InstagramApp;
import com.unum.android.instagram.LoggedInstaUsers;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.TipDialogUtils;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by webwerks on 19/8/16.
 */
public class DrawerActivity extends BaseActivity
		implements View.OnClickListener, WebServiceCall.LoginCallback, WebServiceCall.SwapDrafrs {
	String[] item = {
			"SETTINGS", "TIPS", "LEGAL"
	};

	int mLastSpinnerPosition = 0;
	int selectedPos = 0;
	boolean addNew = true;
	public static boolean loginChanged = false;
	private InstagramApp instagramApp;
	private WebServiceCall mWebServiceCall;
	private Toolbar toolbar;
	private Spinner spinner_nav;
	private ImageView menuSchedule;
	private TextView menuSwap;
	private DrawerAdapter drawerAdapter;
	private ToolbarSpinnerAdapter spinAdapter;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private ListView listView;
	private RelativeLayout layoutDrawerList;
	private static HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private ArrayList<DrawerItem> drawerMenu;
	Drawable homeUpDrawable;

	private QueueFragment queueFragment;
	private DraftQueueFragment draftQueueFragment;
	public static ArrayList<TipDialogUtils.Tips> mTips;

	private int SELECT_TIP_TYPE = 7;

	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;

	private WebServiceCall.SwapDrafrs mSwapDrafrs;

	private static int ui_loading = 0;

	private Handler handler = new Handler(new Handler.Callback() {
		@Override public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = instagramApp.getUserInfo();
				mWebServiceCall = new WebServiceCall(DrawerActivity.this, instagramApp, userInfoHashmap,
						DrawerActivity.this);
				mWebServiceCall.loginWithUnum();
			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				showToast("Check your network.");
			}
			return false;
		}
	});

	private void loadFragment() {
		mSectionsPagerAdapter = null;
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
		mViewPager.setAdapter(mSectionsPagerAdapter);
	}

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		setContentView(R.layout.activity_drawer);

		ui_loading = 1;
		if (getIntent() != null && getIntent().getExtras() != null) {
			userInfoHashmap =
					(HashMap<String, String>) getIntent().getSerializableExtra(BaseActivity.KEY_USER_INFO);
		} else {
			return;
		}

		instagramApp = new InstagramApp(this, InstaConstants.CLIENT_ID, InstaConstants.CLIENT_SECRET,
				InstaConstants.CALLBACK_URL);

		homeUpDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_logo_with_bg,
				getApplicationContext().getTheme());
		drawerMenu = new ArrayList<DrawerItem>();
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		layoutDrawerList = (RelativeLayout) findViewById(R.id.layoutDrawerList);
		spinner_nav = (Spinner) findViewById(R.id.spinner_nav);
		menuSchedule = (ImageView) findViewById(R.id.menuSchedule);
		menuSwap = (TextView) findViewById(R.id.menuSwap);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		//imgback = (ImageView) findViewById(R.id.imgback);
		menuSchedule.setOnClickListener(this);
		menuSwap.setOnClickListener(this);

		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("");
		addItemsToSpinner();

		listView = (ListView) findViewById(R.id.listView);
		drawerAdapter = new DrawerAdapter(this, drawerMenu);
		listView.setAdapter(drawerAdapter);
		addDrawerMenus();

		mViewPager = (ViewPager) findViewById(R.id.container);

		mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				switch (position) {
					case 0:
						menuSwap.setVisibility(View.GONE);
						menuSchedule.setVisibility(View.VISIBLE);
						break;
					case 1:
						menuSwap.setVisibility(View.VISIBLE);
						menuSchedule.setVisibility(View.GONE);
						break;
				}
			}

			@Override public void onPageSelected(int position) {

			}

			@Override public void onPageScrollStateChanged(int state) {

			}
		});

		actionBarDrawerToggle =
				new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.label_drawer_open,
						R.string.label_drawer_close) {
					@Override public void onDrawerOpened(View drawerView) {
						super.onDrawerOpened(drawerView);
					}

					@Override public void onDrawerClosed(View drawerView) {
						super.onDrawerClosed(drawerView);
						// Code here will execute once activity_drawer is closed
					}
				};

		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
					case 0:
						startActivity(new Intent(DrawerActivity.this, SettingActivity.class));
						drawerLayout.closeDrawers();
						break;
					case 1:
						startActivityForResult(new Intent(DrawerActivity.this, TipsActivity.class),
								SELECT_TIP_TYPE);
						drawerLayout.closeDrawers();
						break;
					case 2:
						startActivity(new Intent(DrawerActivity.this, LegalActivity.class));
						drawerLayout.closeDrawers();
						break;
					case 3:
						showLoadingDialog("Logging out...");
						new Handler().postDelayed(new Runnable() {
							@Override public void run() {
								dismissLoadingDialog();
								UnumSession session2 = new UnumSession(DrawerActivity.this);

								//logout from unum server
								WebServiceCall webServiceCall2 = new WebServiceCall(DrawerActivity.this);
								webServiceCall2.logoutUser();
								Database.deleteSingleUser(session2.getUNUM_USER_ID());
								Log("USER DELETED." + session2.getUNUM_USER_NAME());
								//clear session
								session2.resetUnumSession();
								instagramApp.resetAccessToken();
								//login with new user
								ArrayList<LoggedInstaUsers.User> userArrayList = Database.getAllUsers();
								for (LoggedInstaUsers.User user : userArrayList) {
									Log("Existing User = " + user.getUsername());
								}
								if (!userArrayList.isEmpty()) {
									addNew = false;
									LoggedInstaUsers.User user = userArrayList.get(0);
									instagramApp.setmAccessToken(user.getAccessToken());
									instagramApp.storeAccessToken(user.getAccessToken(), user.getId(),
											user.getUsername(), user.getName());
									instagramApp.fetchUserName(handler);
								} else {
									startActivity(new Intent(DrawerActivity.this, LoginActivity.class));
									finish();
								}
							}
						}, 3);

						break;
					case 4:
						showLoadingDialog("Logging out...");
						new Handler().postDelayed(new Runnable() {
							@Override public void run() {
								WebServiceCall webServiceCall = new WebServiceCall(DrawerActivity.this);
								webServiceCall.logoutUser();

								UnumSession session = new UnumSession(DrawerActivity.this);
								Database.deleteAllUsers();
								session.resetUnumSession();
								instagramApp.resetAllAccessTokens();

								dismissLoadingDialog();
								startActivity(new Intent(DrawerActivity.this, LoginActivity.class));
								finish();
							}
						}, 3);

						break;
				}
			}
		});
	}

	private void addDrawerMenus() {
		drawerMenu.clear();
		for (int i = 0; i < item.length; i++) {
			DrawerItem drawerItem = new DrawerItem(item[i]);
			drawerMenu.add(drawerItem);
		}

		LoggedInstaUsers.User user = Database.getActiveUser();
		Log("CURRENT ACTIVE USER = " + user.getUsername());
		DrawerItem drawerItem = new DrawerItem("LOG OUT OF @" + user.getUsername());
		drawerMenu.add(drawerItem);

		final ArrayList<LoggedInstaUsers.User> userArrayList = Database.getAllUsers();
		if (userArrayList.size() > 1) {
			DrawerItem drawerItem2 = new DrawerItem("LOG OUT OF ALL ACCOUNTS");
			drawerMenu.add(drawerItem2);
		}
		drawerAdapter.setDrawerMenu(drawerMenu);
	}

	private ArrayList<String> getInstagramUserList(ArrayList<LoggedInstaUsers.User> userArrayList) {
		final ArrayList<String> list = new ArrayList<String>();
		int size = userArrayList.size();
		for (int i = 0; i < size; i++) {
			LoggedInstaUsers.User u = userArrayList.get(i);
			if (instagramApp.getTOken().equals(u.getAccessToken())) {
				selectedPos = i;
			}
			list.add("@" + u.getUsername());
		}
		list.add(getString(R.string.message_add_new_acc));

		return list;
	}

	// add items into spinner dynamically
	public void addItemsToSpinner() {

		final ArrayList<LoggedInstaUsers.User> userArrayList = Database.getAllUsers();
		ArrayList<String> list = getInstagramUserList(userArrayList);
		// Custom ArrayAdapter with spinner item layout to set popup background
		spinAdapter = new ToolbarSpinnerAdapter(getApplicationContext(), list);
		spinner_nav.setAdapter(spinAdapter);
		spinner_nav.setSelection(selectedPos);
		spinner_nav.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
				if (mLastSpinnerPosition == position) {
					return; //do nothing
				}
				mLastSpinnerPosition = position;
				if (position == list.size() - 1) {
					//Add new account
					addNew = true;
					instagramApp.authorize();

					instagramApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
						@Override public void onSuccess() {
							instagramApp.fetchUserName(handler);
						}

						@Override public void onFail(String error) {
							showToast(error);
						}
					});
				} else {
					addNew = false;
					LoggedInstaUsers.User user = userArrayList.get(position);
					//AppLogger.print(">>>>>>> UPDATE USER = " + user.getUsername() + " / " + user.getName());
					instagramApp.setmAccessToken(user.getAccessToken());
					instagramApp.storeAccessToken(user.getAccessToken(), user.getId(), user.getUsername(),
							user.getName());
					instagramApp.fetchUserName(handler);
				}
			}

			@Override public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override protected void onResume() {
		super.onResume();
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if (theme > 3) {
			menuSchedule.setImageResource(R.drawable.ic_menu_schedule_white);
			homeUpDrawable =
					ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_logo_with_bg_white,
							getApplicationContext().getTheme());
			actionBarDrawerToggle.setHomeAsUpIndicator(homeUpDrawable);
			layoutDrawerList.setBackgroundColor(Color.BLACK);
			drawerAdapter.setTextColorTo(Color.WHITE);
			spinAdapter.setTextColorTo(Color.WHITE);
			toolbar.setBackgroundColor(Color.BLACK);
			menuSwap.setTextColor(Color.WHITE);
		} else {
			menuSchedule.setImageResource(R.drawable.ic_menu_schedule);
			homeUpDrawable =
					ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_logo_with_bg,
							getApplicationContext().getTheme());
			actionBarDrawerToggle.setHomeAsUpIndicator(homeUpDrawable);
			layoutDrawerList.setBackgroundColor(Color.WHITE);
			drawerAdapter.setTextColorTo(Color.BLACK);
			spinAdapter.setTextColorTo(Color.BLACK);
			toolbar.setBackgroundColor(Color.WHITE);
			menuSwap.setTextColor(Color.BLACK);
		}
		drawerAdapter.notifyDataSetChanged();
		spinAdapter.notifyDataSetChanged();
		menuSchedule.invalidate();
	}

	@Override protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

		Drawable drawable =
				ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_logo_with_bg,
						getApplicationContext().getTheme());

		actionBarDrawerToggle.setHomeAsUpIndicator(drawable);
		actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
					drawerLayout.closeDrawer(GravityCompat.START);
				} else {
					drawerLayout.openDrawer(GravityCompat.START);
				}
			}
		});
		actionBarDrawerToggle.syncState();

		loadFragment();
	}

	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log("activity = onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SELECT_TIP_TYPE) {
			if (resultCode == Activity.RESULT_OK) {

				queueFragment.isTutorial = true;
				queueFragment.enableToolbarSelection(false);

				String tipType = data.getStringExtra(BaseActivity.KEY_TIP);
				if (tipType.equalsIgnoreCase("addDeletePostTip")) {
					new TipDialogUtils(DrawerActivity.this, mTips, 1);
				} else if (tipType.equalsIgnoreCase("postTip")) {
					new TipDialogUtils(DrawerActivity.this, mTips, 6);
				} else if (tipType.equalsIgnoreCase("rearrangeTip")) {
					new TipDialogUtils(DrawerActivity.this, mTips, 3);
				} else if (tipType.equalsIgnoreCase("editTips")) {
					new TipDialogUtils(DrawerActivity.this, mTips, 5);
				} else if (tipType.equalsIgnoreCase("tileTip")) {
					new TipDialogUtils(DrawerActivity.this, mTips, 0);
				}
			}
		}
	}

	@Override public void onClick(View v) {
		switch (v.getId()) {
			case R.id.menuSchedule:
				startActivity(new Intent(DrawerActivity.this, ScheduleTabbedActivity.class));
				break;
			case R.id.menuSwap:
				mSwapDrafrs = new WebServiceCall.SwapDrafrs() {
					@Override public void onSwapSuccess() {
						queueFragment.getInstagramImages();
						draftQueueFragment.getInstagramImages();
					}

					@Override public void onSwapFailure() {

					}
				};
				mWebServiceCall = new WebServiceCall(DrawerActivity.this, instagramApp, userInfoHashmap,
						DrawerActivity.this);
				mWebServiceCall.swapDrafts(mSwapDrafrs);
				break;
		}
	}

	public static HashMap<String, String> getUserInfoHashmap() {
		return userInfoHashmap;
	}

	public interface LoginChangeListener {
		public void onLoginChange(HashMap<String, String> userInfoMap);
	}

	@Override public void loginSuccess() {
		AppLogger.print("loginSuccess ");
		addItemsToSpinner();
		addDrawerMenus();

		if (ui_loading > 0) {
			loginChanged = true;
			if (addNew) {
				addNew = false;
			}
			((LoginChangeListener) mSectionsPagerAdapter.getCurrentFragment()).onLoginChange(
					userInfoHashmap);
		} else {
			loadFragment();
		}
		drawerLayout.closeDrawers();
	}

	@Override public void loginFail() {
		showToast(getString(R.string.some_err_occured));
	}

	@Override public void onSwapSuccess() {

	}

	@Override public void onSwapFailure() {

	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		private Fragment mCurrentFragment;

		public Fragment getCurrentFragment() {
			return mCurrentFragment;
		}

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override public void setPrimaryItem(ViewGroup container, int position, Object object) {
			if (getCurrentFragment() != object) {
				mCurrentFragment = ((Fragment) object);
			}
			super.setPrimaryItem(container, position, object);
		}

		@Override public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class below).
			Fragment mFragment = null;
			switch (position) {
				case 0:
					queueFragment = new QueueFragment();
					Bundle bundle = new Bundle();
					bundle.putSerializable(BaseActivity.KEY_USER_INFO, userInfoHashmap);
					queueFragment.setArguments(bundle);
					mFragment = queueFragment;
					break;
				case 1:
					draftQueueFragment = new DraftQueueFragment();
					Bundle b = new Bundle();
					b.putSerializable(BaseActivity.KEY_USER_INFO, userInfoHashmap);
					draftQueueFragment.setArguments(b);
					mFragment = draftQueueFragment;
					break;
			}
			return mFragment;
		}

		@Override public int getCount() {
			// Show 2 total pages.
			return 2;
		}
	}
}
//Example 1
//TSnackbar.make(findViewById(R.id.coordinatorLayout),"Hello from TSnackBar.", TSnackbar.LENGTH_LONG).show();
//Example 2
//TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "A Snackbar is a lightweight material design method for providing feedback to a user, while optionally providing an action to the user.", TSnackbar.LENGTH_LONG);
//snackbar.setActionTextColor(Color.WHITE);
//View snackbarView = snackbar.getView();
//snackbarView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
//textView.setTextColor(Color.YELLOW);
//snackbar.show();
