package com.unum.android.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.unum.android.R;
import com.unum.android.utils.GlideUtils;

/**
 * Created by Nishant Shah on 22-Sep-16.
 */
public class UnumProgressView extends FrameLayout {
	public UnumProgressView(Context context) {
		super(context);
		init(context);
	}

	public UnumProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public UnumProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public UnumProgressView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context);
	}
	private void init(Context context) {
		LayoutInflater inflater =
				(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rootView = inflater.inflate(R.layout.coumpound_unum_progress_view, this, true);
		ImageView loadingView= (ImageView) rootView.findViewById(R.id.loadingView);
		GlideUtils.loadGif(context,loadingView);
	}
}
