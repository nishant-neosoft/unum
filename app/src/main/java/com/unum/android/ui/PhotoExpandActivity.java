package com.unum.android.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import com.unum.android.R;
import com.unum.android.data.model.Image;
import com.unum.android.utils.GlideUtils;
import java.util.ArrayList;

/**
 * Created by Nishant Shah on 16-Sep-16.
 */
public class PhotoExpandActivity extends BackButtonActivity {

	private ViewPager mPager;
	private ArrayList<Image> images;
	private long position;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		getSupportActionBar().setTitle("");
		getSupportActionBar().collapseActionView();
		getSupportActionBar().hide();
		setContentView(R.layout.activity_photo_expand);

		init();

	}

	private void init() {
		images=getIntent().getParcelableArrayListExtra("images");
		position=getIntent().getLongExtra("position",0);

		for(Image image : new ArrayList<>(images)){
			if(image.getPath().startsWith(QueueFragment.EMPTY_IMAGE_PATH)){
				images.remove(image);
			}
		}

		int indexAt = 0;
		for(int i = 0; i < images.size(); i++){
			if(images.get(i).getId() == position)
			{
				indexAt = i;
				break;
			}
		}

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(new CustomPagerAdapter(this,images));
		mPager.setCurrentItem(indexAt);
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case android.R.id.home:
				finish();
		}
		return super.onOptionsItemSelected(item);
	}

	public class CustomPagerAdapter extends PagerAdapter {

		private Context mContext;
		private ArrayList<Image> images;
		private LayoutInflater inflater;
		private int minWidth;

		public CustomPagerAdapter(Context context, ArrayList<Image> images) {
			mContext = context;
			this.images=images;
			inflater = LayoutInflater.from(mContext);

			final WindowManager windowManager = (WindowManager) context.getApplicationContext()
					.getSystemService(Context.WINDOW_SERVICE);
			final DisplayMetrics metrics = new DisplayMetrics();
			windowManager.getDefaultDisplay().getMetrics(metrics);
			minWidth = metrics.widthPixels;
		}

		@Override
		public Object instantiateItem(ViewGroup collection, int position) {
			Image image = images.get(position);
			ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_photo_expand, collection, false);
			ImageView imageView= (ImageView) layout.findViewById(R.id.imageView);
			//imageView.getLayoutParams().width = minWidth;
			imageView.setMinimumWidth(minWidth);
			if(image.getName().startsWith(QueueFragment.INSTA_IMAGE)){
				GlideUtils.getImageFromPath(mContext, imageView, image.getInsta_high_resolution_image_path());
			}else {
				GlideUtils.getImageFromPath(mContext, imageView, image.getPath());
			}
			collection.addView(layout);
			return layout;
		}

		@Override
		public void destroyItem(ViewGroup collection, int position, Object view) {
			collection.removeView((View) view);
		}

		@Override
		public int getCount() {
			return images.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}


	}
}
