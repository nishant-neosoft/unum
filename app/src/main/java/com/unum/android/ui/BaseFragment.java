package com.unum.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.utils.AppLogger;

public class BaseFragment extends Fragment {

	protected Activity activity;
	protected ProgressDialog pDialog;
	protected Toast toast;

	public BaseFragment() {
		// Required empty public constructor
	}

	public void Log(String msg) {
		AppLogger.d("", msg);
	}

	public void showToast(String message) {
		toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
		toast.show();
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = getActivity();
	}

	protected void showLoadingDialog(Context context) {
		pDialog = new ProgressDialog(context, R.style.LoadingDialogTheme);
		pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		pDialog.show();
	}

	protected void showLoadingDialog(Context context, String msg) {
		pDialog = new ProgressDialog(context);
		pDialog.setMessage(msg);
		pDialog.show();
	}

	protected void dismissLoadingDialog() {
		try {
			pDialog.dismiss();
		} catch (Exception e) {
			//to prevent WindowLeaked exception.
			e.printStackTrace();
		}
	}

	@Override public void onDestroy() {
		if (toast != null) {
			toast.cancel();
		}
		super.onDestroy();
	}
}
