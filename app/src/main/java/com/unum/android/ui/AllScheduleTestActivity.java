package com.unum.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.google.gson.Gson;
import com.unum.android.R;
import com.unum.android.adapter.ScheduleListAdapter;
import com.unum.android.data.local.Alarm;
import com.unum.android.data.local.Database;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.utils.AppLogger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by haridas on 31/8/16.
 */
public class AllScheduleTestActivity extends BackButtonActivity implements View.OnClickListener {

	private ScheduleListAdapter scheduleListAdapter;
	private ListView scheduleList;
	private List<Alarm> alarmList;
	private Button btnGetReminder, btnCreateReminder, btnUpdateReminder, btnDeleteReminder,
			btnGetCustom, btnCreateCustom, btnUpdateCustom, btnDeleteCustom;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_schedule_test);
		scheduleList = (ListView) findViewById(R.id.schedulList);

		btnGetReminder = (Button) findViewById(R.id.btnGetReminder);
		btnGetReminder.setOnClickListener(this);
		btnCreateReminder = (Button) findViewById(R.id.btnCreateReminder);
		btnCreateReminder.setOnClickListener(this);
		btnUpdateReminder = (Button) findViewById(R.id.btnUpdateReminder);
		btnUpdateReminder.setOnClickListener(this);
		btnDeleteReminder = (Button) findViewById(R.id.btnDeleteReminder);
		btnDeleteReminder.setOnClickListener(this);
		btnGetCustom = (Button) findViewById(R.id.btnGetCustom);
		btnGetCustom.setOnClickListener(this);
		btnCreateCustom = (Button) findViewById(R.id.btnCreateCustom);
		btnCreateCustom.setOnClickListener(this);
		btnUpdateCustom = (Button) findViewById(R.id.btnUpdateCustom);
		btnUpdateCustom.setOnClickListener(this);
		btnDeleteCustom = (Button) findViewById(R.id.btnDeleteCustom);
		btnDeleteCustom.setOnClickListener(this);
	}

	private void getReminders() {
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.getUserReminders(new WebServiceCall.Callback<List<GetUserReminderResponse>>() {
			@Override public void onSuccess(List<GetUserReminderResponse> getUserReminderResponses) {
				AppLogger.d("TEST REMINDER :: " + getUserReminderResponses.size());
			}

			@Override public void onFailure() {
				AppLogger.d("GET REMINDER FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void getSchedules() {
		//WebServiceCall webServiceCall = new WebServiceCall(this);
		//webServiceCall.getAllSchedules(new WebServiceCall.Callback<ResponseBody>() {
		//	@Override public void onSuccess(ResponseBody responseBody) {
		//		AppLogger.d("GET CUSTOM SUCCESS :: " + new Gson().toJson(responseBody));
		//	}
		//
		//	@Override public void onFailure() {
		//		AppLogger.d("GET CUSTOM FAIL");
		//	}
		//
		//	@Override public void onProgressUpdate(int progress) {
		//
		//	}
		//});
	}

	private void createReminder() {
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.createReminder("", true, false, false, false, false, false, false,
				new WebServiceCall.Callback<String>() {
					@Override public void onSuccess(String s) {
						AppLogger.d("CREATE REMINDER SUCCESS :: " + new Gson().toJson(s));
					}

					@Override public void onFailure() {
						AppLogger.d("CREATE REMINDER FAIL");
					}

					@Override public void onProgressUpdate(int progress) {

					}
				});
	}

	private void updateReminder() {
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.updateReminder("test", "", true, false, false, false, false, false, false,
				new WebServiceCall.Callback<String>() {
					@Override public void onSuccess(String s) {
						AppLogger.d("UPDATE REMINDER SUCCESS :: " + new Gson().toJson(s));
					}

					@Override public void onFailure() {
						AppLogger.d("UPDATE REMINDER FAIL");
					}

					@Override public void onProgressUpdate(int progress) {

					}
				});
	}

	private void createSchedule() {
		ArrayList<String> postIds = new ArrayList<String>();
		postIds.add("test");
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.updateSchedule("", postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				AppLogger.d("CREATE SCHEDULE SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				AppLogger.d("CREATE SCHEDULE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void updateSchedule() {
		ArrayList<String> postIds = new ArrayList<String>();
		postIds.add("test");
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.updateSchedule("", postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				AppLogger.d("UPDATE SCHEDULE SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				AppLogger.d("UPDATE SCHEDULE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void deleteReminder() {
		ArrayList<String> scheduleIds = new ArrayList<String>();
		scheduleIds.add("test");
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.deleteReminder(scheduleIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				AppLogger.d("DELETE REMINDER SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				AppLogger.d("DELETE REMINDER FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void deleteSchedule() {
		ArrayList<String> postIds = new ArrayList<String>();
		postIds.add("test");
		WebServiceCall webServiceCall = new WebServiceCall(this);
		webServiceCall.deleteSchedules(postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				AppLogger.d("DELETE SCHEDULE SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				AppLogger.d("DELETE SCHEDULE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.add:
				Intent i = new Intent(this, CreateReminderActivity.class);
				startActivity(i);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_allschdule, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override protected void onResume() {
		super.onResume();
		Database.init(this);
		//scheduleListAdapter = new ScheduleListAdapter(this, Database.getAll());
	//	scheduleList.setAdapter(scheduleListAdapter);
	}

	@Override protected void onPause() {
		super.onPause();
		Database.deactivate();
	}

	@Override public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btnGetReminder:
				getReminders();
				break;
			case R.id.btnCreateReminder:
				createReminder();
				break;
			case R.id.btnUpdateReminder:
				updateReminder();
				break;
			case R.id.btnDeleteReminder:
				deleteReminder();
				break;
			case R.id.btnGetCustom:
				getSchedules();
				break;
			case R.id.btnCreateCustom:
				createSchedule();
				break;
			case R.id.btnUpdateCustom:
				updateSchedule();
				break;
			case R.id.btnDeleteCustom:
				deleteSchedule();
				break;
		}
	}
}
