package com.unum.android.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adobe.creativesdk.aviary.AdobeImageIntent;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.unum.android.R;
import com.unum.android.adapter.QueueAdapter;
import com.unum.android.data.model.Image;
import com.unum.android.data.model.request.UpdateMultiplePost;
import com.unum.android.data.model.response.GetUserPostsResponse;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.ui.helper.OnStartDragListener;
import com.unum.android.ui.helper.SimpleItemTouchHelperCallback;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Snehal on 8/29/2016.
 */
public class DraftQueueFragment extends HomeBaseFragment
		implements QueueAdapter.ViewHolder.OnItemClickListener, View.OnClickListener,
		DeleteImagesDialog.onDeleteListener, CaptionDialog.OnPostUpdated,
		TileImageDialogFragment.OnImageTiled, OnStartDragListener, DrawerActivity.LoginChangeListener {
	private static final int REQUEST_CODE_PICKER = 10000;
	private static final int TOTAL_EMPTY_GRID_BOX = 18;
	final int REQUEST_IMAGE_EDIT = 1;
	private ArrayList<Image> images;
	private ArrayList<Image> selectedImages;
	private ArrayList<Integer> selectedIndexes;
	private ArrayList<com.nguyenhoanglam.imagepicker.model.Image> selectedGalleryImages;
	private QueueAdapter adapter;
	private RelativeLayout mainLayout;
	private RecyclerView recyclerView;
	private ImageView btnTileImage;
	private ImageView btnAddImage;
	private ImageView btnShuffleImage;
	private ImageView btnImageEdit;
	private ImageView btnImageDelete;
	private ImageView btnAddComment;
	private ImageView btnPost;
	private ImageView btnGridShift;
	private ImageView btnPhantomMode;
	private ImageView btnHideInsta;
	private LinearLayout layoutOptions;
	private ItemTouchHelper mItemTouchHelper;
	private static int count = 0;

	@Override public void fetchUserUnumImages() {
		showLoadingDialog(getActivity());
		WebServiceCall.Callback<List<GetUserPostsResponse>> callback =
				new WebServiceCall.Callback<List<GetUserPostsResponse>>() {
					@Override public void onSuccess(List<GetUserPostsResponse> getUserPostsResponses) {
						if (isAdded()) {
							dismissLoadingDialog();
							ArrayList<GetUserPostsResponse> posts =
									(ArrayList<GetUserPostsResponse>) getUserPostsResponses;
							Collections.sort(posts, new CustomComparator());

							int size = posts.size();
							if (null != images) {
								images.clear();
							}

							boolean isAdded = false;
							for (int i = 0; i < TOTAL_EMPTY_GRID_BOX; i++) {
								isAdded = false;
								for (int j = 0; j < size; j++) {
									GetUserPostsResponse userPostsResponse = posts.get(j);
									if (i == userPostsResponse.getIndex()) {
										//Log("--------Adding unum images ... ");
										long id = userPostsResponse.getIndex();
										String postId = userPostsResponse.get_id();
										String msg = userPostsResponse.getMessage();
										String name = EMPTY_IMAGE + i;
										String path = userPostsResponse.getImageUrl();
										String draft = userPostsResponse.getDraft();

										if (draft.equalsIgnoreCase(mUnumSession.getUNUM_QUEUE_DRAFT())) {
											Image image = new Image(id, postId, msg, name, path, draft, false);
											Log(id + " / " + name + " / " + path);
											images.add(image);
											isAdded = true;
										}
									}
								}
								if (!isAdded) {
									//Log("--------Adding empty images ... ");
									long id = i;
									String postId = "0";
									String name = EMPTY_IMAGE + i;
									String path = EMPTY_IMAGE + i;
									String msg = "";

									Image image = new Image(id, postId, msg, name, path, "", false);
									Log(i + " / " + name + " / " + path);
									images.add(image);
								}
							}

							Log("--------Adding instagram images ... ");
							size = dataArrayList.size();
							for (int i = 0; i < size; i++) {
								long id = i + TOTAL_EMPTY_GRID_BOX;
								String postId = "0";
								String msg = "";
								String name = INSTA_IMAGE;
								String path = dataArrayList.get(i).getImages().getThumbnail().getUrl();

								Image image = new Image(id, postId, msg, name, path, "", false);
								image.setInsta_high_resolution_image_path(
										dataArrayList.get(i).getImages().getStandard_resolution().getUrl());
								Log(id + " / " + name + " / " + path);
								images.add(image);
							}
							orientationBasedUI(getResources().getConfiguration().orientation);
						}
					}

					@Override public void onFailure() {

					}

					@Override public void onProgressUpdate(int progress) {

					}
				};

		mWebServiceCall.getUsersPosts(callback);
	}

	@Override public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
		mItemTouchHelper.startDrag(viewHolder);
	}

	@Override public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser && count == 0) {
			count++;
			getInstagramImages();
		}
		AppLogger.print("setUserVisibleHint Draft = loginChnaged = "+ DrawerActivity.loginChanged);
		if (isVisibleToUser && DrawerActivity.loginChanged) {
			HashMap<String, String> current = ((DrawerActivity) getActivity()).getUserInfoHashmap();
			userInfo = current;
			DrawerActivity.loginChanged = false;
			getInstagramImages();
		}
	}

	@Override public void orientationBasedUI(int orientation) {
		final WindowManager windowManager = (WindowManager) getActivity().getApplicationContext()
				.getSystemService(Context.WINDOW_SERVICE);
		final DisplayMetrics metrics = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(metrics);

		int columns = orientation == Configuration.ORIENTATION_PORTRAIT ? 3 : 5;

		GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), columns);
		recyclerView.setLayoutManager(layoutManager);

		if (adapter != null) {
			int size = metrics.widthPixels / columns;
			adapter.setImageSize(size);
		}

		recyclerView.setAdapter(adapter);
		recyclerView.scrollToPosition(9);
	}

	@Override public void onSingleTap(int position) {
		boolean isInstaImage = false;
		if (INSTA_IMAGE.equals(images.get(position).getName())) {
			isInstaImage = true;
		} else {
			adapter.removeInstaImageIfAny();
		}
		clickImage(position);
		enableToolbarSelection(isInstaImage);
	}

	@Override public void onDoubleTap(int position) {
		startActivity(
				new Intent(getActivity(), PhotoExpandActivity.class).putExtra("position", position)
						.putParcelableArrayListExtra("images", images));
	}

	@Override public void onItemMoved() {
		//called after drag and drop.
		//TODO just post images.
	}

	public void enableToolbarSelection(boolean isInstagramImageSelected) {

		boolean selectedEmptyImages = false;

		for (Image image : selectedImages) {
			if (image.getPath().startsWith(EMPTY_IMAGE)) {
				selectedEmptyImages = true;
			} else {
				selectedEmptyImages = false;
				break;
			}
		}

		btnAddImage.setVisibility(View.VISIBLE);
		btnAddComment.setVisibility(View.VISIBLE);
		btnImageDelete.setVisibility(View.VISIBLE);
		btnImageEdit.setVisibility(View.VISIBLE);
		btnShuffleImage.setVisibility(View.VISIBLE);
		btnTileImage.setVisibility(View.VISIBLE);
		btnGridShift.setVisibility(View.VISIBLE);
		btnPhantomMode.setVisibility(View.VISIBLE);
		btnHideInsta.setVisibility(View.VISIBLE);

		btnAddImage.setEnabled(true);
		btnAddComment.setEnabled(true);
		btnImageDelete.setEnabled(true);
		btnImageEdit.setEnabled(true);
		btnShuffleImage.setEnabled(true);
		btnTileImage.setEnabled(true);
		btnGridShift.setEnabled(true);
		btnPhantomMode.setEnabled(true);
		btnHideInsta.setEnabled(true);

		//Selecting only empty grid image
		if (selectedEmptyImages) {
			btnAddImage.setEnabled(true);
			btnAddComment.setEnabled(false);
			btnImageDelete.setVisibility(View.GONE);
			btnPost.setVisibility(View.GONE);
			btnImageEdit.setEnabled(false);
			btnShuffleImage.setEnabled(false);
			btnTileImage.setEnabled(false);
		} else {
			btnAddImage.setVisibility(View.GONE);
			btnAddComment.setEnabled(true);
			btnImageDelete.setEnabled(true);
			btnImageEdit.setEnabled(false);
			btnShuffleImage.setEnabled(true);
			btnTileImage.setEnabled(true);
			btnPost.setVisibility(View.GONE);
		}

		if (!selectedEmptyImages && selectedImages.size() == 1) {
			btnAddImage.setVisibility(View.GONE);
			btnShuffleImage.setVisibility(View.GONE);
			btnAddComment.setVisibility(View.VISIBLE);
			btnImageDelete.setVisibility(View.VISIBLE);
			btnPost.setVisibility(View.VISIBLE);
			btnImageEdit.setVisibility(View.VISIBLE);
			btnTileImage.setVisibility(View.VISIBLE);
			btnImageEdit.setEnabled(true);
		}

		if (isInstagramImageSelected) {
			btnAddImage.setVisibility(View.GONE);
			btnAddComment.setVisibility(View.GONE);
			btnImageDelete.setVisibility(View.GONE);
			btnImageEdit.setVisibility(View.GONE);
			btnShuffleImage.setVisibility(View.GONE);
			btnTileImage.setVisibility(View.GONE);
			btnPost.setVisibility(View.GONE);

			btnGridShift.setVisibility(View.VISIBLE);
			btnPhantomMode.setVisibility(View.VISIBLE);
			btnHideInsta.setVisibility(View.VISIBLE);
		} else {
			btnGridShift.setVisibility(View.GONE);
			btnPhantomMode.setVisibility(View.GONE);
			btnHideInsta.setVisibility(View.GONE);
		}

		if (isInstagramImageSelected) {
			adapter.removeAllExceptInstaImages();
		}
		layoutOptions.setVisibility(selectedImages.size() > 0 ? View.VISIBLE : View.GONE);
	}

	private void clickImage(int position) {
		int selectedItemPosition = selectedImagePosition(images.get(position));
		if (selectedItemPosition == -1) {
			adapter.addSelected(images.get(position));
		} else {
			adapter.removeSelectedPosition(selectedItemPosition, position);
		}
	}

	private int selectedImagePosition(Image image) {
		for (int i = 0; i < selectedImages.size(); i++) {
			if (selectedImages.get(i).getPath().equals(image.getPath())) {
				return i;
			}
		}

		return -1;
	}

	@Override public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnPost:
				if (selectedImages.size() == 1) {
					postImageToInstagram();
				}
				break;
			case R.id.btnGridShift:
				featureComingSoon();
				break;
			case R.id.btnPhantomMode:
				featureComingSoon();
				break;
			case R.id.btnHideInsta:
				featureComingSoon();
				break;
			case R.id.btnTileImage:
				int emptyBlock = 0;
				for (int i = 0; i < images.size(); i++) {
					if (images.get(i).getPath().startsWith(EMPTY_IMAGE)) {
						emptyBlock++;
					}
				}

				if (emptyBlock >= 9) {
					FragmentManager tileImageFragment = getFragmentManager();
					TileImageDialogFragment tileImageDialog =
							TileImageDialogFragment.newInstance(selectedImages);
					tileImageDialog.setImageTileCallBack(this);
					tileImageDialog.show(tileImageFragment, "TileImageDialog");
				} else {
					showToast(getString(R.string.message_empty_block));
				}

				break;
			case R.id.btnAddImage:
				Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
				intent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_MULTIPLE);
				intent.putExtra(ImagePickerActivity.INTENT_EXTRA_LIMIT, selectedImages.size());
				intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SHOW_CAMERA, false);
				intent.putExtra(ImagePickerActivity.INTENT_EXTRA_TITLE,
						getString(R.string.message_tap_to_select));

				startActivityForResult(intent, REQUEST_CODE_PICKER);
				break;
			case R.id.btnAddComment:
				FragmentManager fm = getFragmentManager();
				CaptionDialog captionDialog =
						CaptionDialog.newInstance(selectedImages, mUnumSession.getUNUM_QUEUE_DRAFT());
				captionDialog.setCallback(this);
				captionDialog.show(fm, "CaptionDialog");
				break;
			case R.id.btnImageDelete:
				FragmentManager fm2 = getFragmentManager();
				DeleteImagesDialog deleteImagesDialog = DeleteImagesDialog.newInstance(selectedImages);
				deleteImagesDialog.setListener(this);
				deleteImagesDialog.show(fm2, "DeleteImagesDialog");
				break;
			case R.id.btnShuffleImage:
				performItemShuffle();
				break;
			case R.id.btnImageEdit:
				if (selectedImages.size() > 0) {
					Uri imageUri = Uri.parse(selectedImages.get(0).getPath().toString());
					Intent imageEditorIntent =
							new AdobeImageIntent.Builder(getActivity()).setData(imageUri).build();
					startActivityForResult(imageEditorIntent, REQUEST_IMAGE_EDIT);
				}
				break;
		}
	}

	private void postImageToInstagram() {
		if (selectedImages.size() > 0) {
			String mediaPath = selectedImages.get(0).getPath().toString();
			String caption = selectedImages.get(0).getMessage().toString();

			String mimeType;
			if (mediaPath.lastIndexOf(".") != -1) {
				String ext = mediaPath.substring(mediaPath.lastIndexOf(".") + 1);
				MimeTypeMap mime = MimeTypeMap.getSingleton();
				mimeType = mime.getMimeTypeFromExtension(ext);
			} else {
				mimeType = "image/jpeg";
			}

			Intent postInstageram = new Intent(getActivity(), PostToInstagramActivity.class);
			postInstageram.putExtra("CONTENT_TYPE", mimeType);
			postInstageram.putExtra("MEDIA_PATH", mediaPath);
			postInstageram.putExtra("CAPTION", caption);
			startActivity(postInstageram);
		}
	}

	private void performItemShuffle() {
		int size = selectedImages.size();
		long[] array = new long[size];
		for (int i = 0; i < selectedImages.size(); i++) {
			array[i] = selectedImages.get(i).getId();
			AppLogger.print(
					"SELECTED : " + selectedImages.get(i).getId() + "/" + selectedImages.get(i).getPath());
		}
		Random rgen = new Random();  // Random number generator
		for (int i = 0; i < array.length; i++) {
			int randomPosition = rgen.nextInt(array.length);
			long temp = array[i];
			array[i] = array[randomPosition];
			array[randomPosition] = temp;
		}
		ArrayList<Image> copy = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			copy.add(selectedImages.get(i));
		}

		ArrayList<UpdateMultiplePost.Posts> postsForUpdate = new ArrayList<UpdateMultiplePost.Posts>();
		UnumSession unumSession = new UnumSession(getContext());
		String drafts = unumSession.getUNUM_DEFAULT_DRAFT();

		for (int i = 0; i < size; i++) {
			Image image = copy.get(i);
			images.set((int) array[i], copy.get(i));
			String path = image.getPath();
			if (!path.startsWith(QueueFragment.EMPTY_IMAGE_PATH)) {
				postsForUpdate.add(
						new UpdateMultiplePost().new Posts(image.getPostId(), drafts, image.getPath(),
								image.getMessage(), (int) array[i]));
			}
		}
		adapter.notifyDataSetChanged();
		UpdateMultiplePost updates = new UpdateMultiplePost(postsForUpdate);
		WebServiceCall webServiceCall = new WebServiceCall(getContext());
		webServiceCall.updateMultiplePosts(updates, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				AppLogger.print("UPDATE SUCCESS");
			}

			@Override public void onFailure() {
				AppLogger.print("UPDATE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {
			}
		});
	}

	@Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == getActivity().RESULT_OK && data != null) {
			switch (requestCode) {
				case REQUEST_CODE_PICKER:
					selectedGalleryImages =
							data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
					int size = selectedGalleryImages.size();
					for (int i = 0; i < size; i++) {
						int pos = (int) selectedImages.get(i).getId();
						String path = selectedGalleryImages.get(i).getPath();
						adapter.updateImagePathForPosition(pos, path);

						uploadFile(pos, path, mUnumSession.getUNUM_QUEUE_DRAFT(), null);
					}
					btnAddComment.setEnabled(true);
					break;

				case REQUEST_IMAGE_EDIT:
					Uri editedImageUri = data.getParcelableExtra(AdobeImageIntent.EXTRA_OUTPUT_URI);
					Log.e("EDITED IMAGE URI", editedImageUri.getPath());
					break;
			}
		}
	}

	@Override public void onResume() {
		super.onResume();

		adapter.setTheme(mUnumSession.getUNUM_DEFAULT_THEME());
		adapter.notifyDataSetChanged();
		if (mainLayout != null && mUnumSession != null) {
			int theme = mUnumSession.getUNUM_DEFAULT_THEME();
			if (theme > 3) {
				mainLayout.setBackgroundColor(Color.BLACK);
			} else {
				mainLayout.setBackgroundColor(Color.WHITE);
			}
		}
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_queue, container, false);
		if (selectedImages == null) {
			selectedImages = new ArrayList<>();
		}

		if (selectedIndexes == null) {
			selectedIndexes = new ArrayList<>();
		}

		if (selectedGalleryImages == null) {
			selectedGalleryImages = new ArrayList<>();
		}
		images = new ArrayList<>();

		adapter = new QueueAdapter(getActivity(), images, selectedImages, this);
		mainLayout = (RelativeLayout) view.findViewById(R.id.main);
		recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
		layoutOptions = (LinearLayout) view.findViewById(R.id.layoutOptions);
		btnTileImage = (ImageView) view.findViewById(R.id.btnTileImage);
		btnAddComment = (ImageView) view.findViewById(R.id.btnAddComment);
		btnAddImage = (ImageView) view.findViewById(R.id.btnAddImage);
		btnImageDelete = (ImageView) view.findViewById(R.id.btnImageDelete);
		btnShuffleImage = (ImageView) view.findViewById(R.id.btnShuffleImage);
		btnImageEdit = (ImageView) view.findViewById(R.id.btnImageEdit);
		btnPost = (ImageView) view.findViewById(R.id.btnPost);
		btnGridShift = (ImageView) view.findViewById(R.id.btnGridShift);
		btnPhantomMode = (ImageView) view.findViewById(R.id.btnPhantomMode);
		btnHideInsta = (ImageView) view.findViewById(R.id.btnHideInsta);

		btnTileImage.setOnClickListener(this);
		btnAddComment.setOnClickListener(this);
		btnAddImage.setOnClickListener(this);
		btnImageDelete.setOnClickListener(this);
		btnShuffleImage.setOnClickListener(this);
		btnImageEdit.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		btnGridShift.setOnClickListener(this);
		btnPhantomMode.setOnClickListener(this);
		btnHideInsta.setOnClickListener(this);

		layoutOptions.setVisibility(View.GONE);

		mainLayout.setBackgroundColor(getResources().getColor(R.color.black));

		ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
		mItemTouchHelper = new ItemTouchHelper(callback);
		mItemTouchHelper.attachToRecyclerView(recyclerView);

		return view;
	}

	@Override public void onDelete() {
		for (Image image : selectedImages) {
			int id = (int) image.getId();
			adapter.updateImagePathForPosition(id, EMPTY_IMAGE + id);
		}
		adapter.removeAllSelectedSingleClick();
		layoutOptions.setVisibility(View.GONE);
	}

	@Override public void onPostUpdate(String msg) {
		int size = selectedImages.size();
		if (size == 1) {
			int pos = (int) selectedImages.get(0).getId();
			adapter.updateMessageForImage(pos, msg);
		} else {
			for (int i = 0; i < size; i++) {
				int pos = (int) selectedImages.get(i).getId();
				adapter.updateMessageForImage(pos, msg);
			}
		}

		adapter.removeAllSelectedSingleClick();

		layoutOptions.setVisibility(View.GONE);
	}

	@Override
	public void onImageTiled(ArrayList<TileImageDialogFragment.SlicedImageData> tiledImage) {
		if (tiledImage.size() > 0) {
			int size = tiledImage.size();
			for (int i = 0; i < size; i++) {
				int pos = (int) tiledImage.get(i).getIndex();
				String path = tiledImage.get(i).getFilePath();
				adapter.updateImagePathForPosition(pos, path);
				uploadFile(pos, path, mUnumSession.getUNUM_QUEUE_DRAFT(), new OnUploadComplete() {
					@Override public void imageUploaded() {
						btnAddComment.setEnabled(true);
					}
				});
			}
		}
	}

	@Override public void onLoginChange(HashMap<String, String> userInfoMap) {
		this.userInfo = userInfoMap;
		getInstagramImages();
	}
}
