package com.unum.android.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.unum.android.R;
import com.unum.android.utils.UnumSession;

public class CalendarFragment extends Fragment {

	private FrameLayout main;
	int theme = 0;

	public static CalendarFragment newInstance() {
		CalendarFragment fragment = new CalendarFragment();
		return fragment;
	}

	public CalendarFragment() {
		// Required empty public constructor
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override public void onPrepareOptionsMenu(Menu menu) {
		MenuItem item=menu.findItem(R.id.add);
		item.setVisible(false);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_calendar, container, false);
		main = (FrameLayout) view.findViewById(R.id.main);

		UnumSession mUnumSession = new UnumSession(getActivity());
		theme = mUnumSession.getUNUM_DEFAULT_THEME();

		return view;
	}

	@Override public void onResume() {
		super.onResume();
		if(main != null){
			if(theme > 3){
				main.setBackgroundColor(Color.BLACK);
			}else{
				main.setBackgroundColor(Color.WHITE);
			}
		}
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override public void onDetach() {
		super.onDetach();
	}
}
