package com.unum.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.instagram.InstaConstants;
import com.unum.android.instagram.InstagramApp;
import com.unum.android.ui.views.UnumProgressView;
import com.unum.android.utils.UnumSession;
import java.util.HashMap;

public class LoginActivity extends BaseActivity
		implements View.OnClickListener, WebServiceCall.LoginCallback {

	private InstagramApp instagramApp;
	private Button btnLoginInstagram;
	private WebServiceCall mWebServiceCall;
	private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private UnumSession mUnumSession;
	private UnumProgressView unumProgressView;


	private Handler handler = new Handler(new Handler.Callback() {
		@Override public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = instagramApp.getUserInfo();
				showLoading();
				mWebServiceCall = new WebServiceCall(LoginActivity.this, instagramApp, userInfoHashmap,
						LoginActivity.this);
				mWebServiceCall.loginWithUnum();
			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				showToast(getString(R.string.message_no_internet_message));
			}
			return false;
		}
	});

	private void showLoading() {
		unumProgressView.setVisibility(View.VISIBLE);
	}
	private void hideLoading(){
		unumProgressView.setVisibility(View.GONE);
	}

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
		setContentView(R.layout.activity_login);

		btnLoginInstagram = (Button) findViewById(R.id.btnLoginInstagram);
		unumProgressView = (UnumProgressView) findViewById(R.id.unumProgressView);
		btnLoginInstagram.setOnClickListener(this);
		showLoading();
		mUnumSession = new UnumSession(this);

		instagramApp = new InstagramApp(this, InstaConstants.CLIENT_ID, InstaConstants.CLIENT_SECRET,
				InstaConstants.CALLBACK_URL);
		if (instagramApp.hasAccessToken()) {
			btnLoginInstagram.setEnabled(false);
			//displayInfoDialogView();
			// tvSummary.setText("Connected as " + mApp.getUserName());
			//btnConnect.setText( "Disconnect" );
			//llAfterLoginView.setVisibility( View.VISIBLE );
			if (!mUnumSession.getUNUM_USER_ID().isEmpty()
					|| mUnumSession.getUNUM_USER_ID() != null
					|| !mUnumSession.getUNUM_USER_ID().equalsIgnoreCase("")) {
				instagramApp.fetchUserName(handler);
			}
		}else{
			hideLoading();
		}

		instagramApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
			@Override public void onSuccess() {
				instagramApp.fetchUserName(handler);
			}

			@Override public void onFail(String error) {
				Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void connectOrDisconnectUser() {
		if (instagramApp.hasAccessToken()) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
			builder.setMessage("Disconnect from Instagram?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							instagramApp.resetAccessToken();
						}
					})
					.setNegativeButton("No", null);
			final AlertDialog alert = builder.create();
			alert.show();
		} else {
			instagramApp.authorize();
		}
	}

	@Override public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnLoginInstagram:

				connectOrDisconnectUser();

				break;
		}
	}

	@Override public void loginSuccess() {
		Log("onSuccess Login unum");
		hideLoading();
		startActivity(
				new Intent(LoginActivity.this, DrawerActivity.class).putExtra(BaseActivity.KEY_USER_INFO,
						userInfoHashmap));
		finish();
	}

	@Override public void loginFail() {

	}
}
