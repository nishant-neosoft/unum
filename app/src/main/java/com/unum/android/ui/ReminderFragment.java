package com.unum.android.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.google.gson.Gson;
import com.unum.android.R;
import com.unum.android.adapter.ScheduleListAdapter;
import com.unum.android.data.model.response.GetUserReminderResponse;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.ui.views.UnumProgressView;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.util.List;

public class ReminderFragment extends BaseFragment {

	private RelativeLayout main;
	private ListView listView;
	private UnumProgressView unumProgressView;
	private List<GetUserReminderResponse> list;
	private int theme = 0;

	public final static int REQUEST_CODE = 105;
	public final static int RESULT_CODE = 106;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 */
	public static ReminderFragment newInstance() {
		ReminderFragment fragment = new ReminderFragment();
		return fragment;
	}

	public ReminderFragment() {
		// Required empty public constructor
	}

	@Override public void onResume() {
		super.onResume();
		if(main != null){
			if(theme > 3){
				main.setBackgroundColor(Color.BLACK);
			}else{
				main.setBackgroundColor(Color.WHITE);
			}
		}
	}

	private void showLoading() {
		unumProgressView.setVisibility(View.VISIBLE);
	}
	private void hideLoading(){
		unumProgressView.setVisibility(View.GONE);
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_reminder, container, false);
		main = (RelativeLayout) view.findViewById(R.id.main);
		unumProgressView = (UnumProgressView) view.findViewById(R.id.unumProgressView);
		listView = (ListView) view.findViewById(R.id.listView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(getActivity(), CreateReminderActivity.class);
				GetUserReminderResponse reminder = list.get(i);
				intent.putExtra(BaseActivity.KEY_REMINDER_ID, new Gson().toJson(reminder));
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		UnumSession mUnumSession = new UnumSession(getActivity());
		theme = mUnumSession.getUNUM_DEFAULT_THEME();

		getReminders();
		return view;
	}

	@Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log("resultCode = "+resultCode);
		if (resultCode == getActivity().RESULT_OK && data != null) {
			switch (requestCode) {
				case REQUEST_CODE:
					getReminders();
					break;
			}
		}
	}

	private void getReminders() {
		showLoading();
		WebServiceCall webServiceCall = new WebServiceCall(getActivity());
		webServiceCall.getUserReminders(new WebServiceCall.Callback<List<GetUserReminderResponse>>() {
			@Override public void onSuccess(List<GetUserReminderResponse> getUserReminderResponses) {
				list = getUserReminderResponses;
				AppLogger.d("TEST REMINDER :: " + list.size());
				hideLoading();
				ScheduleListAdapter scheduleListAdapter =
						new ScheduleListAdapter(getActivity(), getUserReminderResponses);
				listView.setAdapter(scheduleListAdapter);
			}

			@Override public void onFailure() {
				hideLoading();
				AppLogger.d("GET REMINDER FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override public void onDetach() {
		super.onDetach();
	}
}
