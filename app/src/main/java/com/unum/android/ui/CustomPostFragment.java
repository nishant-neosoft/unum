package com.unum.android.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.unum.android.R;
import com.unum.android.adapter.QueueAdapter;
import com.unum.android.data.model.Image;
import com.unum.android.data.model.response.GetAllSchedulesResponse;
import com.unum.android.data.model.response.GetUserPostsResponse;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.ui.views.UnumProgressView;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.DateUtil;
import com.unum.android.utils.UnumSession;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CustomPostFragment extends BaseFragment
		implements View.OnClickListener, QueueAdapter.ViewHolder.OnItemClickListener {

	private RelativeLayout main;
	private FrameLayout layoutGrid;
	private RecyclerView recyclerView;
	private TextView txtDate, txtTime;
	private Button btnSave, btnRemove;
	private ImageView btnDate, btnTime;
	private QueueAdapter adapter;
	private Calendar now;
	private ArrayList<Image> images;
	private ArrayList<Image> selectedImages;
	private UnumProgressView unumProgressView;
	int theme = 0;

	public static CustomPostFragment newInstance() {
		CustomPostFragment fragment = new CustomPostFragment();
		return fragment;
	}

	public CustomPostFragment() {
		// Required empty public constructor
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override public void onPrepareOptionsMenu(Menu menu) {
		MenuItem item=menu.findItem(R.id.add);
		item.setVisible(false);
	}

	private void showLoading() {
		unumProgressView.setVisibility(View.VISIBLE);
	}
	private void hideLoading(){
		unumProgressView.setVisibility(View.GONE);
	}

	@Override public void onResume() {
		super.onResume();
		if(main != null){
			if(theme > 3){
				main.setBackgroundColor(Color.BLACK);
			}else{
				main.setBackgroundColor(Color.WHITE);
			}
		}
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_custom_post, container, false);

		selectedImages = new ArrayList<>();
		images = new ArrayList<>();
		adapter = new QueueAdapter(getActivity(), images, selectedImages, this);

		unumProgressView = (UnumProgressView) view.findViewById(R.id.unumProgressView);
		main = (RelativeLayout) view.findViewById(R.id.main);
		layoutGrid = (FrameLayout) view.findViewById(R.id.layoutGrid);
		recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
		txtDate = (TextView) view.findViewById(R.id.txtDate);
		txtTime = (TextView) view.findViewById(R.id.txtTime);
		btnDate = (ImageView) view.findViewById(R.id.btnDate);
		btnTime = (ImageView) view.findViewById(R.id.btnTime);
		btnRemove = (Button) view.findViewById(R.id.btnRemove);
		btnSave = (Button) view.findViewById(R.id.btnSave);

		btnDate.setOnClickListener(this);
		btnTime.setOnClickListener(this);
		btnSave.setOnClickListener(this);
		btnRemove.setOnClickListener(this);

		now = Calendar.getInstance();
		txtDate.setText(DateUtil.getDisplayDateMonthName(now.getTime()));
		txtTime.setText(DateUtil.getDisplayTime(now.getTime()));

		UnumSession mUnumSession = new UnumSession(getActivity());
		theme = mUnumSession.getUNUM_DEFAULT_THEME();
		switch (theme) {
			case 0:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGold));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGold));
				break;
			case 1:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeCyan));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeCyan));
				break;
			case 2:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemePitch));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemePitch));
				break;
			case 3:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGrey));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGrey));
				break;
			case 4:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGold));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGold));
				break;
			case 5:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeCyan));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeCyan));
				break;
			case 6:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemePitch));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemePitch));
				break;
			case 7:
				btnDate.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGrey));
				btnTime.setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorSchemeGrey));
				break;
		}

		return view;
	}

	@Override public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if(isVisibleToUser){
			getSchedules();
		}
	}

	private void getSchedules() {
		showLoading();
		WebServiceCall webServiceCall = new WebServiceCall(getActivity());
		webServiceCall.getAllSchedules(new WebServiceCall.Callback<List<GetAllSchedulesResponse>>() {
			@Override public void onSuccess(List<GetAllSchedulesResponse> responseBody) {
				AppLogger.d("GET SCHEDULES SUCCESS :: " + new Gson().toJson(responseBody));
				hideLoading();
				getUserImages();
			}

			@Override public void onFailure() {
				hideLoading();
				AppLogger.d("GET CUSTOM FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void getUserImages() {
		UnumSession mUnumSession = new UnumSession(getActivity());
		showLoading();
		WebServiceCall.Callback<List<GetUserPostsResponse>> callback =
				new WebServiceCall.Callback<List<GetUserPostsResponse>>() {
					@Override public void onSuccess(List<GetUserPostsResponse> getUserPostsResponses) {
						if (isAdded()) {
							hideLoading();
							ArrayList<GetUserPostsResponse> posts =
									(ArrayList<GetUserPostsResponse>) getUserPostsResponses;
							int size = posts.size();
							if (null != images) {
								images.clear();
							}
							for (int j = 0; j < size; j++) {
								GetUserPostsResponse userPostsResponse = posts.get(j);
								long id = userPostsResponse.getIndex();
								String postId = userPostsResponse.get_id();
								String msg = userPostsResponse.getMessage();
								String name = QueueFragment.EMPTY_IMAGE_PATH + j;
								String path = userPostsResponse.getImageUrl();
								String draft = userPostsResponse.getDraft();
								if (draft.equalsIgnoreCase(mUnumSession.getUNUM_DEFAULT_DRAFT())) {
									Image image = new Image(id, postId, msg, name, path, draft, false);
									Log(id + " / " + name + " / " + path);
									if (null != path && !path.startsWith(QueueFragment.EMPTY_IMAGE_PATH)) {
										images.add(image);
									}
								}
							}
							orientationBasedUI(getResources().getConfiguration().orientation);
						}
					}

					@Override public void onFailure() {
						showToast(getString(R.string.message_post_some_err_occured));
						hideLoading();
					}

					@Override public void onProgressUpdate(int progress) {

					}
				};
		WebServiceCall mWebServiceCall = new WebServiceCall(getActivity());
		mWebServiceCall.getUsersPosts(callback);
	}

	public void orientationBasedUI(int orientation) {
		final WindowManager windowManager = (WindowManager) getActivity().getApplicationContext()
				.getSystemService(Context.WINDOW_SERVICE);
		final DisplayMetrics metrics = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(metrics);

		int columns = orientation == Configuration.ORIENTATION_PORTRAIT ? 3 : 5;

		GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), columns);
		recyclerView.setLayoutManager(layoutManager);

		if (adapter != null) {
			int size = (metrics.widthPixels / columns) - 15;
			adapter.setImageSize(size);
			layoutGrid.getLayoutParams().height = size * 2;
		}

		recyclerView.setAdapter(adapter);
		recyclerView.scrollToPosition(9);
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override public void onDetach() {
		super.onDetach();
	}

	@Override public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btnDate:
				showDatePicker();
				break;
			case R.id.btnTime:
				showTimePicker();
				break;
			case R.id.btnSave:
				createSchedule();
				break;
			case R.id.btnRemove:
				deleteSchedule();
				break;
		}
	}

	private void createSchedule() {
		showLoading();
		ArrayList<String> postIds = new ArrayList<String>();
		for (Image image : selectedImages) {
			postIds.add(image.getPostId());
			Log(">"+image.getPostId());
		}
		if(postIds.size() <= 0){
			showToast("Please select posts.");
			return;
		}
		String date = DateUtil.getDisplayDateTime(now.getTime());
		Log(">"+date);
		WebServiceCall webServiceCall = new WebServiceCall(getActivity());
		webServiceCall.updateSchedule(date, postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				hideLoading();
				showToast("Your schedule has been created.");
				AppLogger.d("CREATE SCHEDULE SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				hideLoading();
				showToast("Create post has failed due to some error. Please try again later.");
				AppLogger.d("CREATE SCHEDULE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void deleteSchedule() {
		showLoading();
		ArrayList<String> postIds = new ArrayList<String>();
		for (Image image : selectedImages) {
			postIds.add(image.getPostId());
			Log(">"+image.getPostId());
		}
		if(postIds.size() <= 0){
			showToast("Please select posts.");
			return;
		}
		WebServiceCall webServiceCall = new WebServiceCall(getActivity());
		webServiceCall.deleteSchedules(postIds, new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				hideLoading();
				showToast("Your schedule has been removed.");
				AppLogger.d("DELETE SCHEDULE SUCCESS :: " + new Gson().toJson(s));
			}

			@Override public void onFailure() {
				hideLoading();
				showToast("Remove post has failed due to some error. Please try again later.");
				AppLogger.d("DELETE SCHEDULE FAIL");
			}

			@Override public void onProgressUpdate(int progress) {

			}
		});
	}

	private void showDatePicker() {

		DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
				txtDate.setText(year + " " + monthOfYear + " " + dayOfMonth);
				now.set(Calendar.YEAR, year);
				now.set(Calendar.MONTH, monthOfYear);
				now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			}
		}, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
		dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
	}

	private void showTimePicker() {

		TimePickerDialog timePickerDialog =
				TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
						txtTime.setText(hourOfDay + ":" + minute);
						now.set(Calendar.HOUR_OF_DAY, hourOfDay);
						now.set(Calendar.MINUTE, minute);
						now.set(Calendar.SECOND, second);
					}
				}, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
		timePickerDialog.show(getActivity().getFragmentManager(), "TimePickerDialog");
	}

	@Override public void onSingleTap(int position) {
		clickImage(position);
	}

	private void clickImage(int position) {
		int selectedItemPosition = selectedImagePosition(images.get(position));
		if (selectedItemPosition == -1) {
			adapter.addSelected(images.get(position));
		} else {
			adapter.removeSelectedPosition(selectedItemPosition, position);
		}
	}

	private int selectedImagePosition(Image image) {
		for (int i = 0; i < selectedImages.size(); i++) {
			if (selectedImages.get(i).getPath().equals(image.getPath())) {
				return i;
			}
		}
		return -1;
	}

	@Override public void onDoubleTap(int position) {

	}

	@Override public void onItemMoved() {

	}
}
