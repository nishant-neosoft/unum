package com.unum.android.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.unum.android.R;

/**
 * This abstract activity adds back button support into toolbar. Main content view is set with
 * <code>setContentView(id)</code>,
 * and activity_drawer view is set by <code>setDrawerView(id)</code>.
 * <p>
 */
public abstract class BackButtonActivity extends BaseActivity {
	private Toolbar toolbar;
	private FrameLayout baseLayout;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.back_button_activity);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		//toolbar.setLogo( R.drawable.ic_action_logo_with_bg );
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				onBackPressed();
			}
		});

		baseLayout = (FrameLayout) findViewById(R.id.baseLayout);
	}

	@Override public void onBackPressed() {
		super.onBackPressed();
		supportFinishAfterTransition();
	}

	@Override public void setContentView(int id) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(id, this.baseLayout);
	}

	@Override protected void onStart() {
		super.onStart();
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	// getters for layout components
	protected Toolbar getToolbar() {
		return toolbar;
	}
}
