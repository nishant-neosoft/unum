package com.unum.android.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.ThemeUtils;
import com.unum.android.utils.UnumSession;

public class BaseActivity extends AppCompatActivity {
	public static final String KEY_USER_INFO = "com.unum.android.userinfo";
	public static final String KEY_DELETE_IMAGES = "com.unum.android.delete.images";
	public static final String KEY_UPDATE_POST_IMAGES = "com.unum.android.update.post.images";
	public static final String KEY_TILE_IMAGES = "com.unum.android.update.tile.images";
	public static final String KEY_TIP = "com.unum.android.tip.type";
	public static final String KEY_REMINDER_ID = "com.unum.android.reminder.id";
	public static final String KEY_CREATE_REMINDER = "com.unum.android.create.reminder";
	public static final String KEY_UPDATE_REMINDER = "com.unum.android.update.reminder";

	protected Toast toast;
	private boolean backClicked = false;
	private ProgressDialog pDialog;

	public UnumSession mUnumSession;
	public int theme;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		// Set portrait orientation for the whole app (for the time being)
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	/*
		Alarm alarm=new Alarm();
		alarm.setAlarmActive(true);
		alarm.setDays(new Alarm.Day[]{Alarm.Day.WEDNESDAY});
		alarm.setId(99);
		alarm.setAlarmTime("11:29");
		alarm.schedule(this);
		Database.init(this);
		Database.create(alarm);
		Database.deactivate();//on pause
		*/
	}

	@Override protected void onStop() {
		super.onStop();
	}

	@Override protected void onDestroy() {
		if (toast != null) {
			toast.cancel();
		}
		super.onDestroy();
	}

	public void Log(String msg) {
		AppLogger.d("", msg);
	}

	public void Log(String tag, String msg) {
		AppLogger.d(tag, msg);
	}

	public void showToast(String message) {
		toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
		toast.show();
	}

	protected void createSnackBar(View view, String msg) {
		Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
		snackbar.show();
	}

	protected void showLoadingDialog() {
		pDialog = new ProgressDialog(this, R.style.LoadingDialogTheme);
		pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		pDialog.show();
	}

	protected void showLoadingDialog(String customMsg) {
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(customMsg);
		pDialog.show();
	}

	protected void dismissLoadingDialog() {
		if(pDialog!=null)
		pDialog.dismiss();
	}

	protected void showMessageDialogWithOkButton(final String title, final String message,
			DialogInterface.OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(R.string.ok, listener);
		builder.create().show();
	}

	protected void showFragment(int fragment_holder, Fragment fragment) {
		showFragment(fragment_holder, fragment, "");
	}

	protected void showFragment(int fragment_holder, Fragment fragment, String tag) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(fragment_holder, fragment, tag);
		//transaction.show(fragment);
		transaction.commit();
	}

	@Override public void onBackPressed() {
		super.onBackPressed();
		//		if ( backClicked )
		//		{
		//			backClicked = false;
		//			super.onBackPressed();
		//		}
		//		else
		//		{
		//			backClicked = true;
		//			toast = Toast.makeText( getApplicationContext(), R.string.message_exit_app, Toast.LENGTH_SHORT );
		//			toast.show();
		//		}
	}

	@Override protected void onResume() {
		changeTheme();
		super.onResume();
		backClicked = false;
	}

	public void changeTheme(){
		mUnumSession = new UnumSession(this);
		ThemeUtils.onActivityCreateSetTheme(this,mUnumSession.getUNUM_DEFAULT_THEME());
	}
}
