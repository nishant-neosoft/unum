package com.unum.android.ui.helper;

/**
 * Created by Snehal on 9/21/2016.
 */
public interface OnTitleChangeListener {
	public void	onTitleChanged(String title);
}
