package com.unum.android.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import com.unum.android.R;
import com.unum.android.utils.UnumSession;
import java.util.ArrayList;
import java.util.List;

public class ScheduleTabbedActivity extends BackButtonActivity {

	private TabLayout tabLayout;
	private ViewPager viewPager;
	private CoordinatorLayout coordinatorLayout;
	int colorPressed = R.color.colorAccent;
	int colorNormal = Color.WHITE;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_tabbed);

		coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		mUnumSession = new UnumSession(this);
		theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if (theme > 3) {
			tabLayout = (TabLayout) findViewById(R.id.tabs_dark);
		} else {
			tabLayout = (TabLayout) findViewById(R.id.tabs_light);
		}
		tabLayout.setVisibility(View.VISIBLE);
		setupTheme();
		setUpViewPager();
	}

	private void setupTheme() {
		colorPressed = R.color.colorAccent;
		colorNormal = Color.WHITE;
		switch (theme) {
			case 0:
				colorPressed = R.color.colorSchemeGold;
				colorNormal = Color.WHITE;
				break;
			case 1:
				colorPressed = R.color.colorSchemeCyan;
				colorNormal = Color.WHITE;
				break;
			case 2:
				colorPressed = R.color.colorSchemePitch;
				colorNormal = Color.WHITE;
				break;
			case 3:
				colorPressed = R.color.colorSchemeGrey;
				colorNormal = Color.WHITE;
				break;
			case 4:
				colorPressed = R.color.colorSchemeGold;
				colorNormal = Color.BLACK;
				break;
			case 5:
				colorPressed = R.color.colorSchemeCyan;
				colorNormal = Color.BLACK;
				break;
			case 6:
				colorPressed = R.color.colorSchemePitch;
				colorNormal = Color.BLACK;
				break;
			case 7:
				colorPressed = R.color.colorSchemeGrey;
				colorNormal = Color.BLACK;
				break;
		}
	}

	private void setUpViewPager() {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFragment(ReminderFragment.newInstance(), getString(R.string.title_reminder));
		adapter.addFragment(CustomPostFragment.newInstance(), getString(R.string.title_custom_post));
		adapter.addFragment(CalendarFragment.newInstance(), getString(R.string.title_calendar));
		viewPager.setAdapter(adapter);
		tabLayout.setupWithViewPager(viewPager);
		viewPager.setVisibility(View.VISIBLE);
		//Setting default values
		setTitle(getString(R.string.title_reminder));
		LinearLayout ll = (LinearLayout) tabLayout.getChildAt(0);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ll.getChildAt(0).setBackgroundColor(getResources().getColor(colorPressed, getTheme()));
		} else {
			ll.getChildAt(0).setBackgroundColor(getResources().getColor(colorPressed));
		}

		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override public void onTabSelected(TabLayout.Tab tab) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					ll.getChildAt(tab.getPosition())
							.setBackgroundColor(getResources().getColor(colorPressed, getTheme()));
				} else {
					ll.getChildAt(tab.getPosition())
							.setBackgroundColor(getResources().getColor(colorPressed));
				}
			}

			@Override public void onTabUnselected(TabLayout.Tab tab) {
				ll.getChildAt(tab.getPosition()).setBackgroundColor(colorNormal);
			}

			@Override public void onTabReselected(TabLayout.Tab tab) {

			}
		});
	}

	class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
			viewPager.setOnPageChangeListener(this);
		}

		@Override public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

		}

		@Override public void onPageSelected(int position) {
			setTitle(mFragmentTitleList.get(position));
		}

		@Override public void onPageScrollStateChanged(int state) {

		}
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.add:
				Intent i = new Intent(this, CreateReminderActivity.class);
				i.putExtra(BaseActivity.KEY_REMINDER_ID, BaseActivity.KEY_CREATE_REMINDER);
				startActivityForResult(i, ReminderFragment.REQUEST_CODE);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_allschdule, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log("activity = onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
	}
}
