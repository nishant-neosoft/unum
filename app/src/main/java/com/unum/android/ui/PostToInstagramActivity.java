package com.unum.android.ui;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.unum.android.R;
import com.unum.android.ui.views.UnumImageView;
import com.unum.android.utils.GlideUtils;
import com.unum.android.utils.UnumSession;
import java.io.File;

public class PostToInstagramActivity extends BackButtonActivity {

	private UnumImageView imageToUpload;
	private RelativeLayout main;
	private TextView textPostCaption;
	private Button buttonPostToInstagram;

	private String type;
	private String mediaPath;
	private String caption;
	private String filePath;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		changeTheme();
		setContentView(R.layout.activity_post_to_instagram);

		findViews();

		initClicks();
	}

	private void findViews() {

		type = getIntent().getStringExtra("CONTENT_TYPE");
		caption = getIntent().getStringExtra("CAPTION");
		mediaPath = getIntent().getStringExtra("MEDIA_PATH");

		main = (RelativeLayout) findViewById(R.id.main);
		imageToUpload = (UnumImageView) findViewById(R.id.image_to_upload);
		textPostCaption = (TextView) findViewById(R.id.text_post_caption);
		buttonPostToInstagram = (Button) findViewById(R.id.button_post_to_instagram);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.x;

		LinearLayout.LayoutParams lParmars = new LinearLayout.LayoutParams(width, height);
		imageToUpload.setLayoutParams(lParmars);

		textPostCaption.setText(caption);
		GlideUtils.getImageFromPath(this, imageToUpload.getImageView(),
				mediaPath);//,imageToUpload,width,height);
		imageToUpload.getProgressBar().setVisibility(View.GONE);
		filePath = fetchFileFromStorage();

		mUnumSession = new UnumSession(this);
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();
		if (theme > 3) {
			main.setBackgroundColor(Color.BLACK);
		} else {
			main.setBackgroundColor(Color.WHITE);
		}
		switch (theme) {
			case 0:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeGold));
				break;
			case 1:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeCyan));
				break;
			case 2:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemePitch));
				break;
			case 3:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeGrey));
				break;
			case 4:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeGold));
				break;
			case 5:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeCyan));
				break;
			case 6:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemePitch));
				break;
			case 7:
				buttonPostToInstagram.setBackgroundColor(
						ContextCompat.getColor(this, R.color.colorSchemeGrey));
				break;
		}
	}

	private String fetchFileFromStorage() {
		String mFileName = "", mfileNameWithoutExtn = "", mfileExtn = "";
		String mRootDir;

		mFileName = mediaPath.substring(mediaPath.lastIndexOf('/') + 1, mediaPath.length());
		mfileNameWithoutExtn = mFileName.substring(0, mFileName.lastIndexOf('.'));
		mfileExtn = mFileName.substring(mFileName.lastIndexOf('.') + 1);

		mRootDir = Environment.getExternalStorageDirectory() + File.separator + "UNUM";

		String fileName = mfileNameWithoutExtn + "." + mfileExtn;
		File mFile = new File(mRootDir, fileName);

		return mFile.getAbsoluteFile().toString();
	}

	private void initClicks() {
		buttonPostToInstagram.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				postToInstagram();
			}
		});
	}

	private void postToInstagram() {
		Intent intent = getPackageManager().getLaunchIntentForPackage("com.instagram.android");
		ClipboardManager clipboardManager =
				(ClipboardManager) getSystemService(Activity.CLIPBOARD_SERVICE);
		ClipData clipData = ClipData.newPlainText("caption", caption);
		clipboardManager.setPrimaryClip(clipData);

		if (intent != null) {
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setPackage("com.instagram.android");
			share.setType(type);
			File media = new File(filePath);
			Uri uri = Uri.fromFile(media);
			share.putExtra(Intent.EXTRA_STREAM, uri);

			startActivity(Intent.createChooser(share, "Share to"));
		} else {
			// bring user to the market to download the app.
			intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=" + "com.instagram.android"));
			startActivity(intent);
		}
	}
}
