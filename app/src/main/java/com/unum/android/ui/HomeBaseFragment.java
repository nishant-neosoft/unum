package com.unum.android.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;
import com.unum.android.R;
import com.unum.android.data.model.InstagramImages;
import com.unum.android.data.model.response.GetUserPostsResponse;
import com.unum.android.data.server.CallbackRequest;
import com.unum.android.data.server.InstagramClient;
import com.unum.android.data.server.InstagramService;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.instagram.ApplicationData;
import com.unum.android.instagram.InstagramApp;
import com.unum.android.utils.AppLogger;
import com.unum.android.utils.UnumSession;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Dhaval Parmar on 22/9/16.
 */

public class HomeBaseFragment extends BaseFragment {

	public static final String INSTA_IMAGE = "instagram_image";
	public static final String EMPTY_IMAGE = "image";

	public WebServiceCall mWebServiceCall;
	public InstagramApp mApp;
	public ArrayList<InstagramImages.Data> dataArrayList;
	public HashMap<String, String> userInfo;

	public boolean isFirstLaunch = true;
	public UnumSession mUnumSession;

	@Override public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = getActivity();
		mWebServiceCall = new WebServiceCall(activity);
		mUnumSession = new UnumSession(getActivity());
		isFirstLaunch = mUnumSession.isFirstLaunch();

		if (getArguments() != null) {
			userInfo =
					(HashMap<String, String>) getArguments().getSerializable(BaseActivity.KEY_USER_INFO);
		}
		mApp = new InstagramApp(getActivity(), ApplicationData.CLIENT_ID, ApplicationData.CLIENT_SECRET,
				ApplicationData.CALLBACK_URL);

		//getInstagramImages();
	}

	public void fetchUserUnumImages() {
	}

	public void setUserInfo(HashMap<String, String> userInfo) {
		this.userInfo = userInfo;
	}

	public void getInstagramImages() {
		showLoadingDialog(getActivity(), getString(R.string.message_loading_photos));
		InstagramService instagramService = InstagramClient.getClient().create(InstagramService.class);
		AppLogger.print("getInstagramImages() userInfo = "+String.valueOf(userInfo));
		AppLogger.print("getInstagramImages() = "+mApp.getTOken()+"/ "+mApp.getName());
		Call<InstagramImages> call =
				instagramService.getAllImages(userInfo.get(InstagramApp.TAG_ID), mApp.getTOken());
		call.enqueue(new CallbackRequest<InstagramImages>() {
			@Override
			public void onResponse(Call<InstagramImages> call, Response<InstagramImages> response) {
				if (isAdded()) {
					super.onResponse(call, response);
					AppLogger.print("RESPONSE code() = "+response.code());
					AppLogger.print("RESPONSE isSuccessful() = "+response.isSuccessful());
					AppLogger.print("RESPONSE message())= "+response.message());
					try {
						AppLogger.print("RESPONSE errorBody()=" + response.errorBody().string());
					}catch (Exception e){
						e.printStackTrace();
					}
					AppLogger.print("RESPONSE body()= "+response.body()+"/ "+response.message());
					dismissLoadingDialog();
				}
			}

			@Override public void onResponse(Response<InstagramImages> response) {
				dismissLoadingDialog();
				dataArrayList = response.body().getData();
				fetchUserUnumImages();
			}

			@Override public void onError() {
				AppLogger.print("ERROR FETCHING IMAGES...");
				dismissLoadingDialog();
			}
		});
	}

	public void featureComingSoon() {
		showToast(getString(R.string.message_coming_soon));
	}

	public interface OnUploadComplete{
		public void imageUploaded();
	}

	public void uploadFile(int pos, String pathGallery, String draft, OnUploadComplete listener) {
		showLoadingDialog(getActivity());

		File selectedFile = new File(pathGallery);

		String mimeType;
		if (pathGallery.lastIndexOf(".") != -1) {
			String ext = pathGallery.substring(pathGallery.lastIndexOf(".") + 1);
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			mimeType = mime.getMimeTypeFromExtension(ext);
		} else {
			mimeType = "image/jpeg";
		}

		WebServiceCall.Callback<String> call = new WebServiceCall.Callback<String>() {
			@Override public void onSuccess(String s) {
				dismissLoadingDialog();
				if(listener!=null){
					listener.imageUploaded();
				}
			}

			@Override public void onFailure() {
				showToast(getString(R.string.some_err_occured));
				dismissLoadingDialog();
			}

			@Override public void onProgressUpdate(int progress) {
			}
		};

		mWebServiceCall.getImageResources(pos, Long.toString(selectedFile.length()), mimeType,
				selectedFile, draft, call);
	}

	public class CustomComparator implements Comparator<GetUserPostsResponse> {
		@Override public int compare(GetUserPostsResponse o1, GetUserPostsResponse o2) {
			Integer i1 = o1.getIndex();
			Integer i2 = o2.getIndex();
			return i1.compareTo(i2);
		}
	}

	public void orientationBasedUI(int orientation) {
		//Overloading
	}
}
