package com.unum.android.ui;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import com.unum.android.R;
import com.unum.android.utils.ThemeUtils;
import com.unum.android.utils.UnumSession;

/**
 * Created by Dhaval Parmar on 20/9/16.
 */
public class SettingActivity extends BackButtonActivity {
	RadioGroup mColorAccentRadioGroup;
	Switch mLiveSwitch;
	Switch mModeSwitch;

	RadioButton radioPeach;
	RadioButton radioCyan;
	RadioButton radioGold;
	RadioButton radioGrey;

	UnumSession mUnumSession;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//changeTheme();
		setContentView(R.layout.activity_setting);
		getToolbar().setTitle(getString(R.string.title_setting));

		findViews();

		initClicks();
	}

	private void findViews() {
		mUnumSession = new UnumSession(this);
		int theme = mUnumSession.getUNUM_DEFAULT_THEME();

		mColorAccentRadioGroup = (RadioGroup) findViewById(R.id.color_accent_radioGroup);
		mLiveSwitch = (Switch) findViewById(R.id.swict_livemode);
		mModeSwitch = (Switch) findViewById(R.id.switch_color_mode);

		radioPeach = (RadioButton) findViewById(R.id.radio_pitch);
		radioCyan = (RadioButton) findViewById(R.id.radio_blue);
		radioGrey = (RadioButton) findViewById(R.id.radio_grey);
		radioGold = (RadioButton) findViewById(R.id.radio_brown);

		if (theme > 3) {
			mModeSwitch.setChecked(true);
		} else {
			mModeSwitch.setChecked(false);
		}

		switch (theme) {
			case 0:
				radioPeach.setChecked(false);
				radioCyan.setChecked(false);
				radioGrey.setChecked(false);
				radioGold.setChecked(true);
				break;
			case 1:
				radioPeach.setChecked(false);
				radioCyan.setChecked(true);
				radioGrey.setChecked(false);
				radioGold.setChecked(false);
				break;
			case 2:
				radioPeach.setChecked(true);
				radioCyan.setChecked(false);
				radioGrey.setChecked(false);
				radioGold.setChecked(false);
				break;
			case 3:
				radioPeach.setChecked(false);
				radioCyan.setChecked(false);
				radioGrey.setChecked(true);
				radioGold.setChecked(false);
				break;
			case 4:
				radioPeach.setChecked(false);
				radioCyan.setChecked(false);
				radioGrey.setChecked(false);
				radioGold.setChecked(true);
				break;
			case 5:
				radioPeach.setChecked(false);
				radioCyan.setChecked(true);
				radioGrey.setChecked(false);
				radioGold.setChecked(false);
				break;
			case 6:
				radioPeach.setChecked(true);
				radioCyan.setChecked(false);
				radioGrey.setChecked(false);
				radioGold.setChecked(false);
				break;
			case 7:
				radioPeach.setChecked(false);
				radioCyan.setChecked(false);
				radioGrey.setChecked(true);
				radioGold.setChecked(false);
				break;
		}
	}

	private void initClicks() {
		mColorAccentRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
				switch (checkedId) {
					case R.id.radio_pitch:
						if (mModeSwitch.isChecked()) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.PITCH_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.PITCH);
						}
						break;
					case R.id.radio_blue:
						if (mModeSwitch.isChecked()) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.CYAN_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.CYAN);
						}
						break;
					case R.id.radio_brown:
						if (mModeSwitch.isChecked()) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GOLD_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GOLD);
						}
						break;
					case R.id.radio_grey:
						if (mModeSwitch.isChecked()) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GREY_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GREY);
						}
						break;
				}
			}
		});

		mModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
				int selectedId = mColorAccentRadioGroup.getCheckedRadioButtonId();
				switch (selectedId) {
					case R.id.radio_pitch:
						if (isChecked) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.PITCH_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.PITCH);
						}
						break;
					case R.id.radio_blue:
						if (isChecked) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.CYAN_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.CYAN);
						}
						break;
					case R.id.radio_brown:
						if (isChecked) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GOLD_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GOLD);
						}
						break;
					case R.id.radio_grey:
						if (isChecked) {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GREY_DARK);
						} else {
							ThemeUtils.changeToTheme(SettingActivity.this, ThemeUtils.GREY);
						}
						break;
				}
			}
		});
	}
}
