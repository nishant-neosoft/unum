package com.unum.android.instagram;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.unum.android.R;
import com.unum.android.data.server.WebServiceCall;
import com.unum.android.ui.DrawerActivity;
import java.io.File;
import java.util.HashMap;

public class MainActivity extends Activity implements OnClickListener {
	WebServiceCall mWebServiceCall;
	private InstagramApp mApp;
	private Button btnConnect, btnViewInfo, btnGetAllImages, btnFollowers, btnFollwing, btnSetting,
			btnUploadImage;
	private LinearLayout llAfterLoginView;
	private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private Handler handler = new Handler(new Callback() {

		@Override public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = mApp.getUserInfo();
			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				Toast.makeText(MainActivity.this, "Check your network.", Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	});

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mApp = new InstagramApp(this, ApplicationData.CLIENT_ID, ApplicationData.CLIENT_SECRET,
				ApplicationData.CALLBACK_URL);
		mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

			@Override public void onSuccess() {
				// tvSummary.setText("Connected as " + mApp.getUserName());
				btnConnect.setText("Disconnect");
				llAfterLoginView.setVisibility(View.VISIBLE);
				// userInfoHashmap = mApp.
				mApp.fetchUserName(handler);
			}

			@Override public void onFail(String error) {
				Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
			}
		});
		setWidgetReference();
		bindEventHandlers();

		if (mApp.hasAccessToken()) {
			// tvSummary.setText("Connected as " + mApp.getUserName());
			btnConnect.setText("Disconnect");
			llAfterLoginView.setVisibility(View.VISIBLE);
			mApp.fetchUserName(handler);

			new Handler().postDelayed(new Runnable() {
				@Override public void run() {
					//mWebServiceCall = new WebServiceCall(MainActivity.this, mApp, userInfoHashmap);
					//mWebServiceCall.loginWithUnum();
					// mWebServiceCall.updateUserInfo();
					// mWebServiceCall.getSelfInfo();
					// mWebServiceCall.logoutUser();
					// mWebServiceCall.updateDeviceToken();
				}
			}, 5000);
		}
	}

	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			switch (requestCode) {
				case 0:
					Uri uriGallery = data.getData();
					Log.e("FILE URI GALLERY", uriGallery.toString());
					String pathGallery = getPath(MainActivity.this, uriGallery);
					Log.e("File Path GALLERY: ", pathGallery);
					File selectedFile = new File(pathGallery);

					String mimeType = getContentResolver().getType(uriGallery);
					Log.e("File MIMETYPE GALLERY: ", String.valueOf(mimeType));

					Cursor returnCursor = getContentResolver().query(uriGallery, null, null, null, null);
					int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
					returnCursor.moveToFirst();
					Log.e("File SIZE GALLERY: ", Long.toString(returnCursor.getLong(sizeIndex)));

					//mWebServiceCall.getImageResources(Long.toString(returnCursor.getLong(sizeIndex)),
					//		mimeType, selectedFile);

					break;
			}
		}
	}

	private void bindEventHandlers() {
		btnConnect.setOnClickListener(this);
		btnViewInfo.setOnClickListener(this);
		btnGetAllImages.setOnClickListener(this);
		btnFollwing.setOnClickListener(this);
		btnFollowers.setOnClickListener(this);

		btnSetting.setOnClickListener(this);
		btnUploadImage.setOnClickListener(this);
	}

	private void setWidgetReference() {
		llAfterLoginView = (LinearLayout) findViewById(R.id.llAfterLoginView);
		btnConnect = (Button) findViewById(R.id.btnConnect);
		btnViewInfo = (Button) findViewById(R.id.btnViewInfo);
		btnGetAllImages = (Button) findViewById(R.id.btnGetAllImages);
		btnFollowers = (Button) findViewById(R.id.btnFollows);
		btnFollwing = (Button) findViewById(R.id.btnFollowing);
		btnSetting = (Button) findViewById(R.id.btnSetting);
		btnUploadImage = (Button) findViewById(R.id.btnUploadImage);
	}

	@Override public void onClick(View v) {
		if (v == btnConnect) {
			connectOrDisconnectUser();
		}else if (v == btnGetAllImages) {
			Log.d("TAG", "userInfoHashmap :: " + userInfoHashmap);
			startActivity(
					new Intent(MainActivity.this, AllMediaFiles.class).putExtra("userInfo", userInfoHashmap));
		} else if (v == btnSetting) {
			startActivity(new Intent(MainActivity.this, DrawerActivity.class));
		} else if (v == btnUploadImage) {
			openImageChooser();
		} else {
			String url = "";
			if (v == btnFollowers) {
				url = "https://api.instagram.com/v1/users/"
						+ userInfoHashmap.get(InstagramApp.TAG_ID)
						+ "/follows?access_token="
						+ mApp.getTOken();
			} else if (v == btnFollwing) {
				url = "https://api.instagram.com/v1/users/"
						+ userInfoHashmap.get(InstagramApp.TAG_ID)
						+ "/followed-by?access_token="
						+ mApp.getTOken();
			}
			startActivity(new Intent(MainActivity.this, Relationship.class).putExtra("userInfo", url));
		}
	}

	private void openImageChooser() {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");

		Intent chooser = Intent.createChooser(intent, "Choose a Picture");
		startActivityForResult(chooser, 0);
	}

	public String getPath(Context context, Uri uri) {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = { "_data" };
			Cursor cursor = null;

			try {
				cursor = context.getContentResolver().query(uri, projection, null, null, null);
				int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception e) {
				// Eat it
				e.printStackTrace();
			}
		} else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

	private void connectOrDisconnectUser() {
		if (mApp.hasAccessToken()) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setMessage("Disconnect from Instagram?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							mApp.resetAccessToken();
							// btnConnect.setVisibility(View.VISIBLE);
							llAfterLoginView.setVisibility(View.GONE);
							btnConnect.setText("Connect");
							// tvSummary.setText("Not connected");
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			final AlertDialog alert = builder.create();
			alert.show();
		} else {
			mApp.authorize();
		}
	}
}