package com.unum.android.instagram;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.unum.android.R;
import com.unum.android.data.model.InstagramImages;
import com.unum.android.instagram.lazyload.ImageLoader;
import java.util.ArrayList;

public class MyGridListAdapter extends BaseAdapter {
	// private Context context;
	private ArrayList<InstagramImages.Data> instagramImages;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;

	public MyGridListAdapter(Context context, ArrayList<InstagramImages.Data> imagesList) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.instagramImages = imagesList;
		this.imageLoader = new ImageLoader(context);
	}

	@Override public int getCount() {
		return instagramImages.size();
	}

	@Override public Object getItem(int position) {
		return position;
	}

	@Override public long getItemId(int position) {
		return position;
	}

	@Override public View getView(int position, View convertView, ViewGroup parent) {
		View view = inflater.inflate(R.layout.activity_media_list, null);
		Holder holder = new Holder();
		holder.ivPhoto = (ImageView) view.findViewById(R.id.ivImage);
		String url = instagramImages.get(position).getImages().getThumbnail().getUrl();
		imageLoader.DisplayImage(url, holder.ivPhoto);
		return view;
	}

	private class Holder {
		private ImageView ivPhoto;
	}
}
