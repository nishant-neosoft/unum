package com.unum.android.instagram;

import java.util.ArrayList;

/**
 * Created by Snehal on 8/30/2016.
 */
public class LoggedInstaUsers {
	ArrayList<User> userArrayList;

	public ArrayList<User> getUserArrayList() {
		return userArrayList;
	}

	public void setUserArrayList(ArrayList<User> userArrayList) {
		this.userArrayList = userArrayList;
	}

	public void addUser(String accessToken, String id, String username, String name) {
		if (userArrayList == null) {
			userArrayList = new ArrayList<>();
		} else {
			for (User user : userArrayList) {
				user.setActive(false);
			}
		}

		User user = new User(accessToken, id, username, name, true);
		userArrayList.add(user);
	}

	public class User {
		private String accessToken;
		private String id;
		private String username;
		private String name;
		private boolean isActive;

		public User(String accessToken, String id, String username, String name, boolean isActive) {
			this.accessToken = accessToken;
			this.id = id;
			this.username = username;
			this.name = name;
			this.isActive = isActive;
		}

		@Override public String toString() {
			return username;
		}

		public boolean isActive() {
			return isActive;
		}

		public void setActive(boolean active) {
			isActive = active;
		}

		public String getAccessToken() {
			return accessToken;
		}

		public void setAccessToken(String accessToken) {
			this.accessToken = accessToken;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
	}
}
