package com.unum.android.instagram;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import com.google.gson.Gson;
import com.unum.android.R;
import com.unum.android.data.model.InstagramImages;
import com.unum.android.data.server.CallbackRequest;
import com.unum.android.data.server.InstagramClient;
import com.unum.android.data.server.InstagramService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Response;

public class AllMediaFiles extends Activity {
	public static final String TAG_DATA = "data";
	public static final String TAG_IMAGES = "images";
	public static final String TAG_THUMBNAIL = "thumbnail";
	public static final String TAG_URL = "url";
	private static int WHAT_ERROR = 1;
	private GridView gvAllImages;
	private HashMap<String, String> userInfo;
	private List<String> imageThumbList = new ArrayList<String>();
	private Context context;
	private int WHAT_FINALIZE = 0;
	private ProgressDialog pd;
	private InstagramApp mApp;

	private void print(String msg) {
		Log.d("TAG", msg);
	}

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_media_list_files);
		gvAllImages = (GridView) findViewById(R.id.gvAllImages);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra("userInfo");
		print("userinfo :: " + userInfo.toString());
		context = AllMediaFiles.this;
		mApp = new InstagramApp(this, ApplicationData.CLIENT_ID, ApplicationData.CLIENT_SECRET,
				ApplicationData.CALLBACK_URL);

		getInstagramImages();
	}

	private void getInstagramImages() {
		pd = ProgressDialog.show(context, "", "Loading images...");
		InstagramService instagramService = InstagramClient.getClient().create(InstagramService.class);
		Call<InstagramImages> call =
				instagramService.getAllImages(userInfo.get(InstagramApp.TAG_ID), mApp.getTOken());
		call.enqueue(new CallbackRequest<InstagramImages>() {
			@Override
			public void onResponse(Call<InstagramImages> call, Response<InstagramImages> response) {
				super.onResponse(call, response);
				pd.dismiss();
				print("INSTAGRAM API IMAGES" + new Gson().toJson(response.code()));
			}

			@Override public void onResponse(Response<InstagramImages> response) {
				pd.dismiss();
				print("INSTAGRAM API IMAGES ::: " + new Gson().toJson(response.body().getData()));
				gvAllImages.setAdapter(new MyGridListAdapter(context, response.body().getData()));
			}

			@Override public void onError() {
			}
		});
	}
}
